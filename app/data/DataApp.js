export default {
    dataHome: [
        { id: 1, name: "THANH TOÁN", icon: require('../images/icon/ic_thanhtoantaiquay.png'), iconWidth: 45, iconHeight: 40, redirectTo: 'transactionInit' },
        { id: 2, name: "LỊCH SỬ\nGIAO DỊCH", icon: require('../images/icon/ic_history.png'), iconWidth: 40, iconHeight: 40, redirectTo: 'history' },
        { id: 3, name: "KIỂM TRA\nGIAO DỊCH", icon: require('../images/icon/ic_group.png'), iconWidth: 36, iconHeight: 40, redirectTo: 'transactionCheck', verifiedLogin: true },
        { id: 4, name: "THÔNG TIN", icon: require('../images/icon/ic_setting_group.png'), iconWidth: 45, iconHeight: 45, redirectTo: 'setting' }
    ],
    dataInitTrans: [
        { key: "Số điện thoại", value: "%phoneNumber%" },
        { key: "Số tiền", value: "%amount%" },
        { key: "Voucher", value: "%voucher%" },
        { key: "Phí giao dịch", value: "Miễn phí", border: true, marginTop: 10 },
        { key: "Thanh toán", value: "%totalAmount%", marginTop: 10 },
    ],
    dataResultTrans: [
        { key: "Số điện thoại", value: "%phoneNumber%" },
        { key: "Số tiền", value: "%amount%" },
        { key: "Mã giao dịch", value: "%transId%", transDetails: true },
    ],
    dataSetting: [
        { key: "Mã cửa hàng", value: "%storeId%" },
        { key: "Tên cửa hàng", value: "%storeName%" },
        { key: "Địa chỉ", value: "%address%" }
    ],
    refundPolicy: [
        "1. Chỉ được phép hoàn tiền với những giao dịch thực hiện trong ngày.",
        "2. Số tiền hoàn lại không được phép lớn hơn số tiền ví MoMo đã thanh toán. Trong đó, số tiền ví MoMo đã thanh toán = Số tiền thanh toán - Voucher - Phí giao dịch (nếu có).",
        "3. Voucher đã thanh toán không được hoàn lại.",
        "4. Phí giao dịch không được hoàn lại.",
        "5. Mỗi giao dịch chỉ được phép hoàn tiền 1 lần.",
    ]
};