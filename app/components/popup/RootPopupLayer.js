import React, { Component } from 'react';
import BasePopup from "../popup/BasePopup";
import PopupManager from "./PopupManager";
import {
    View,
} from 'react-native';
const TAG = 'ROOT-POPUP'
export default class RootPopupLayer extends Component {
    popupStack = []
    bottomSheetStack = []
    loadingStack = []

    constructor(props) {
        super(props);
        this.state = {
            trigger: false
        }
    }

    refresh(popups) {
        let newStack = []
        if (popups) {
            for (let popup of popups) {
                if (popup && popup.isClose == false) {
                    newStack.push(popup)
                }
            }
        }
        return newStack
    }

    isUniquePopupExists(popup, packageStack) {
        if (popup && popup.props.isUnique) {
            for (let i = 0; i < packageStack.length; i++) {
                let popupPackage = packageStack[i]
                if (popupPackage && popupPackage.isClose == false && popupPackage.popup.props.refId == popup.props.refId) {
                    return true
                }
            }
        }
        return false
    }

    showPopup(popup) {
        if (popup && popup.props) {
            if (popup.props.refId == PopupManager.REF_LOADINGPOPUP) {
                if (this.isUniquePopupExists(popup, this.loadingStack) == false) {
                    let loadingPackage = {
                        popup: popup,
                        isClose: false,
                    }
                    this.loadingStack.push(loadingPackage)
                }
            } else if (popup.props.priority >= BasePopup.PRIORITY_BOTTOMSHEET && popup.props.priority < BasePopup.PRIORITY_POPUP) {
                let bottomSheetPackage = {
                    popup: popup,
                    isClose: false,
                }
                if (this.isUniquePopupExists(popup, this.bottomSheetStack) == false) {
                    this.bottomSheetStack.push(bottomSheetPackage)
                }
            } else if (popup.props.priority >= BasePopup.PRIORITY_POPUP) {
                let popupPackage = {
                    popup: popup,
                    isClose: false,
                }
                if (this.isUniquePopupExists(popup, this.popupStack) == false) {
                    this.popupStack.push(popupPackage);
                }
            }
        }
        this.loadingStack = this.refresh(this.loadingStack)
        this.bottomSheetStack = this.refresh(this.bottomSheetStack)
        this.popupStack = this.refresh(this.popupStack)
        this.setState({
            trigger: true,
        })
    }

    clear(callback) {
        this.loadingStack = []
        this.bottomSheetStack = []
        this.popupStack = []
        this.setState({
            trigger: true,
        }, () => {
            if (callback) {
                callback()
            }
        })
    }

    getInstance(refId) {
        if (refId && this.refs[refId]) {
            return this.refs[refId]
        } else {
            return null
        }
    }

    renderStack(packageStack) {
        let views = []
        let displayPopup = null
        for (let i = 0; i < packageStack.length; i++) {
            let popupPackage = packageStack[i]
            if (popupPackage && popupPackage.isClose == false) {
                if (displayPopup == null || (displayPopup && displayPopup.props.priority < popupPackage.popup.props.priority)) {
                    popupPackage.popup = this.generatePopupElement(popupPackage, i)
                    displayPopup = popupPackage.popup
                }
            }
        }
        console.log(TAG + 'view length:' + views.length)
        views.push(displayPopup)
        return views
    }

    generatePopupElement(popupPackage, key) {
        let popupElement = React.cloneElement(popupPackage.popup, {
            ref: popupPackage.popup.props.refId ? popupPackage.popup.props.refId : null,
            key: key,
            show: true,
            onHideForExecute: () => {
                popupPackage.isClose = true
                this.setState({
                    trigger: true,
                })
            }
        });
        return popupElement
    }

    render() {
        return (
            <View pointerEvents={'box-none'}
                style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                {
                    this.renderStack(this.bottomSheetStack)
                }
                {
                    this.renderStack(this.popupStack)
                }
                {
                    this.renderStack(this.loadingStack)
                }
            </View>
        );
    }
}


