/**
 * Created by hiendv on 3/10/2018
 */

'use strict';
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
} from 'react-native';
var MoMoStrings = require('../../../screens/MoMoStrings');
var TimerMixin = require('react-timer-mixin');
var MomoConfig = require('../../../screens/MomoConfig');
var MomoReactToNative = require('../../../native/MomoReactToNative');
import MomoRootNavigation from '../../../root/MomoRootNavigation'
var MomoModel = require('../../../controls/MomoModel');
var Link = require('../../../controls/LinkComponent');
var counter = 0;
import TouchIDModule from "../../../native/TouchIDModule";
import AuthBox from '../../../controls/authentication/AuthBox';
import DeeplinkHandler from '../../../root/handler/DeeplinkHandler'
import {isIphoneX} from '../../../controls/MomoDeviceHelper';
import InputBottomSheet from "./InputBottomSheet";
import PopupManager from "../../PopupManager";

export default class PasswordAuthenticationSheet extends InputBottomSheet {
    freeze = false
    middleHeight = 0;
    middleTouchIDHeight = 0;
    middleErrorMessageHeight = 0;

    defaultState = {
        ...this.defaultState,
        isAutoFocus: false,
        passErrDesc: "",//'Bạn đã nhập sai mật khẩu vui lòng nhập lại'
        wrongCount: 0,
        value: "",
        isClearValue: false,
        touchIdEnable: false,
        isCheckPass: true,
        heightPopup: this.heightPopup
    };

    constructor(props) {
        super(props);

        this.title = MoMoStrings.textPass
        this.length = 6
        this.mode = AuthBox.MODE_PASSWORD
        this.linkText = MoMoStrings.textForgotPassword

        MomoReactToNative.authenticateWithPassword((pass) => {
            this.passTmp = pass;
        });
        this.state = {
            ...this.defaultState
        }
        MomoReactToNative.authenticateWithPassword((pass) => {
            this.passTmp = pass;
        });
    }

    showXXX() {
        this.setState({
            isTouchOutSideToClose: false,
            onRequestClose: () => {
            }
        }, () => {
            this.freeze = true;
            this.isTouchIdAvailable((isEnable) => {
                if (isEnable) {
                    setTimeout(() => {
                            this.onTouchIdClick();
                            this.unfreeze(1000)
                        },
                        200)

                }
                else {
                    this.unfreeze(500)
                }
            });
        });
    }

    unfreeze(ms) {
        setTimeout(() => {
            this.setState({
                isTouchOutSideToClose: true,
                onRequestClose: null,
            }, () => {
                this.freeze = false;
                this.clearPass("");
            });
        }, ms);
    }

    close(callBack) {
        // Prevent close when show TouchId Scan
        if (!this.freeze) {
            super.close(callBack)
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.showXXX();
        this.isTouchIdAvailable((isEnable) => {
            this.setState({
                touchIdEnable: isEnable
            });
        });
    }

    //HungHC: Check IOS TouchID Enable
    isTouchIdAvailable(callBack) {
        console.log("isTouchIdAvailable");
        TouchIDModule.canAuthenticate((canAuthenticate)=>{
            var showTouchID = canAuthenticate
                && this.passTmp != null && this.passTmp.length > 0;
            if (this.state.touchIdEnable != showTouchID) {
                this.setState({touchIdEnable: showTouchID}, () => {
                    callBack(showTouchID);
                });
            }
            else {
                callBack(showTouchID);
            }
        })
    }


    onTouchIdClick() {
        TouchIDModule.canAuthenticate((canAuthenticate)=>{
            this.setState({touchIdEnable: canAuthenticate});
            if (canAuthenticate) {
                TouchIDModule.authenticate((password)=>{
                    if (password && password.length == 6) {
                        this.onComplete(password);
                    }
                })
            }
        })
    }

    clearPass(errDesc, callback) {
        this.setState({
            value: "",
            isClearValue: true,
            passErrDesc: errDesc
        }, () => {
            if (callback) {
                callback()
            }
        });
    }

    passTmp = null

    onComplete(userPass) {
        MomoReactToNative.authenticateWithPassword((pass) => {
            this.passTmp = pass;

            console.log("TYPE PASSWORD COMPLETE " + userPass)
            let messsageFail = MoMoStrings.textCheckPassFail;
            if (this.props.isSDKPayment) {
                //HungHC: If user already Login to App, verify in APP
                if (this.passTmp) {
                    if (userPass === this.passTmp) {
                        counter = 0;
                        this.onPressFinished(userPass, true, "");
                    }
                    else {
                        messsageFail = MoMoStrings.textIncorrectPass.replace('{0}', (2 - counter));
                        this.onPressFinished(userPass, false, messsageFail);
                    }
                }
                //HungHC: Dont verify in APP, return raw password
                else {
                    this.onPressFinished(userPass, true, "");
                }
            }
            else {
                if (this.passTmp) {
                    if (userPass === this.passTmp) {
                        counter = 0;
                        this.onPressFinished(userPass, true, "");
                    }
                    else {
                        messsageFail = MoMoStrings.textIncorrectPass.replace('{0}', (2 - counter));
                        this.onPressFinished(userPass, false, messsageFail);
                    }
                }
                else {
                    var tranHisMsg = {};
                    tranHisMsg._class = "mservice.backend.entity.msg.LoginMsg";
                    let msgType = "CHECK_PASS";
                    var extra = {};
                    MomoReactToNative.getPinHashWithPassword(userPass, (pHash) => {
                        extra.pHash = pHash;
                        MomoModel.sendMsgToBEWithExtra(
                            tranHisMsg,
                            msgType,
                            extra,
                            response => this.onPressFinished(userPass, response.result, response.errorDesc),
                            error => this.onPressFinished(userPass, false, messsageFail + error),
                            userPass
                        );
                    });
                }
            }
        });

    }

    onPressFinished(passEnter, isSuccess, errDesc) {
        //HungHC: disable freeze TouchID
        this.setState({
            isTouchOutSideToClose: true,
            onRequestClose: null,
        }, () => {
            this.freeze = false;
            this.clearPass(isSuccess ? "" : errDesc, () => {
                counter++;
                if(this.props.callbackInputPass){
                    this.props.callbackInputPass(isSuccess);
                }
                if (isSuccess) {
                    counter = 0;
                    this.close(() => {
                        if (this.props.onSuccessPassCode) {
                            this.props.onSuccessPassCode(passEnter);
                        }
                    });
                }
                else if (!isSuccess && counter >= 3) {
                    var message = MoMoStrings.textEnterPassFail3Time;
                    if (counter > 3) {
                        message = MoMoStrings.textEnterPassFailOver3Time;
                    }
                    this.refs.momoView.showAlertView(
                        message,
                        [MoMoStrings.btnAlertOk],// array button
                        (selectedIndex) => { // press button index
                            if (selectedIndex == 0) {
                                if (this.props.isSDKPayment) {
                                    // MomoReactToNative.encryptParam("", "");
                                    DeeplinkHandler.callbackDeeplink("","");
                                    MomoRootNavigation.finish();
                                } else {
                                    MomoReactToNative.signout();
                                }
                            }
                        },
                        false, //isTouchOutSide
                        () => {
                        }, //callback when close
                        true
                    );
                }
            });
        });

    }

    _renderErrMsg() {
        let {passErrDesc} = this.state;
        if (passErrDesc && passErrDesc.trim() != "") {
            return (
                <View
                    onLayout={(event) => {
                        if (this.middleErrorMessageHeight == 0) {
                            this.middleErrorMessageHeight = event.nativeEvent.layout.height
                        }

                        this.reRenderMiddle()
                    }} style={{paddingTop: 5, marginHorizontal: 10}}>
                    <Text ellipsizeMode="tail" numberOfLines={1}
                          style={{color: '#F68086', fontSize: 14, textAlign: 'center'}}>
                        {passErrDesc}
                    </Text>
                </View>
            )
        }
        else {
            return null;
        }
    }

    _renderTouchID() {
        let {touchIdEnable} = this.state;
        if (touchIdEnable) {
            return (
                <View
                    onLayout={(event) => {
                        if (this.middleTouchIDHeight == 0) {
                            this.middleTouchIDHeight = event.nativeEvent.layout.height
                        }
                        this.reRenderMiddle()
                    }}
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: (this.middleHeight - this.state.middleContentHeight > 6 ? ((this.middleHeight - this.middleTouchIDHeight) / 2) : 6),
                        paddingBottom: 10,
                    }}>
                    <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row',
                        }}
                        onPress={() => this.onTouchIdClick()}>
                        <Image style={styles.touchIDImage}
                               resizeMode='cover'
                               source={isIphoneX() ? require('../../../common_img/ic_faceid.png') : require('../../../common_img/ic_touchid.png')}/>

                        <Text style={{
                            color: MomoConfig.textColor,
                            marginLeft: 10,
                            fontSize: 14
                        }}>
                            {
                                isIphoneX() ? 'Xác thực gương mặt' : 'Xác thực vân tay'
                            }
                        </Text>
                    </TouchableOpacity>
                </View>

            );
        }
        else {
            return <View style={{flex: 1}}/>;
        }
    }

    renderMiddleArea() {
        return (
            <View onLayout={(event) => {
                if (this.middleHeight == 0) {
                    this.middleHeight = event.nativeEvent.layout.height
                }
                this.reRenderMiddle()
            }} style={{backgroundColor: 'transparent', flex: 1}}>
                {this._renderErrMsg()}
                {this._renderTouchID()}
            </View>
        );
    }

    reRenderMiddle() {
        this.setState({
            middleContentHeight: this.middleErrorMessageHeight + this.middleTouchIDHeight
        })

        if (!this.checkRerender && this.middleHeight > 0 && this.middleErrorMessageHeight > 0 && this.middleTouchIDHeight > 0 && (this.middleErrorMessageHeight + this.middleTouchIDHeight) > this.middleHeight) {
            this.checkRerender = true
            this.setState({
                heightPopup: this.heightPopup + ((this.middleErrorMessageHeight + this.middleTouchIDHeight) - this.middleHeight)
            })
        }
    }


    renderLinkContent() {
        if (this.props.isSDKPayment) {
            return null;
        } else {
            return (<Link onTouch={this.onTapLink.bind(this)} text={this.linkText}/>);
        }
    }

    onTapLink() {
        PopupManager.showAlert(
            "Đặt lại mật khẩu",
            "Bạn sẽ phải đăng xuất khỏi tài khoản này để đặt lại mật khẩu.",
            ["ĐỒNG Ý", "KHÔNG"],
            (selectedIndex) => {
                if (selectedIndex == 0) {
                    MomoReactToNative.resetPassword();
                }
                else {

                }
            }
        );
    }

}

var styles = StyleSheet.create({
    container: {},
    pass: {
        borderWidth: 1,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 40,
        backgroundColor: "#FFF",
        paddingHorizontal: 45,
        borderColor: MomoConfig.borderBoxColor
    },
    headerArea: {
        flex: 1,
        paddingBottom: 10,
        paddingTop: 15,
        backgroundColor: MomoConfig.backgroundColor
    },
    touchIDImage: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
});
module.exports = PasswordAuthenticationSheet;
