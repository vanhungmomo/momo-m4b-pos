import EmptyModalSheet from "./EmptyModalSheet";
import PopupManager from "../../PopupManager";

var StyleSheet = require('StyleSheet');
var Dimensions = require('Dimensions');
var React = require('react');
var ReactNative = require('react-native');
var {
    TouchableOpacity,
    Text,
    Image,
    View,
    ListView
} = ReactNative;
var Momo = require('../../../controls/GlobalMomoComponent');
var {Line} = Momo;

var MomoConfig = require('../../../screens/MomoConfig');

const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
export default class PickerBottomSheet  extends EmptyModalSheet{
    static propTypes = {
        ...View.propTypes,
        ...EmptyModalSheet.propTypes,
    };

    static defaultProps = {
        ...EmptyModalSheet.defaultProps,
    };

    constructor(props) {
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        var datas = this.props.dataSource?this.props.dataSource:[];
        this.state = {
            ...this.defaultState,
            data: datas,
            dataSource: ds.cloneWithRows(datas),
            selectedName: this.props.selectedName? this.props.selectedName : "",
            selectedID : this.props.selectedID? this.props.selectedID : -1
        }
    }

    updateData(title, dataSource, callback = null){
        this.setState({title: title, dataSource : this.state.dataSource.cloneWithRows(dataSource)},()=>callback(true));
    }

    renderActionSheet() {
        return (
            <View style={[styles.containerStyle, this.props.style]}>
                {this.renderHeader()}
                <View style={[{borderWidth: 0, flex: 1}, this.props.styleChild]}>
                    {this.renderListChildren()}
                </View>
            </View>
        )
    }

    renderListChildren(){
        return (<ListView
            removeClippedSubviews={false}
            keyboardShouldPersistTaps="always"
            enableEmptySections={true}
            dataSource={this.state.dataSource}
            renderRow={this._renderRow.bind(this)}
            renderSeparator={(sectionID, rowID) => {
                return <Line key={sectionID + rowID.toString()} color='#D8D8D8'/>}}
        />);
    }

    _renderRow(rowData, sectionID, rowID) {
        return(<TouchableOpacity  style={[styles.row,this.props.styleRow]} onPress={()=>this._pressRow(rowID, rowData)}>
            <Text style={[styles.rowLabel,this.props.styleRowLabel]}>{rowData.name}</Text>
            {
                (rowData.name == this.state.selectedName || rowID == this.state.selectedID) ?
                    <Image
                        style={[styles.rowImage,this.props.styleSelectedIcon]}
                        source={this.props.selectedIcon? this.props.selectedIcon : require('../../../controls/img/selected.png')}
                    />
                    :null
            }
        </TouchableOpacity>);
    }

    _pressRow(rowID, rowData) {
        this.props.callback(rowID,rowData.id, rowData.name, rowData);
        this._closePicker();
    }

    _closePicker() {
        this.close()
    }
}

var styles = StyleSheet.create({
    containerStyle: {

        height: heightScreen * 2 / 3,
        width: widthScreen,
        backgroundColor: "#fff"
    },
    title: {
        fontSize: 18,
        color: '#505050'
    },
    rowImage: {
        height: 20,
        width: 20,
        resizeMode: 'contain'
    },
    rowLabel: {
        flex: 1,
        fontSize: 16,
        color: '#4d4d4d'
    },
    row: {
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        height: 50,
        paddingHorizontal: 20
    }
});