/**
 * Created by anbui on 12/12/17.
 */
'use strict';

var StyleSheet = require('StyleSheet');
var Dimensions = require('Dimensions');
var React = require('react');
var ReactNative = require('react-native');
var {
    TouchableOpacity,
    Text,
    Image,
    View
} = ReactNative;

var MomoConfig = require('../../../screens/MomoConfig');
import BaseBottomSheet from '../BaseBottomSheet'

const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
export default class EmptyModalSheet extends BaseBottomSheet {

    static propTypes = {
        ...View.propTypes,
        ...BaseBottomSheet.propTypes,
    };

    static defaultProps = {
        ...BaseBottomSheet.defaultProps,
    };

    constructor(props) {
        super(props);
        this.state = {
            ...this.defaultState,
        }
    }

    onClosePopup() {
        if (this.props.onClose != null && typeof this.props.onClose === "function") {
            this.props.onClose();
        }
        else {
            if (this.props.onClosePopup)
                this.props.onClosePopup();
            else
                this.close();
        }
    }

    renderRightIcon() {
        return (
            (this.props.rightButtonIcon && this.props.rightButtonAction) ?
                <TouchableOpacity onPress={this.props.rightButtonAction}
                                  style={[styles.touchableRight, styles.touchableArea]}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        {this.props.rightButtonIcon()}
                    </View>
                </TouchableOpacity>
                : null
        )
    }

    renderLeftIcon() {
        return (
            this.props.showCloseButton ?
                <TouchableOpacity onPress={() => this.onClosePopup()}
                                  style={[styles.touchableLeft, styles.touchableArea]}>
                    <Image source={require('../../../common_img/ic_close.png')}
                           style={{width: 15, height: 15}}/>
                </TouchableOpacity>
                : null
        )
    }

    renderHeader() {
        return (
            this.props.isHideHeader ?
                null :
                <View style={styles.headerPopup}>
                    {this.renderLeftIcon()}
                    {this.props.title ? <Text style={[styles.title]}>{this.props.title}</Text> : null}
                    {this.renderRightIcon()}
                </View>
        )
    }

    _setModalVisible(visible){

        if(visible){
            this.startAnimAnimation();
        }
        else {
            this.close();
        }
    }

    renderActionSheet() {
        return (
            <View style={[styles.containerStyle, this.props.style]}>
                {this.renderHeader()}
                <View style={[{borderWidth: 0, flex: 1}, this.props.dropStyle]}>
                    {this.props.children}
                </View>
            </View>
        )
    }

}

var styles = StyleSheet.create({
    containerStyle: {

        height: heightScreen * 2 / 3,
        width: widthScreen,
        backgroundColor: "#fff"
    },
    headerPopup: {
        alignItems: 'center', borderBottomWidth: 1, borderColor:'#DADADA',
        justifyContent: 'center',
        height: 50
    },
    mainButtonDefaultTextStyle: {
        color: '#4A90E2',
        fontSize: 18,
        textAlign: "center"
    },
    buttonDefaultTextStyle: {
        color: MomoConfig.primaryColor,
        fontSize: 18,
        textAlign: "center",
    },
    mainButtonDefaultContainerStyle: {
        borderRadius: 5,
        backgroundColor: "#FFFFFF",
        width: widthScreen - 20,
        height: 44,
        flexDirection: 'column',
        alignItems: 'center',
        borderWidth: 0,
        marginBottom: 10,
    },
    buttonDefaultContainerStyle: {
        backgroundColor: "#FFFFFF",
        width: widthScreen - 20,
        height: 44,
        flexDirection: 'column',
        alignItems: 'center',
    },
    touchableArea: {
        height: 50,
        maxWidth: 80,
        justifyContent:'center',
        position:'absolute',
        top: 0,
        borderWidth: 0,
    },
    touchableLeft: {
        left: 0,
        paddingLeft: 20
    },
    touchableRight: {
        right: 0,
        paddingRight: 5
    },
    title: {
        fontSize: 18,
        color: '#505050'
    }
});
