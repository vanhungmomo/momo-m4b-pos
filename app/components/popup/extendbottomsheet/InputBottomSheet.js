/**
 * Created by anbui on 12/12/17.
 */
'use strict';
import {Component} from 'react';

var StyleSheet = require('StyleSheet');
var PropTypes = require('react/lib/ReactPropTypes');
var Dimensions = require('Dimensions');
var View = require('View');
var React = require('react');
var ReactNative = require('react-native');
var {
    TouchableOpacity,
    Text,
    Image
} = ReactNative;

var window = Dimensions.get('window');
var MomoConfig = require('../../../screens/MomoConfig');
var heightBubblePopup = window.height * 2 / 3;
import BaseBottomSheet from '../BaseBottomSheet';
import MomoView from "../../../controls/MomoView";
import MomoKeyBoard from "../../../controls/MomoKeyBoard";
import AuthBox from "../../../controls/authentication/AuthBox";
import {Link} from "../../../controls/LinkComponent";

const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
export default class InputBottomSheet extends BaseBottomSheet {

    static propTypes = {
        ...View.propTypes,
        ...BaseBottomSheet.propTypes,
        heightPopup: React.PropTypes.number,
        showCloseButton: React.PropTypes.bool,
        transparentSize: React.PropTypes.number
    };

    static defaultProps = {
        ...BaseBottomSheet.defaultProps,
        heightPopup: heightBubblePopup,
        isTouchOutSideToClose: true,
        onRequestClose: null,
        showCloseButton: true,
        transparentSize: 0.8,
        show: true,
    };
    defaultState = {
        ...this.defaultState,
        isClearValue: false,
        value: '',
        heightPopup: heightBubblePopup,
        isTouchOutSideToClose: true,
        onRequestClose: null,
    };

    constructor(props) {
        super(props);

        this.title = this.props.title ? this.props.title : '';
        this.length = this.props.length ? this.props.length : 6;
        this.mode = this.props.mode ? this.props.mode : AuthBox.MODE_PASSWORD;
        this.linkText = this.props.linkText ? this.props.linkText : "";
        this.heightPopup = this.props.heightPopup ? this.props.heightPopup : heightBubblePopup;
        this.state = {
            ...this.defaultState
        }
    }

    close(callback){
        super.close();
        if(callback)
            callback();
    }
    clearValue() {
        this.setState({
            isClearValue: true,
            value: ''
        })
    }
    onComplete(value) {
        console.log("InputBottomSheet: Completed " + this.state.value)
    }
    onTapLink() {
        console.log("InputBottomSheet: Tap on link ")
    }
    onTyping(userPass) {
        console.log("InputBottomSheet: Typing" + userPass)
        let countChar = (userPass != null) ? userPass.trim().length : 0;
        if (countChar > this.length)
            return;

        if (countChar <= this.length) {
            this.setState({
                isClearValue: false,
                value: userPass,
            });
        }
    }

    renderRightIcon() {
        if (this.props.rightButtonIcon && this.props.rightButtonAction) {
            return (
                <TouchableOpacity onPress={this.props.rightButtonAction}
                                  style={[styles.touchableRight, styles.touchableArea]}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        {this.props.rightButtonIcon()}
                    </View>
                </TouchableOpacity>
            );
        }
    }
    renderHeader() {
        return (
            <View style={styles.headerPopup}>
                {
                    this.props.showCloseButton
                        ?  <TouchableOpacity onPress={() => this.close()}
                                             style={[styles.touchableLeft,styles.touchableArea]}>
                            <Image source={require('../../../common_img/ic_close.png')}
                                   style={{width: 15, height: 15}} />
                        </TouchableOpacity>
                        : null
                }

                <Text style={[styles.title]}>{this.title}</Text>
                {this.renderRightIcon()}
            </View>
        )
    }
    renderPassModeArea() {
        return (
            <View style={[styles.headerArea]}>
                {this.renderBoxArea()}
                {this.renderMiddleArea()}
                {this.renderLinkArea()}
            </View>
        );
    }
    renderBoxArea() {
        return (
            <View style={{ alignItems: 'center', backgroundColor: 'transparent', paddingTop: 15, paddingBottom: 5 }}>
                <AuthBox value={this.state.value}
                         mode={this.mode}
                         length={this.length}
                         onClear={() => {
                             console.log("InputBottomSheet:onClear");
                             this.clearValue();
                         }}
                         onComplete={this.onComplete.bind(this)} />
            </View>
        );
    }
    renderMiddleArea() {
        return (
            <View style={{ backgroundColor: 'transparent', flex: 1 }}>

            </View>
        );
    }
    renderLinkArea() {
        return (
            <View accessibilityLabel={"link"}
                  style={{ alignItems: "center", justifyContent: 'flex-end', paddingBottom: 10 }}>
                {
                    this.renderLinkContent()
                }
            </View>
        );
    }
    renderLinkContent() {
        var Link = require("../../../controls/LinkComponent");
        return (
            <Link onTouch={this.onTapLink.bind(this)} text={this.linkText} />
        );
    }
    renderKeyBoardInput() {
        return (
            <View accessibilityLabel='Keyboard' style={{ borderWidth: 0 }}>
                <View style={[this.props.style,styles.cssLine]} />
                <MomoKeyBoard onMomoKeyBoardTextChanged={this.onTyping.bind(this)} isClear={this.state.isClearValue} />
            </View>
        );
    }

    renderActionSheet() {
        return (
            <View style={[styles.containerStyle, this.props.style, {height: this.state.heightPopup}]}>
                {this.renderHeader()}
                <MomoView ref="momoView" paddingBottom={0} useAlertBar={false}>
                    <View style={{ borderWidth: 0, flex: 1 }}>
                        {this.renderPassModeArea()}
                        {this.renderKeyBoardInput()}
                    </View>
                </MomoView>
            </View>
        )
    }

}

var styles = StyleSheet.create({
    containerStyle: {
        flexDirection: 'column',
        height: heightScreen * 2 / 3,
        width: widthScreen,
        backgroundColor: "#fff"
    },
    mainButtonDefaultTextStyle: {
        color: '#4A90E2',
        fontSize: 18,
        textAlign: "center"
    },
    headerPopup: {
        //padding :10,
        alignItems: 'center', borderBottomWidth: 1, borderColor: '#DADADA',
        justifyContent: 'center',
        height: 50
    },
    buttonDefaultTextStyle: {
        color: MomoConfig.primaryColor,
        fontSize: 18,
        textAlign: "center",
    },
    mainButtonDefaultContainerStyle: {
        borderRadius: 5,
        backgroundColor: "#FFFFFF",
        width: widthScreen - 20,
        height: 44,
        flexDirection: 'column',
        alignItems: 'center',
        borderWidth: 0,
        marginBottom: 10,
    },
    buttonDefaultContainerStyle: {
        backgroundColor: "#FFFFFF",
        width: widthScreen - 20,
        height: 44,
        flexDirection: 'column',
        alignItems: 'center',
    },
    cssLine: {
        height: 1,
        backgroundColor: "#dadada"
    },
    touchableArea: {
        height: 50,
        width:50,
        justifyContent:'center',
        position:'absolute',
        top: 0,
        borderWidth: 0,
    },
    touchableLeft: {
        left: 0,
        paddingLeft: 20
    },
    touchableRight: {
        right: 0,
        paddingRight: 5
    },
    title: {
        fontSize: 18,
        color: '#505050'
    },
    headerArea: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: MomoConfig.backgroundColor
    },

});
