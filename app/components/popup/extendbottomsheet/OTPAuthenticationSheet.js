/**
 * Created by hiendv on 3/10/2018
 */
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
} from 'react-native';
import MomoConfig from '../../../screens/MomoConfig'
import MomoReactToNative from '../../../native/MomoReactToNative';
import MomoModel from '../../../controls/MomoModel';
import MoMoStrings from '../../../screens/MoMoStrings';
import AuthBox from '../../../controls/authentication/AuthBox';
import Link from '../../../controls/LinkComponent';
import InputBottomSheet from "./InputBottomSheet";

var window = Dimensions.get('window');
const LAST_RESEND_KEY = "FRAUD_LAST_RESEND_OTP"
const RETRY_TIME = 60
export default class OTPAuthenticationSheet extends InputBottomSheet {
    heightPopup = window.height * 2 / 3;
    middleHeight = 0;
    middleContentHeight = 0;

    static defaultProps = {
        ...InputBottomSheet.defaultProps,
        remainSeconds: 60,
    };

    constructor(props) {
        super(props);
        this.title = MoMoStrings.titleEnterPassOTP
        this.length = 6
        this.mode = AuthBox.MODE_OTP
        this.linkText = MoMoStrings.textOTPResend;
        this.state = {
            ...this.defaultState,
            resendAvailable: 0,
            isClearValue: false,
            value: '',
            isTouchOutSideToClose: true,
            onRequestClose: null,
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.retryResend(this.props.remainSeconds);
    }

    componentWillUnmount() {
        if (this.reloadResend) {
            clearInterval(this.reloadResend)
        }
    }

    retryResend(remainSeconds) {
        this.setState({
            resendAvailable: 0
        }, () => {
            if (this.reloadResend) {
                clearInterval(this.reloadResend)
            }
            if (remainSeconds) {
                remainSeconds = Math.ceil(remainSeconds / 1000)
            } else {
                remainSeconds = 60
            }
            this.reloadResend = setInterval(() => {
                remainSeconds = remainSeconds - 1
                this.setState({
                    resendAvailable: remainSeconds
                })
                if (remainSeconds <= -1) {
                    if (this.reloadResend) {
                        clearInterval(this.reloadResend)
                    }
                }
            }, 1000)
        })
    }

    renderMiddleArea() {
        return (
            <View onLayout={(event) => {
                this.middleHeight = event.nativeEvent.layout.height;
                this.reRenderMiddle()
            }} style={{backgroundColor: 'transparent', flex: 1}}>
                {
                    this.state.resendAvailable > 0 ? <Text
                            numberOfLines={2}
                            onLayout={(event) => {
                                this.middleContentHeight = event.nativeEvent.layout.height
                                this.reRenderMiddle()
                            }}
                            style={styles.description}>{MoMoStrings.messageOTPResend.replace("{0}", this.state.resendAvailable)}</Text> :
                        <Text
                            numberOfLines={2}
                            onLayout={(event) => {
                                this.middleContentHeight = event.nativeEvent.layout.height
                                this.reRenderMiddle()
                            }}
                            style={styles.description}>{" "}</Text>
                }

            </View>
        );
    }

    reRenderMiddle() {
        if (this.middleHeight > 0 && this.middleContentHeight > 0 && this.middleContentHeight > this.middleHeight && (this.heightPopup + (this.middleContentHeight - this.middleHeight) > this.state.heightPopup)) {
            this.setState({
                heightPopup: this.heightPopup + (this.middleContentHeight - this.middleHeight)
            })
        }
    }

    onComplete(value) {
        console.log("OTPAuthenticationSheet: OTP COMPLETED " + value)
        if (this.props.onComplete) {
            this.close();
            this.props.onComplete(value)
        }
    }

    onTapLink() {
        console.log("OTPAuthenticationSheet: Tab Re-Send ")
        this.startCheckPointSendOTP(() => {
            this.retryResend()
        })
        this.reSendOTP()
    }

    startCheckPointSendOTP(callback) {
        MomoReactToNative.getPreferencesWithKeyCallback(LAST_RESEND_KEY, (lastClick) => {
            let param = MomoModel.parseJsonString(lastClick)
            let currentTime = new Date().getTime()
            if (param) {
                param.lastResend = currentTime
                if (param.waitTime > 0) {
                    param.waitTime = param.waitTime * 2
                } else {
                    param.waitTime = RETRY_TIME
                }
            } else {
                param = {
                    lastResend: currentTime,
                    waitTime: RETRY_TIME
                }
            }
            MomoReactToNative.setSettingKeyValue(LAST_RESEND_KEY, JSON.stringify(param));
            if (callback) {
                callback()
            }
        });
    }

    reSendOTP() {
        if (this.props.reSendOTP) {
            this.props.reSendOTP()
        }
    }

    renderLinkContent() {
        if (this.state.resendAvailable >= 0) {
            return (
                <Text style={{color: MomoConfig.secondTextColor}}>{this.linkText}</Text>
            );
        } else {
            return (
                <Link onTouch={this.onTapLink.bind(this)} text={this.linkText}/>
            );
        }
    }
}

const styles = StyleSheet.create({
    description: {
        color: MomoConfig.secondTextColor,
        paddingTop: 10,
        paddingHorizontal: 20,
        textAlign: 'center',
        paddingBottom: 15,
        minHeight: window.height / 10
    }

});
