/**
 * Created by anbui on 12/12/17.
 */
'use strict';

var StyleSheet = require('StyleSheet');
var PropTypes = require('react/lib/ReactPropTypes');
var Dimensions = require('Dimensions');
var View = require('View');
var React = require('react');
var ReactNative = require('react-native');
var {
    TouchableOpacity,
    Text,
} = ReactNative;

var window = Dimensions.get('window');
var MomoConfig = require('../../../screens/MomoConfig');
import MoMoStrings from '../../../screens/MoMoStrings'
import BaseBottomSheet from '../BaseBottomSheet'
import Line from "../../../controls/LineComponent";

const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
export default class ActionSheet extends BaseBottomSheet {

    static propTypes = {
        ...View.propTypes,
        ...BaseBottomSheet.propTypes,
        mainButton: PropTypes.any,
        buttons: PropTypes.any,
    };

    static defaultProps = {
        ...BaseBottomSheet.defaultProps,
    };

    constructor(props) {
        super(props);
        this.state = {
            ...this.defaultState,
        }
    }

    renderButton(button) {
        let text = ''
        let textStyle = null
        let containerStyle = null
        let onClick = null
        let closeOnClick = true
        if (button) {
            if (button.text) {
                text = button.text
            }
            if (button.textStyle) {
                textStyle = button.textStyle
            }
            if (button.containerStyle) {
                containerStyle = button.containerStyle
            }
            if (button.onClick) {
                onClick = button.onClick
            }
            if (button.closeOnClick != undefined) {
                closeOnClick = button.closeOnClick
            }
            return (
                <View key={button.key}
                      style={containerStyle}>
                    <TouchableOpacity
                        style={{
                            flex: 1,
                            width: widthScreen - 20,
                            backgroundColor: 'transparent',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        onPress={() => {
                            if (onClick) {
                                onClick()
                            }
                            if (closeOnClick == true) {
                                this.close()
                            }
                        }}>
                        <Text style={textStyle}>
                            {text}
                        </Text>
                    </TouchableOpacity>
                    {
                        button.showBottomLine ? <View
                            style={{height: 1, width: widthScreen - 20, backgroundColor: '#E6E6E6'}}/> : null
                    }
                </View>
            )
        } else {
            return null
        }
    }

    renderMainButton() {
        if (this.props.mainButton) {
            let mainButton = {
                key: 'mainButton',
                text: this.props.mainButton.text ? this.props.mainButton.text : MoMoStrings.btnAlertCancel2,
                textStyle: [
                    styles.mainButtonDefaultTextStyle,
                    this.props.mainButton.textStyle,
                ],
                containerStyle: [
                    styles.mainButtonDefaultContainerStyle,
                    this.props.mainButton.containerStyle,
                ],
                closeOnClick: this.props.mainButton.closeOnClick,
                onClick: this.props.mainButton.onClick,
            }
            return this.renderButton(mainButton)
        } else {
            return null
        }
    }

    renderArrayButton() {
        let styleFirstButton =
            this.props.styleFirstButton?
            this.props.styleFirstButton :
            {
                    borderTopRightRadius: 0,
                    borderTopLeftRadius: 0
            };
        let styleLastButton = this.props.styleLastButton?
            this.props.styleLastButton :
            {
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5
            };
        if (this.props.buttons) {
            let views = []
            for (let i = 0; i < this.props.buttons.length; i++) {
                let button = this.props.buttons[i]
                let convertButton = {
                    key: i,
                    text: button.text ? button.text : i,
                    textStyle: [
                        styles.buttonDefaultTextStyle,
                        button.textStyle,
                    ],
                    containerStyle: [
                        i == 0 ? styleFirstButton : null,
                        i == this.props.buttons.length - 1 ? styleLastButton : null,
                        styles.buttonDefaultContainerStyle,
                        button.containerStyle,
                    ],
                    onClick: button.onClick,
                    closeOnClick: button.closeOnClick,
                    showBottomLine: i == this.props.buttons.length - 1 ? false : (button.showBottomLine != undefined ? button.showBottomLine : true)
                }
                views.push(this.renderButton(convertButton))
            }
            return (
                <View style={{paddingBottom: 5}}>
                    {
                        views
                    }
                </View>
            )
        } else {
            return null
        }
    }

    renderSheetTitle() {
        return (
            this.props.sheetTitle ?
                <View
                    key={'sheetTitle'}
                    style={[styles.titleContainerStyle,this.props.titleContainerStyle]}>
                    <View style={[styles.titleStyleView,this.props.titleStyleView]}>
                        <Text style={[styles.titleLabelStyle,this.props.titleLabelStyle]}>
                            {this.props.sheetTitle}
                        </Text>
                    </View>
                    <View style={[styles.titleUnderLineStyle,this.props.titleUnderLineStyle]}>
                        <Line color={"#E6E6E6"} height={1}/>
                    </View>
                </View>
                : null
        )
    }

    renderActionSheet() {
        return (
            <View style={[styles.containerStyle,this.props.containerStyle]}>
                {
                    this.renderSheetTitle()
                }
                {
                    this.renderArrayButton()
                }
                {
                    this.renderMainButton()
                }
            </View>
        )
    }

}

var styles = StyleSheet.create({
    containerStyle: {
        flexDirection: 'column',
        alignItems: 'center',
    },
    mainButtonDefaultTextStyle: {
        color: '#4A90E2',
        fontSize: 18,
        textAlign: "center"
    },
    buttonDefaultTextStyle: {
        color: MomoConfig.primaryColor,
        fontSize: 18,
        textAlign: "center",
    },
    mainButtonDefaultContainerStyle: {
        borderRadius: 5,
        backgroundColor: "#FFFFFF",
        width: widthScreen - 20,
        height: 44,
        flexDirection: 'column',
        alignItems: 'center',
        borderWidth: 0,
        marginBottom: 10,
    },
    buttonDefaultContainerStyle: {
        backgroundColor: "#FFFFFF",
        width: widthScreen - 20,
        height: 44,
        flexDirection: 'column',
        alignItems: 'center',
    },
    titleContainerStyle: {
        backgroundColor: "#FFFFFF",
        alignItems: 'center',
        flexDirection: 'column',
        borderWidth: 0,
        paddingBottom: 0,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5
    },
    titleStyleView:{
        width: widthScreen - 20,
        height: 44,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 0
    },
    titleLabelStyle: {
        color: MomoConfig.secondTextColor,
        fontSize: 18,
        textAlign: "center"
    },
    titleUnderLineStyle: {
        padding: 0,
        width: widthScreen - 20
    }
});
