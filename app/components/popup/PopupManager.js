/**
 * Created by anbui on 12/8/17.
 */
import React, { Component } from 'react';
import LoadingPopup from "./extendpopup/LoadingPopup";
import PopupBuilder from "./PopupBuilder";
import {
    Keyboard,
    InteractionManager,
    Dimensions
} from 'react-native';
const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
export default class PopupManager {
    static REF_LOADINGPOPUP = 'REF_LOADINGPOPUP'
    static REF_CONNECTION_ERROR_POPUP = 'REF_CONNECTION_ERROR_POPUP';
    static popupLayer

    static getInstance(refId) {
        if (PopupManager.popupLayer) {
            return PopupManager.popupLayer.getInstance(refId)
        } else {
            return null
        }
    }

    static clearPopup(callback) {
        if (PopupManager.popupLayer) {
            PopupManager.popupLayer.clear(callback)
        }
    }

    static showPopup(popup) {
        Keyboard.dismiss()
        InteractionManager.runAfterInteractions(() => {
            if (PopupManager.popupLayer) {
                PopupManager.popupLayer.showPopup(popup)
            }
        });

    }

    static showBottomSheet(actionSheet) {
        PopupManager.showPopup(actionSheet)
    }

    static showActionSheet(mainButton, buttons, callback, title) {
        let actionSheet = PopupBuilder.buildActionSheet(mainButton, buttons, callback, title)
        PopupManager.showBottomSheet(actionSheet)
    }

    static showEmptyPopup(REACT_COMPONENT, REACT_DEFAULT, callback) {
        Keyboard.dismiss()
        if (PopupManager.popupLayer) {
            let popup = PopupBuilder.buildEmptyPopup(REACT_COMPONENT, REACT_DEFAULT, callback)
            PopupManager.popupLayer.showPopup(popup)
        }
    }

    static showAlert(title, message, titleCancel, titleConfirm, cancelable, callback, direction = 'row') {
        if (PopupManager.popupLayer) {
            let param = new PopupBuilder.BasicPopupParam();
            param.titleConfirm = titleConfirm;
            param.titleCancel = titleCancel;
            param.cancelable = cancelable;
            param.callback = callback;
            param.title = title;
            param.message = message;
            param.direction = direction;
            let popup = PopupBuilder.buildBasicPopup(param);
            PopupManager.showPopup(popup);
        }
    }


    static showAlertView(title, message, cancelObject, confirmObject, cancelable = false) {
        if (PopupManager.popupLayer) {
            let callback = (index) => {
                if (index == 0 && confirmObject && confirmObject.callback) {
                    confirmObject.callback();
                }
                else if (cancelObject && cancelObject.callback) {
                    cancelObject.callback();
                }
            }

            let param = new PopupBuilder.BasicPopupParam()
            param.titleConfirm = confirmObject.title
            param.titleCancel = cancelObject.title
            param.cancelable = cancelable
            param.callback = callback
            param.title = title
            param.message = message
            let popup = PopupBuilder.buildBasicPopup(param)
            PopupManager.showPopup(popup)
        }
    }

    static showAlertWithImageHeader(title, message, titleCancel, titleConfirm, cancelable, callback, icon, iconStyle = null) {
        if (PopupManager.popupLayer) {
            let param = new PopupBuilder.BasicPopupParam()
            param.titleConfirm = titleConfirm
            param.titleCancel = titleCancel
            param.cancelable = cancelable
            param.callback = callback
            param.title = title
            param.message = message
            param.icon = icon
            param.iconStyle = iconStyle;
            let popup = PopupBuilder.buildBasicPopup(param)
            PopupManager.showPopup(popup)
        }
    }

    static showAlertViewWithImageHeader(title, message, cancelObject, confirmObject, cancelable = false, icon, iconStyle = null) {
        if (PopupManager.popupLayer) {

            let callback = (index) => {
                if (index == 0 && confirmObject && confirmObject.callback) {
                    confirmObject.callback();
                }
                else if (cancelObject && cancelObject.callback) {
                    cancelObject.callback();
                }
            }

            let param = new PopupBuilder.BasicPopupParam()
            param.titleConfirm = confirmObject.title
            param.titleCancel = cancelObject.title
            param.cancelable = cancelable
            param.callback = callback
            param.title = title
            param.message = message
            param.icon = icon
            param.iconStyle = iconStyle;
            let popup = PopupBuilder.buildBasicPopup(param)
            PopupManager.showPopup(popup)
        }
    }

    // static showCrashPopup() {
    //     let MoMoStrings = require("../screens/MoMoStrings");
    //     PopupManager.showAlertWithImageHeader(null, MoMoStrings.noticeAppCrash, null, MoMoStrings.btnAlertClose, true, () => {

    //     }, require('../common_img/img_crashapp.png'))
    // }

    // static showConnectionErrorPopup() {
    //     let MoMoStrings = require("../screens/MoMoStrings");
    //     let param = new PopupBuilder.BasicPopupParam();
    //     param.titleConfirm = MoMoStrings.btnAlertClose;
    //     param.cancelable = false
    //     param.title = MoMoStrings.errorConection;
    //     param.message = MoMoStrings.messageAlertErrorNetwork;
    //     param.icon = require("../common_img/ic_internet_connection.png");
    //     param.iconResizeMode = "contain";
    //     param.iconStyle = {
    //         height: (widthScreen - 30) / 2 - 60
    //     };
    //     param.iconWrapperStyle = {
    //         paddingTop: 15
    //     };
    //     param.isUnique = true;
    //     param.refId = PopupManager.REF_CONNECTION_ERROR_POPUP;
    //     let popup = PopupBuilder.buildBasicPopup(param);
    //     PopupManager.showPopup(popup);
    // }

    static showLoading(mainMsg, subMsg, icon) {
        LoadingPopup.state = LoadingPopup.STATE_SHOWLOADING
        let loadingPopup = PopupManager.getInstance(PopupManager.REF_LOADINGPOPUP)
        console.log("showLoading data = " + loadingPopup);
        if (loadingPopup == null) {
            PopupManager.showPopup(PopupBuilder.buildLoadingPopup(mainMsg, subMsg, icon))
        }
    }

    static hideLoading(animated) {
        LoadingPopup.state = LoadingPopup.STATE_HIDELOADING
        LoadingPopup.animated = animated
        let loadingPopup = PopupManager.getInstance(PopupManager.REF_LOADINGPOPUP)
        if (loadingPopup) {
            loadingPopup.hideLoading(animated)
        }
    }

    static doneLoading(animated) {
        LoadingPopup.state = LoadingPopup.STATE_DONELOADING
        LoadingPopup.animated = animated
        let loadingPopup = PopupManager.getInstance(PopupManager.REF_LOADINGPOPUP)
        if (loadingPopup) {
            loadingPopup.doneLoading(animated)
        }
    }

}