import React from 'react';
import BasicPopup from './extendpopup/BasicPopup'
import LoadingPopup from './extendpopup/LoadingPopup'
import PopupManager from './PopupManager'
export default class PopupBuilder {
    static buildBasicPopup(data) {
        if (data) {
            let titleConfirm = data.titleConfirm;
            let titleCancel = data.titleCancel;
            let callback = data.callback;
            let title = data.title;
            let message = data.message;
            let cancelable = data.cancelable;
            let icon = data.icon;
            let iconStyle = data.iconStyle;
            let iconResizeMode = data.iconResizeMode;
            let iconWrapperStyle = data.iconWrapperStyle;
            let refId = data.refId ? data.refId : new Date().getTime();
            let isUnique = data.isUnique;
            let direction = data.direction;
            let arrayButtons = [];
            if (titleConfirm) {
                let confirmButton = {
                    text: titleConfirm,
                    onClick: () => {
                        let popupInstance = PopupManager.getInstance(refId)
                        if (popupInstance) {
                            popupInstance.close()
                        }
                        if (callback) {
                            callback(0)
                        }
                    }
                };
                arrayButtons.push(confirmButton)
            }
            if (titleCancel) {
                let cancelButton = {
                    text: titleCancel,
                    onClick: () => {
                        let popupInstance = PopupManager.getInstance(refId)
                        if (popupInstance) {
                            popupInstance.close()
                        }
                        if (callback) {
                            callback(1)
                        }
                    }
                };
                arrayButtons.push(cancelButton)
            }
            let popup = <BasicPopup
                refId={refId}
                title={title}
                body={message}
                header={icon}
                headerStyle={iconStyle}
                headerResizeMode={iconResizeMode}
                headerWrapperStyle={iconWrapperStyle}
                buttons={arrayButtons}
                cancelable={cancelable}
                isUnique={isUnique}
                buttonsDirection={direction}
            />
            return popup
        } else {
            return null
        }
    }

    static buildLoadingPopup(mainMsg, subMsg, icon) {
        let refId = PopupManager.REF_LOADINGPOPUP
        let loadingPopup = <LoadingPopup refId={refId} isUnique={true} text={mainMsg} subText={subMsg} icon={icon}
            cancelable={false} />
        return loadingPopup
    }
}
class BasicPopupParam {
    constructor() {

    }

    titleConfirm
    titleCancel
    callback
    title
    message
    cancelable
    icon
}
PopupBuilder.BasicPopupParam = BasicPopupParam