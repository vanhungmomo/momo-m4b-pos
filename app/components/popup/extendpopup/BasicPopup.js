import React, { Component } from 'react';

import {
    Platform,
    Navigator,
    DeviceEventEmitter,
    Modal,
    TouchableOpacity,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    View
} from 'react-native';
import BasePopup from '../BasePopup'
const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
export default class BasicPopup extends BasePopup {

    static defaultProps = {
        ...BasePopup.defaultProps,
        buttonsDirection: 'row'
    };

    renderPopup() {
        return (
            <View style={styles.popupContainer}>
                {
                    this.renderImageHeader()
                }
                {
                    this.renderTitle()
                }
                {
                    this.renderBody()
                }
                {
                    this.renderFooter()
                }
            </View>
        );
    }

    renderImageHeader() {
        let { header } = this.props
        let headerWidth = BasePopup.POPUP_WIDTH
        if (header) {
            return (
                <View style={[{
                    width: headerWidth,
                    borderTopLeftRadius: 5,
                    borderTopRightRadius: 5,
                    overflow: 'hidden',
                    justifyContent: 'center',
                    alignItems: 'center'
                }, this.props.headerWrapperStyle]}>
                    <Image resizeMode={this.props.headerResizeMode ? this.props.headerResizeMode : 'contain'} style={[{
                        width: headerWidth,
                        height: headerWidth / 2,
                        borderTopLeftRadius: 3,
                        borderTopRightRadius: 3,
                        overflow: 'hidden'
                    }, this.props.headerStyle]} source={header} />
                </View>
            );
        } else {
            return null
        }
    }

    renderTitle() {
        let { title, header } = this.props
        if (title) {
            if (typeof title === 'string') {
                return (
                    <View style={[styles.headerFrame, { paddingTop: header ? 20 : 30 }]}>
                        <Text style={{ fontSize: 22, fontWeight: 'bold', color: '#4D4D4D' }}>{title}</Text>
                    </View>
                );
            } else {
                return title
            }
        } else {
            return null
        }
    }

    renderBody() {
        let { body, title, header } = this.props
        if (body) {
            if (typeof body === 'string') {
                return (
                    <View style={[styles.bodyFrame, { paddingTop: title || header ? 20 : 30 }]}>
                        <Text style={{ fontSize: 16, color: '#4D4D4D' }}>{body}</Text>
                    </View>
                );
            } else {
                return body
            }
        } else {
            return null
        }

    }

    renderFooter() {
        let { buttons, buttonsDirection } = this.props
        if (!buttons || buttons.length == 0) {
            return null
        } else if (typeof buttons == 'object') {
            if (Object.prototype.toString.call(buttons) == '[object Array]') {
                let views = []
                for (let i = buttons.length - 1; i >= 0; i--) {
                    if (buttonsDirection == 'row') {
                        views.push(this.renderFooterButton(i, buttons[i]))
                    } else {
                        views.unshift(this.renderFooterButton(i, buttons[i]))
                    }

                }
                return (
                    <View style={[styles.footerFrame, { flexDirection: buttonsDirection, justifyContent: 'flex-end', }]}>
                        {
                            views
                        }
                    </View>
                );
            } else {
                return buttons
            }
        }
    }

    renderFooterButton(index, button) {
        return (
            <TouchableOpacity key={'button' + button.text} style={[styles.defaultFooterButtonFrame, {
                paddingTop: 15
            }]} onPress={() => {
                if (button.onClick) {
                    button.onClick();
                }
                if (button.type == 'close') {
                    this.close()
                }
            }}>
                <Text
                    style={[styles.defaultFooterButton, { color: index == 0 ? '#B0006D' : '#8F8E94' }, button.style]}>{button.text}</Text>
            </TouchableOpacity>
        );
    }
}

var styles = StyleSheet.create({
    overlay: {
        flex: 1,
        backgroundColor: '#00000080',
        justifyContent: 'center',
        alignItems: 'center'
    },
    popupContainer: {
        backgroundColor: '#fff',
        width: BasePopup.POPUP_WIDTH,
        borderRadius: 5,
    },
    headerFrame: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        justifyContent: 'center',
        paddingHorizontal: 20,
        paddingTop: 30,
    },
    bodyFrame: {
        //backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingTop: 20,
    },
    footerFrame: {
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        paddingHorizontal: 20,
        paddingTop: 15,
        paddingBottom: 30,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        flexWrap: "wrap"
    },
    defaultFooterButtonFrame: {
        paddingLeft: 20,
    },
    defaultFooterButton: {
        fontSize: 18,
        color: '#8F8E94',
        fontWeight: '400',
    }
});
