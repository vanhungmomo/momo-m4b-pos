/**
 * Created by anbui on 12/12/17.
 */
'use strict';
import {Component} from 'react';
var StyleSheet = require('StyleSheet');
var PropTypes = require('react/lib/ReactPropTypes');
var Dimensions = require('Dimensions');
var View = require('View');
var React = require('react');
var ReactNative = require('react-native');
var {
    Platform,
    Navigator,
    DeviceEventEmitter,
    Modal,
    TouchableOpacity,
    Text,
    Image,
} = ReactNative;

var window = Dimensions.get('window');
var MomoConfig = require('../../../screens/MomoConfig');

import BasePopup from '../BasePopup'
const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;

export default class EmptyPopup extends BasePopup {

    static propTypes = {
        ...View.propTypes,
        ...BasePopup.propTypes,
    };

    static defaultProps = {
        ...BasePopup.defaultProps,
        overlayOpacity: "00"
    };

    
    constructor(props) {
        super(props);
        this.state = {
            ...this.defaultState,
        }
    }

    requestCancel() {
        if (this.props.REACT_COMPONENT == 'NewUserOnBoardingPopup') {
            return;
        } else {
            super.requestCancel();
        }

    }

    renderPopup() {
        var PopupNavigator = require('../../../screens/popup/PopupNavigator')
        let REACT_DEFAULT = JSON.stringify({
            popupName: this.props.REACT_COMPONENT,
            popupData: this.props.REACT_DEFAULT
        });        
        return (
                <PopupNavigator
                    refId={this.props.refId}
                    callback={this.props.callback}
                    close={() => {
                        this.close()
                    }}
                    REACT_DEFAULT={REACT_DEFAULT}
                />
            
        );
    }
}

var styles = StyleSheet.create({});
