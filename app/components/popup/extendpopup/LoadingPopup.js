import React, { Component } from 'react';
import {
    Platform,
    Navigator,
    DeviceEventEmitter,
    Modal,
    TouchableOpacity,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    View
} from 'react-native';
import BasePopup from '../BasePopup'
import * as Progress from 'react-native-progress';
const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
export default class LoadingPopup extends BasePopup {
    static LOADING_WIDTH = 60
    static ICON_WIDTH = 32
    static ICON_PADDING = (LoadingPopup.LOADING_WIDTH - LoadingPopup.ICON_WIDTH) / 2
    static DELAY_CLOSE = 600

    static state = LoadingPopup.STATE_HIDELOADING
    static animated = true
    static STATE_SHOWLOADING = 'STATE_SHOWLOADING'
    static STATE_LOADING = 'STATE_LOADING'
    static STATE_HIDELOADING = 'STATE_HIDELOADING'
    static STATE_DONELOADING = 'STATE_DONELOADING'

    static defaultProps = {
        ...BasePopup.defaultProps,
    };

    constructor(props) {
        super(props);
        this.state = {
            ...this.defaultState,
            doneLoading: false,
        }
    }

    componentDidMount() {
        super.componentDidMount()
        if (LoadingPopup.state == LoadingPopup.STATE_HIDELOADING) {
            this.hideLoading(LoadingPopup.animated)
        } else if (LoadingPopup.state == LoadingPopup.STATE_DONELOADING) {
            this.doneLoading(LoadingPopup.animated)
        } else {
            LoadingPopup.state = LoadingPopup.STATE_LOADING
            LoadingPopup.animated = true
        }
    }

    componentWillUnmount() {
        super.componentWillUnmount()
        LoadingPopup.state = LoadingPopup.STATE_HIDELOADING
    }

    delayClose(callback) {
        if (callback) {
            let currentTime = new Date().getTime()
            if (currentTime - this.mountedTime > LoadingPopup.DELAY_CLOSE) {
                callback()
            } else {
                setTimeout(() => {
                    callback()
                }, (LoadingPopup.DELAY_CLOSE - (currentTime - this.mountedTime)))
            }
        }
    }

    hideLoading(animated = true) {
        if (animated) {
            this.delayClose(() => {
                this.close()
            })
        } else {
            this.close()
        }
    }

    doneLoading(animated = true) {
        if (animated) {
            this.delayClose(() => {
                this.performDoneLoading()
            })
        } else {
            this.close()
        }
    }

    performDoneLoading() {
        if (this.mounted) {
            this.setState({
                doneLoading: true,
            }, () => {
                setTimeout(() => {
                    if (this.mounted) {
                        this.close()
                    }
                }, Platform.OS == 'ios' ? 400 : 550)
            })
        }
    }

    renderLoading() {
        return (
            <View style={{ width: LoadingPopup.LOADING_WIDTH, height: LoadingPopup.LOADING_WIDTH }}>
                <Progress.CircleSnail size={LoadingPopup.LOADING_WIDTH} spinDuration={2500} indeterminate={true}
                    color='white' />
                {
                    this.props.icon ?
                        <Image
                            resizeMode="contain"
                            style={{
                                position: 'absolute',
                                width: LoadingPopup.ICON_WIDTH,
                                height: LoadingPopup.ICON_WIDTH,
                                top: LoadingPopup.ICON_PADDING,
                                left: LoadingPopup.ICON_PADDING
                            }}
                            source={this.props.icon} /> : null
                }
            </View>

        )
    }

    renderPopup() {
        return (
            <View style={{ width: widthScreen * 2 / 3, justifyContent: 'center', alignItems: 'center' }}>
                {
                    this.state.doneLoading == true ?
                        <Image style={{ width: LoadingPopup.LOADING_WIDTH, height: LoadingPopup.LOADING_WIDTH }}
                            resizeMode="contain"
                            source={require('../../../images/icon/ic_success_process_white.png')} /> :
                        this.renderLoading()
                }
                {
                    this.props.text ? <Text style={styles.text}>{this.props.text}</Text> : null
                }
                {
                    this.props.subText ? <Text style={styles.subText}>{this.props.subText}</Text> : null
                }
            </View>
        );
    }
}

var styles = StyleSheet.create({
    text: {
        color: 'white',
        marginTop: 20,
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
    subText: {
        color: 'white',
        marginTop: 20,
        textAlign: 'center',
        backgroundColor: 'transparent'
    }
});
