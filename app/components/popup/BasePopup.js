import React, { Component } from 'react';
import {
    Platform,
    Navigator,
    DeviceEventEmitter,
    Modal,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Text,
    InteractionManager,
    Animated,
    PanResponder,
    BackHandler,
    View,
    Dimensions,
    StyleSheet
} from 'react-native';

const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
export default class BasePopup extends Component {

    static POPUP_WIDTH = widthScreen - 15 * 2
    static POPUP_MINHEIGHT = widthScreen * 0.5

    static PRIORITY_BOTTOMSHEET = 0
    static PRIORITY_POPUP = 3

    shouldComponentUpdate(nextProps, nextState) {

        if (nextState != this.state || this.props != nextProps) {
            return true
        }
        return false
    }

    // static defaultProps = {
    //     cancelable: true,
    //     overlayOpacity: "CC",
    //     isUnique: false,
    //     priority: BasePopup.PRIORITY_POPUP,
    // };

    defaultState = {
        modalVisible: this.props.show,
        fadeAnim: new Animated.Value(0)
    }

    constructor(props) {
        super(props);
        this.state = {
            ...this.defaultState,
        }
    }

    componentDidMount() {
        this.mounted = true
        this.mountedTime = (new Date().getTime())
        this.startAnimAnimation()
        this.setupBackPress()
    }

    componentWillUnmount() {
        this.mounted = false
        this.removeBackPress()
    }

    startAnimAnimation() {
        Animated.timing(
            this.state.fadeAnim,
            {
                duration: 120,
                toValue: 1
            }
        ).start();
    }

    removeBackPress() {
        if (this.backButtonListener) {
            this.backButtonListener.remove()
        }
    }

    setupBackPress() {
        this.backButtonListener = BackHandler.addEventListener('hardwareBackPress', () => {
            this.requestCancel()
            return true;
        });
    }

    show() {
        this.setState({
            modalVisible: true
        })
    }

    close(callback) {
        if (this.mounted) {
            this.setState({
                modalVisible: false
            }, () => {
                this.onClose()
                if (callback) {
                    callback();
                }
            })
        }
    }

    isClose() {
        return this.state.modalVisible;
    }

    onClose() {
        if (this.props.onHideForExecute) {
            this.props.onHideForExecute()
        }
        if (this.props.onClose) {
            this.props.onClose()
        }
    }

    requestCancel() {
        if (this.props.cancelable) {
            this.close()
        }
    }

    render() {
        if (this.state.modalVisible) {
            return this.renderOverlay()
        } else {
            return null
        }
    }

    renderOverlay() {
        return (
            <Animated.View
                style={[styles.fullScreen, { opacity: this.state.fadeAnim }]}>
                <TouchableOpacity
                    pointerEvent={'box-only'}
                    activeOpacity={1}
                    style={[styles.overlay]}
                    onPress={() => {
                        this.requestCancel()
                    }}>
                </TouchableOpacity>
                <View pointerEvents={'box-none'} style={styles.container}>
                    {
                        this.renderPopup()
                    }
                </View>
            </Animated.View>
        );
    }

    renderPopup() {
        return this.props.children
    }


}

var styles = StyleSheet.create({
    overlay: {
        flex: 1,
        backgroundColor: '#00000080',
        justifyContent: 'center',
        alignItems: 'center',
    },
    fullScreen: {
        position: 'absolute', top: 0, left: 0, right: 0, bottom: 0
    },
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    popupContainer: {
        backgroundColor: '#fff',
        minHeight: BasePopup.POPUP_MINHEIGHT,
        width: BasePopup.POPUP_WIDTH,
        borderRadius: 5,
    }
});
