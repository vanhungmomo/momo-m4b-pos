import { Dimensions } from 'react-native';
let { width, height } = Dimensions.get('window');
export default {
    textinput: {
        color: 'white',
        flex: 1,
    },

    row: {
        paddingHorizontal: 40,
        paddingVertical: 5,
    }
};