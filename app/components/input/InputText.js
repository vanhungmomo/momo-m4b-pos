import React from 'react';
import { View, TextInput, Text, Image, Platform, Keyboard, TouchableOpacity } from 'react-native';
import styles from './styles';
export default class InputText extends React.Component {

    _onChangeText = (text) => {
        if (this.props.onChangeText) {
            this.props.onChangeText(text);
        }
    }

    onPress() {
        Keyboard.apply;
    }

    onPressClear(typeInput) {
        if (this.props.onPressClear) {
            this.props.onPressClear(typeInput);
        }
    }

    render() {
        let { title, style, sourceLeft, sourceRight, backgroundInputColor, color, styleIconLeft, styleIconRight, value, typeInput, borderRadius, onUnFocus, onFocus } = this.props;
        let bgColor = "#FFFFFF20";
        if (backgroundInputColor) {
            bgColor = backgroundInputColor;
        }
        return (<View style={[styles.row, style, { justifyContent: 'center' }]}>
            <View
                onPress={this.onPress}
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: bgColor,
                    borderColor: bgColor,
                    borderRadius: borderRadius ? borderRadius : 4,
                    height: 42,
                }}>

                <TextInput
                    {...this.props}
                    returnKeyType={'done'}
                    style={[styles.textinput, { fontSize: 16, paddingLeft: sourceLeft ? 40 : 20, paddingRight: sourceRight ? 40 : 20, height: 48, color: color }]}
                    onChangeText={this._onChangeText}
                    underlineColorAndroid="transparent"
                    onUnFocus={() => onUnFocus(typeInput)}
                    onFocus={() => onFocus(typeInput)}
                // value={value}
                />
                {sourceLeft ? <Image source={sourceLeft} style={[{ width: 14, height: 24, position: 'absolute', left: 12 }, styleIconLeft]} resizeMode="contain" /> : null}
                {sourceRight && value != "" ?
                    <TouchableOpacity onPress={() => this.onPressClear(typeInput)} style={[{ width: 14, height: 24, position: 'absolute', paddingRight: 30, right: 0, justifyContent: 'center' }]} >
                        <Image source={sourceRight} style={[styleIconRight]} resizeMode="contain" />
                    </TouchableOpacity>
                    : null}
            </View>
        </View>);
    }
}
