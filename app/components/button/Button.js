import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import styles from './styles';
export default class Button extends Component {

    currentTime = 0;

    onPress = () => {
        let currentMiliSecond = new Date().getTime();
        if (this.props.onPressAction && currentMiliSecond - this.currentTime > 500) {
            this.currentTime = currentMiliSecond;
            this.props.onPressAction();
        }
    }

    render() {
        let { title, style, source, backgroundInputColor, color } = this.props;
        return (<TouchableOpacity
            onPress={this.onPress}
            style={[styles.button, style]}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>);
    }
};