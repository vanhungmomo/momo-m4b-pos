import React, { Component } from 'react';
import { Dimensions, Platform } from 'react-native';
let { width, height } = Dimensions.get('window');
export default {
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    bodySearch: {
        flexDirection: 'row',
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: 34,
        overflow: 'hidden',
        flex: 1
    },
    searchView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 34,
        borderRadius: 4,
        backgroundColor: '#FFFFFF26',
        overflow: 'hidden',
        marginRight: 10,
    },
    searchImage: {
        width: 18,
        height: 18,
        marginLeft: 10,
        marginRight: 5,
        resizeMode: 'contain'
    },
    input: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        height: Platform.OS == 'android' ? 40 : 36,
        padding: 0,
        fontSize: 14,
    },
    clearBtn: {
        width: 15,
        height: 15,
        marginHorizontal: 10,
        resizeMode: 'contain'
    }
};