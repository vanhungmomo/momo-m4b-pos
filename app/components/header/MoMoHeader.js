import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Dimensions, StatusBar, Platform, BackHandler, TextInput } from 'react-native';

import styles from './styles';
// import configs from '../../common/configs';
let { width, height } = Dimensions.get('window');
import { NavigationActions } from 'react-navigation';

export default class MoMoHeader extends Component {

    state = {
        textSearch: ""
    }

    _isMounted = false;

    componentDidMount() {
        let Me = this;
        this._isMounted = true;
        if (Platform.OS === 'android') {
            Me.backButtonListener = BackHandler.addEventListener('hardwareBackPress', () => {
                if (Me.props && Me.props.navigation) {
                    if (Me.props.popToTop) {
                        Me.props.navigation.popToTop();
                        return true;
                    } else if (Me.props.handleReloadCamera) {
                        Me.props.handleReloadCamera();
                        Me.props.navigation.goBack();
                        return true;
                    }
                    else {
                        Me.props.navigation.goBack();
                        return true;
                    }
                }
            });
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
        if (Platform.OS === 'android') {
            this.backButtonListener.remove();
        }
    }

    onHandleBack = () => {
        if (this.props && this.props.navigation) {
            if (this.props.popToTop) {
                this.props.navigation.popToTop();
            } else if (this.props.handleReloadCamera) {
                this.props.handleReloadCamera();
                this.props.navigation.goBack();
            } else {
                this.props.navigation.goBack();
            }
        }
    }

    onPressLogout = () => {
        if (this.props.onPressLogout) {
            this.props.onPressLogout();
        }
    }

    onTextChange = (value) => {
        if (!this._isMounted) {
            return;
        }
        this.setState({ textSearch: value }, () => {
            this.props.onTextChange(value);
        });
    }

    resetText = () => {
        if (!this._isMounted) {
            return;
        }
        this.setState({ textSearch: "" }, () => {
            this.props.onTextChange("");
        });
    }

    render() {
        let { hadBack, title, borderBottomColor, isShowSearch, titleSearch, isLogout } = this.props;
        return (
            <View style={{ backgroundColor: '#b0006d', borderBottomColor: "#000", width: width, height: 44, flexDirection: 'row' }} >
                {
                    hadBack ?
                        <TouchableOpacity onPress={this.onHandleBack} style={{ paddingHorizontal: 15, justifyContent: 'center' }}>
                            <Image source={require('../../images/icon/ic_back.png')} />
                        </TouchableOpacity>
                        :
                        null
                }
                {
                    isShowSearch ?
                        <View style={styles.bodySearch}>
                            <View style={styles.searchView}>
                                <Image source={require('../../images/icon/searchbox_ic.png')} style={styles.searchImage} />
                                <TextInput
                                    onFocus={this.onFocus}
                                    keyboardType={"numeric"}
                                    ref="textInput"
                                    style={styles.input}
                                    value={this.state.textSearch}
                                    returnKeyType={'done'}
                                    placeholder={titleSearch}
                                    placeholderTextColor={'#ffffff80'}
                                    underlineColorAndroid='transparent'
                                    onChangeText={this.onTextChange}
                                />
                                {
                                    this.state.textSearch.length > 0 ?
                                        <TouchableOpacity
                                            onPress={this.resetText}>
                                            <Image source={require('../../images/icon/ic_clear.png')}
                                                style={styles.clearBtn} />
                                        </TouchableOpacity> : null
                                }
                            </View>
                        </View>
                        :
                        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                            <Text style={{ fontSize: 18, color: "#FFF" }}>
                                {title ? title : ""}
                            </Text>
                        </View>
                }
                {
                    isLogout ?
                        <TouchableOpacity onPress={this.onPressLogout} style={{ paddingHorizontal: 15, paddingVertical: 15 }}>
                            <Image source={require('../../images/icon/ic_header_logout.png')} style={{ width: 22, height: 22 }} />
                        </TouchableOpacity>
                        :
                        !isShowSearch ?
                            <View style={{ paddingHorizontal: 15 }}>
                                <Image source={require('../../images/icon/ic_none.png')} />
                            </View>
                            : null
                }

            </View>);
    }
}