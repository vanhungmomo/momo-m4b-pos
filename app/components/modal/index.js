import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import Modal from 'react-native-modal';
export default class MoMoModal extends Component {

    callbackCancel = null;
    callbackConfirm = null;
    _isMounted = false;

    state = {
        content: "",
        titleConfirm: "",
        titleCancel: "",
        title: "",
        modalVisible: false
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    showModal(title = "", content, titleConfirm = "", titleCancel = "", callbackCancel, callbackConfirm) {
        this.callbackCancel = callbackCancel;
        this.callbackConfirm = callbackConfirm;
        if (!this._isMounted) {
            return;
        }
        this.setState({
            title,
            titleConfirm,
            titleCancel,
            content,
            modalVisible: true
        });
    }

    onCancel = () => {
        let Me = this;
        if (!this._isMounted) {
            return;
        }
        Me.setState({ modalVisible: false }, () => {
            if (Me.callbackCancel) {
                Me.callbackCancel();
                Me.callbackCancel = null;
            }
        });
    }

    onConfirm = () => {
        let Me = this;
        if (!this._isMounted) {
            return;
        }
        Me.setState({ modalVisible: false }, () => {
            setTimeout(() => {
                if (Me.callbackConfirm) {
                    Me.callbackConfirm();
                    Me.callbackConfirm = null;
                }
            }, 200);
        });
    }

    render() {
        let { titleConfirm, titleCancel, content, title, modalVisible } = this.state;
        return (
            <Modal
                backdropColor={'#000'}
                isVisible={modalVisible}
                animationType={'fade'}
                style={styles.container} >
                <View style={{ backgroundColor: 'white', paddingTop: 15 }}>
                    <View style={{ alignItems: 'flex-start', paddingHorizontal: 15 }}>
                        {
                            title != "" ?
                                <Text style={{ fontSize: 20, color: '#212121', textAlign: 'left', marginBottom: 5 }}>{title}</Text>
                                : null
                        }
                        <Text style={{ fontSize: 16, color: '#212121', textAlign: 'left' }}>{content}</Text>
                    </View>
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', flexDirection: 'row' }}>
                        {
                            titleCancel != "" ?
                                <TouchableOpacity
                                    onPress={this.onCancel}
                                    style={{
                                        backgroundColor: 'transparent',
                                        paddingHorizontal: 15,
                                        paddingVertical: 15
                                    }}><Text
                                        style={{
                                            fontSize: 18,
                                            color: '#8f8e94',
                                        }}>{titleCancel}</Text>
                                </TouchableOpacity>
                                : null
                        }
                        {
                            titleConfirm != "" ?
                                <TouchableOpacity
                                    onPress={this.onConfirm}
                                    style={{
                                        backgroundColor: 'transparent',
                                        paddingHorizontal: 15,
                                        paddingVertical: 15
                                    }}>
                                    <Text style={{
                                        fontSize: 18,
                                        color: '#b0006d',
                                    }} >{titleConfirm}</Text>
                                </TouchableOpacity>
                                : null
                        }
                    </View>
                </View>
            </Modal>
        );
    }
}