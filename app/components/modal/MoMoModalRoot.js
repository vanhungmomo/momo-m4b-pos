import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, Dimensions, ScrollView, StatusBar } from 'react-native';
import styles from './styles';
import Modal from 'react-native-modal';
import configs from '../../common/configs';
export default class MoMoModal extends Component<props> {

    state = {
        modalVisible: false,
        hidden: false
    }
    _isMounted = false;

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    openModal() {
        if (!this._isMounted) {
            return;
        }
        this.setState({ modalVisible: true, hidden: true });
    }

    closeModal(callback) {
        if (!this._isMounted) {
            return;
        }
        this.setState({ modalVisible: false, hidden: false }, () => { callback() });
    }

    renderMain() {
        return null;
    }

    render() {
        let { hidden } = this.props;
        return (<Modal
            supportedOrientations={['portrait']}
            backdropColor={'#000'}
            isVisible={this.state.modalVisible}
            animationType={'fade'}
            style={styles.container}>
            <StatusBar backgroundColor="rgba(0, 0, 0, 0.2)" barStyle={'light-content'} animated={true} hidden={false} />
            {this.renderMain()}
        </Modal>);
    }
}
