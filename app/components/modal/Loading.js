import React from 'react';
import { View } from 'react-native';
import { CirclesLoader, PulseLoader, TextLoader, DotsLoader, LineDotsLoader } from 'react-native-indicator';
import styles from './styles';
export default class Loading extends React.Component {

    render() {
        return (
            <View style={[styles.container, { alignItems: 'center', backgroundColor: '#b0006d' }]}>
                <LineDotsLoader size={12} color={"#FFF"} />
                <TextLoader textStyle={{ color: '#FFF', marginTop: 15, fontSize: 16 }} text="Dữ liệu đang được cập nhật" />
            </View>
        );
    }
}