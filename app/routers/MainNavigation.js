import { NavigationActions } from 'react-navigation'
const resetAction = (routeName) => NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: routeName, drawer: 'close' }),
    ]
});

export default {
    resetAndGotoScreen(navigation, screen) {
        navigation.dispatch(resetAction(screen))
    }
}