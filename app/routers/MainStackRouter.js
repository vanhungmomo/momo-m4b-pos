import { StackNavigator } from "react-navigation";
import splashScreen from '../screens/login/SplashScreen';
import login from '../screens/login';
import home from '../screens/home';
import scanQRCode from '../screens/transaction/ScanQRCode';
import transactionInit from '../screens/transaction/TransactionInit';
import transactionConfirm from '../screens/transaction/TransactionConfirm';
import transactionResult from '../screens/transaction/TransactionResult';
import transactionDetail from '../screens/transaction/TransactionDetail';
import transactionCheck from '../screens/transaction/TransactionCheck';
import setting from '../screens/setting';
import history from '../screens/history';
import transactionScan from '../screens/transaction/TransactionScan';
import transactionScanSensor from '../screens/transaction/TransactionScanSensor';
import transactionRefund from '../screens/transaction/TransactionRefund';
import activeCode from '../screens/login/ActiveCode';
import generateQRCode from '../screens/transaction/GenerateQRCode';
export default (StackNav = StackNavigator({
    splashScreen: { screen: splashScreen },
    login: { screen: login },
    home: { screen: home },
    scanQRCode: { screen: scanQRCode },
    transactionInit: { screen: transactionInit },
    transactionConfirm: { screen: transactionConfirm },
    transactionResult: { screen: transactionResult },
    transactionDetail: { screen: transactionDetail },
    transactionCheck: { screen: transactionCheck },
    setting: { screen: setting },
    history: { screen: history },
    transactionScan: { screen: transactionScan },
    transactionScanSensor: { screen: transactionScanSensor },
    transactionRefund: { screen: transactionRefund },
    activeCode: { screen: activeCode },
    generateQRCode: { screen: generateQRCode }
}));