import React, { Component } from 'react';
import { View } from 'react-native';
import App from './App';
import PopupManager from './components/popup/PopupManager';
import RootPopupLayer from './components/popup/RootPopupLayer';
class setup extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <App />
                <RootPopupLayer ref={(rootPopupLayer) => {
                    if (rootPopupLayer) {
                        PopupManager.popupLayer = rootPopupLayer
                    }
                }} />
            </View>
        );
    }
}
export default setup;

