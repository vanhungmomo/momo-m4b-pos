module.exports = {
    get BarCodeScanner() {
        return require('./scanqrcode/BarCodeScanner');
    },
    get Permissions() {
        return require('./permissions/Permissions');
    },
};
