import React, { Component } from 'react';
import { requireNativeComponent, View } from 'react-native';
import PropTypes from "prop-types";
var ScannerSensor = requireNativeComponent('ScannerSensor', ScannerView);

class ScannerView extends Component {

    _onClickWith = (event) => {
        let data = event;
        if (!this.props.onClickWith) {
            return;
        }
        this.props.onClickWith(event);
    }

    render() {
        return (
            <ScannerSensor
                {...this.props} onClickWith={this._onClickWith} />
        );
    }
}

ScannerView.propTypes = {
    ...View.propTypes,
    onClickWith: PropTypes.func,
    isResume: PropTypes.bool
};

module.exports = ScannerView;
