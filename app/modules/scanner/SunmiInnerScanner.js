import React, { Component } from 'react';
import {
    NativeModules,
    requireNativeComponent,
    DeviceEventEmitter
} from 'react-native';
const SunmiInnerScanner = NativeModules.SunmiInnerScanner;
export default SunmiInnerScanner;
export class SunmiScannerView extends Component {
    componentWillMount() {
        if (this.props.onCodeScan) {
            this.cameraBarCodeReadListener = DeviceEventEmitter.addListener('SunmiInnerScannerView.RESULT',
                (data) => {
                    this.props.onCodeScan(data.result || []);
                });
        }
    }
    componentWillUnmount() {
        if (this.cameraBarCodeReadListener) {
            this.cameraBarCodeReadListener.remove();
        }
    }
    render() {
        return <SunmiScanner {...this.props} />;
    }
}
const SunmiScanner = requireNativeComponent('SunmiScanner', SunmiScannerView);
