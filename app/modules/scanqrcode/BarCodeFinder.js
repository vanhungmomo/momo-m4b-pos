import React, { Component, PropTypes, } from 'react';
import { ActivityIndicator, Dimensions, StyleSheet, Text, View } from 'react-native';

const { height, width } = Dimensions.get('window');
const widthCamera = width - 120;
const heightCamera = width - 120;

class BarCodeFinder extends Component {

    // static propTypes = {
    //     ...View.propTypes,
    //     backgroundColor: PropTypes.string,
    //     borderWidth: PropTypes.number,
    //     borderLength: PropTypes.number,
    //     edgeColor: PropTypes.string,
    //     height: PropTypes.number,
    //     isLoading: PropTypes.bool,
    //     width: PropTypes.number,
    // };

    static defaultProps = {
        backgroundColor: 'transparent',
        borderWidth: 2,
        borderLength: 30,
        edgeColor: '#DADADA',
        height: heightCamera,
        isLoading: false,
        width: widthCamera,
    };

    getBackgroundColor = () => {
        return ({
            backgroundColor: this.props.backgroundColor,
        });
    };

    getEdgeColor = () => {
        return ({
            borderColor: this.props.edgeColor,
        });
    };

    getSizeStyles = () => {
        return ({
            height: this.props.height,
            width: this.props.width,
        });
    };

    getEdgeSizeStyles = () => {
        return ({
            height: this.props.borderLength,
            width: this.props.borderLength,
        });
    };

    renderLoadingIndicator = () => {
        if (this.props.isLoading) {
            return (
                <ActivityIndicator
                    animating={this.props.isLoading}
                    color={this.props.color}
                    size='large'
                />
            );
        } else {
            return null;
        }
    };

    render() {
        return (
            <View style={[styles.container]}>
                <View style={{ flex: 1, backgroundColor: 'transparent' }} />
                <View style={{ backgroundColor: 'transparent', flexDirection: 'row' }}>
                    <View style={{ flex: 1, backgroundColor: 'transparent' }} />
                    <View style={[styles.finder, this.getSizeStyles()]}>
                        <View style={[this.getEdgeSizeStyles(), styles.topLeftEdge, {
                            borderLeftWidth: this.props.borderWidth,
                            borderTopWidth: this.props.borderWidth,
                        }]} />
                        <View style={[this.getEdgeSizeStyles(), styles.topRightEdge, {
                            borderRightWidth: this.props.borderWidth,
                            borderTopWidth: this.props.borderWidth,
                        }]} />
                        {this.renderLoadingIndicator()}
                        <View style={[this.getEdgeSizeStyles(), styles.bottomLeftEdge, {
                            borderLeftWidth: this.props.borderWidth,
                            borderBottomWidth: this.props.borderWidth,
                        }]} />
                        <View style={[this.getEdgeSizeStyles(), styles.bottomRightEdge, {
                            borderRightWidth: this.props.borderWidth,
                            borderBottomWidth: this.props.borderWidth,
                        }]} />
                    </View>
                    <View style={{ flex: 1, backgroundColor: 'transparent' }} />
                </View>
                <View style={{ flex: 1, backgroundColor: 'transparent' }}>
                    <Text style={styles.label}>{'Di chuyển camera để thấy rõ mã QR\nvà xác nhận thanh toán'}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    finder: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
    },
    topLeftEdge: {
        borderColor: '#DADADA',
        position: 'absolute',
        top: 10,
        left: 10,
    },
    topRightEdge: {
        borderColor: '#DADADA',
        position: 'absolute',
        top: 10,
        right: 10,
    },
    bottomLeftEdge: {
        borderColor: '#DADADA',
        position: 'absolute',
        bottom: 10,
        left: 10,
    },
    bottomRightEdge: {
        borderColor: '#DADADA',
        position: 'absolute',
        bottom: 10,
        right: 10,
    },
    label: {
        flex: 1,
        color: 'white',
        textAlign: 'center',
        fontSize: 14
    }
});

module.exports = BarCodeFinder;
