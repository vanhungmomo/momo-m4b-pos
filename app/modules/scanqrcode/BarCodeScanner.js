import React, { Component, PropTypes } from 'react';
import ReactNative, {
    NativeModules,
    requireNativeComponent,
    View,
    UIManager,
    Platform
} from 'react-native';

import BarCodeFinder from './BarCodeFinder';

const BarCodeScannerManager = NativeModules.ExponentBarCodeScannerManager || NativeModules.ExponentBarCodeScannerModule;
const ExponentBarCodeScanner = requireNativeComponent(
    'ExponentBarCodeScanner',
    BarCodeScanner,
    {
        nativeOnly: {
            onBarCodeRead: true,
        },
    }
);
const EventThrottleMs = 500;

function convertNativeProps(props) {
    const newProps = { ...props };

    if (typeof props.torchMode === 'string') {
        newProps.torchMode = BarCodeScanner.Constants.TorchMode[props.torchMode];
    }

    if (typeof props.type === 'string') {
        newProps.type = BarCodeScanner.Constants.Type[props.type];
    }

    return newProps;
}

class BarCodeScanner extends Component {
    static Constants = {
        BarCodeType: BarCodeScannerManager.BarCodeType,
        Type: BarCodeScannerManager.Type,
        TorchMode: BarCodeScannerManager.TorchMode,
    };

    // static propTypes = {
    //     ...View.propTypes,
    //     onBarCodeRead: PropTypes.func,
    //     barCodeTypes: PropTypes.array,
    //     torchMode: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    //     type: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    // };

    static defaultProps = {
        type: BarCodeScannerManager.Type.back,
        torchMode: BarCodeScannerManager.TorchMode.off,
        barCodeTypes: Object.values(BarCodeScannerManager.BarCodeType),
    };

    setNativeProps(props) {
        const nativeProps = convertNativeProps(props);
        if (this._component) {
            this._component.setNativeProps(nativeProps);
        }
    }

    startCamera() {
        if (Platform.OS === 'android') {
            UIManager.dispatchViewManagerCommand(
                ReactNative.findNodeHandle(this),
                UIManager.ExponentBarCodeScanner.Commands.startCamera,
                [],
            );
        } else {
            BarCodeScannerManager.startCamera();
        }
    }

    stopCamera() {
        if (Platform.OS === 'android') {
            UIManager.dispatchViewManagerCommand(
                ReactNative.findNodeHandle(this),
                UIManager.ExponentBarCodeScanner.Commands.stopCamera,
                [],
            );
        } else {
            BarCodeScannerManager.stopCamera();
        }
    }

    readImageQRCode(base64) {
        if (Platform.OS === 'android') {
            UIManager.dispatchViewManagerCommand(
                ReactNative.findNodeHandle(this),
                UIManager.ExponentBarCodeScanner.Commands.readImageQRCode,
                [base64],
            );
        } else {
            BarCodeScannerManager.readImageQRCode(base64);
        }
    }

    render() {
        const nativeProps = convertNativeProps(this.props);

        return (
            <ExponentBarCodeScanner
                {...nativeProps}
                ref={this._setRef}
                onBarCodeRead={this._onBarCodeRead}>
                <BarCodeFinder />
            </ExponentBarCodeScanner>
        );
    }

    _setRef = component => {
        this._component = component;
    };

    _onBarCodeRead = ({ nativeEvent }) => {
        if (
            this._lastEvent &&
            JSON.stringify(nativeEvent) === this._lastEvent &&
            new Date() - this._lastEventTime < EventThrottleMs
        ) {
            return;
        }

        if (this.props.onBarCodeRead) {
            this.props.onBarCodeRead(nativeEvent);
            this._lastEvent = JSON.stringify(nativeEvent);
            this._lastEventTime = new Date();
        }
    };
}

module.exports = BarCodeScanner;
