/**
 * Local Storage Key
 */
export const USER_PROFILE = 'USER_PROFILE';
export const USER_NAME = 'USER_NAME';
export const JWT_TOKEN = 'JWT_TOKEN';
export const ACTIVE_SIGNATURE = 'activeSignature';