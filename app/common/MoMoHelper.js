import React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import cache from './cache'
var cryptojsmd5 = require('crypto-js/md5');
export default {
    formatMoneyToNumber(money) {
        try {
            if (money) {
                var moneyString = money.replaceAll(',', '').replaceAll('đ', '').replaceAll('.', '').replaceAll(' ', '');
                var number = Number(moneyString);
                if (isNaN(number)) {
                    return 0;
                }
                return number
            } else {
                return money;
            }

        } catch (error) {
            return money;
        }
    },

    formatCurrency(amount) {
        let valueChange = "";
        if (amount == "") {
            return amount;
        } else {
            let amountValue = this.formatMoneyToNumber(amount).toString();
            if (amountValue.length > 8) {
                amountValue = amountValue.substring(0, 8);
            }
            return this.formatNumberToMoney(amountValue);
        }
    },

    formatNumberToMoney(number, defaultNum, predicate) {
        predicate = !predicate ? "" : "" + predicate;
        if (number == 0 || number == '' || number == null || number == 'undefined' ||
            isNaN(number) === true ||
            number == '0' || number == '00' || number == '000')
            return "0" + predicate;

        var array = [];
        var result = '';
        var count = 0;

        if (!number) {
            return defaultNum ? defaultNum : "" + predicate
        }

        let flag1 = false;
        if (number < 0) {
            number = -number;
            flag1 = true;
        }

        var numberString = number.toString();
        if (numberString.length < 3) {
            return numberString + predicate;
        }

        for (let i = numberString.length - 1; i >= 0; i--) {
            count += 1;
            if (numberString[i] == "." || numberString[i] == ",") {
                array.push(',');
                count = 0;
            } else {
                array.push(numberString[i]);
            }
            if (count == 3 && i >= 1) {
                array.push('.');
                count = 0;
            }
        }

        for (let i = array.length - 1; i >= 0; i--) {
            result += array[i];
        }

        if (flag1)
            result = "-" + result;

        return result + predicate;
    },

    resetAndGotoPage(navigation, routeName, param) {
        navigation.dispatch(StackActions.reset({
            index: 0,
            key: null,
            actions: [
                NavigationActions.navigate({
                    routeName: routeName,
                    params: param
                })
            ]
        }));
    },

    /**
	 * Get single item from AsyncStorage, if it has on cache, get from cache
	 * @param {*} key 
	 */
    _getCached(key) {
        try {
            if ((typeof cache[key] === 'string' && cache[key] && cache[key] != '') ||
                typeof cache[key] === 'object' && Object.keys(cache[key]).length) {
                var val = cache[key];
                return new Promise(function (resolve) {
                    resolve(val);
                })
            } else {
                return AsyncStorage.getItem(key).then((storageResult) => {
                    var data = storageResult;
                    if (this._isJsonString(storageResult)) {
                        data = JSON.parse(storageResult);
                    }
                    cache[key] = data;
                    return data;
                });
            }
        } catch (e) {
            return new Promise(function (resolve) {
                resolve('');
            })
        }
    },

	/**
	 * Set a key and value to AsyncStorage
	 * @param {*} key 
	 * @param {*} data 
	 */
    _setCached(key, data) {
        var storageData = data;
        if (data && typeof data === 'string') {
            cache[key] = data;
        } else if (data && typeof data === 'object') {
            cache[key] = data;
            storageData = JSON.stringify(data);
        } else {
            cache[key] = data;
        }
        console.log("AsyncStorage save data " + key + " data " + storageData);
        return AsyncStorage.setItem(key, storageData);
    },

	/**
	 * try to clean all data from AsyncStorage
	 */
    _clearCache() {
        AsyncStorage.getAllKeys((err, keys) => {
            console.log("remove_all_key " + keys);
            AsyncStorage.multiRemove(["JWT_TOKEN", "USER_NAME", "USER_PROFILE"]);
        });
    },

	/**
	 * check is a JSON String or not
	 * @param {*} str 
	 */
    _isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    },

    buildFormData(arrData, callback = null) {
        let layout = [];
        for (let index = 0; index < arrData.length; index++) {
            let borderWidth = 0;
            let marginTop = 2;
            let marginBottom = 3;
            let paddingBottom = 0;
            let paddingTop = 0;
            if (index == 0) {
                paddingTop = 8;
            }
            if (arrData[index].border) {
                borderWidth = 1;
                paddingBottom = 10;
            }
            if (arrData[index].marginTop) {
                marginTop = arrData[index].marginTop;
            }
            let viewItem = null;
            let value = arrData[index].value;
            if (arrData[index].formatAmount) {
                value = this.formatNumberToMoney(value, "0", "đ");
            }
            if (arrData[index].transDetails) {
                viewItem = <TouchableOpacity onPress={callback}
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        marginTop: marginTop,
                        marginBottom: marginBottom,
                        paddingTop: paddingTop,
                        paddingBottom: paddingBottom,
                        marginHorizontal: 15,
                        borderBottomWidth: borderWidth,
                        borderBottomColor: "#dcdcdc",
                        marginBottom: -10,
                        paddingBottom: 10,
                    }} key={"item_" + index}>
                    <Text style={{ fontSize: 16, color: '#929292', flex: 1 }}>{arrData[index].key}</Text>
                    <Text style={{ fontSize: 16, color: '#4d4d4d', textAlign: 'right' }}>{value}</Text>
                </TouchableOpacity>
            } else {
                viewItem = <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: marginTop, marginBottom: marginBottom, paddingTop: paddingTop, paddingBottom: paddingBottom, marginHorizontal: 15, borderBottomWidth: borderWidth, borderBottomColor: "#dcdcdc" }} key={"item_" + index}>
                    <Text style={{ fontSize: 16, color: '#929292', flex: 0.35 }}>{arrData[index].key}</Text>
                    <Text style={{ fontSize: 16, color: '#4d4d4d', textAlign: 'right', flex: 0.65 }}>{value}</Text>
                </View>
            }
            layout.push(viewItem);
        }
        return layout;
    },

    getColorStatus(resultTran) {
        let colorSuccess = "#36BC99";
        let colorFail = "#8F8E94";
        if (resultTran == 0) {
            return colorSuccess;
        }
        else {
            return colorFail;
        }
    },

    getNameStatus(resultTran) {
        if (resultTran == 0) {
            return "Thành công";
        } else {
            return "Thất bại";
        }
    },

    replaceAt(string, index, replacement) {
        return string.substr(0, index) + replacement + string.substr(index + replacement.length);
    },

    formatPhone(phone) {
        return this.replaceAt(phone, 4, "***");
    },

    // if (data.amount) {
    //     newItem.value = MoMoHelper.formatNumberToMoney(data.amount, null, "đ");
    // } else {
    //     newItem.value = "";
    // }

    mapValueByKey(key, value) {
        let newValue = "";

        return newValue;
    },

    removeAlias(alias) {
        var str = alias;
        //str= str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");
        //str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");
        return str.replace(/[^\x00-\x7F]/g, "");
    },

    requestId() {
        // let activeSignature = cache.ACTIVE_SIGNATURE;
        // let requestId = "";
        // if (activeSignature && activeSignature != "") {
        //     requestId = cryptojsmd5("MM_" + activeSignature + '_' + new Date().getTime()) + "";
        // }
        // if (requestId == "") {
        //     requestId = cryptojsmd5("MM_" + new Date().getTime()) + "";
        // }
        return cryptojsmd5("MM_" + new Date().getTime()) + "";
    }
}