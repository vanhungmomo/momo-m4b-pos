/** Component */
import cache from './cache'
import configs from './configs';
import MoMoReactToNative from '../modules/MoMoReactToNative';
import PopupManager from '../components/popup/PopupManager'
export default {
    _getRequestBody(param) {
        var formBody = [];
        for (var property in param) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(param[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        return formBody.join("&");
    },

    fetchGet: (params, callback, navigation = null, URL = null, showLoading = null, timeoutInit = null) => {
        if (showLoading) {
            MoMoReactToNative.showLoading();
        }
        let token = cache.JWT_TOKEN;
        let URL_Request = configs.URL_ROOT;
        if (URL) {
            URL_Request = URL_Request + URL + params;
        }
        var body;
        let headers = {
            'Content-Type': 'application/json'
        };
        if (token && token != "") {
            headers.Authorization = 'Bearer ' + token;
        }
        let timeoutPost = 2 * 30 * 1000;
        if (timeoutInit) {
            timeoutPost = timeoutInit;
        }
        console.log("data_request url = " + URL_Request);
        timeout(timeoutPost, fetch(URL_Request, { method: 'GET', headers, body }))
            // .then((response) => response.json())
            .then((result) => {
                console.log("data_response  " + result);
                callback(result);
                if (showLoading) {
                    MoMoReactToNative.hideLoading();
                }
            }).catch((error) => {
                console.log("data_response error " + error);
                callback(null);
                if (showLoading) {
                    MoMoReactToNative.hideLoading();
                }
                if (error && error == "Connection error") {
                    MoMoReactToNative.showAlertNative(
                        "Lỗi kết nối",
                        "Không thể kết nối đến máy chủ. Vui lòng thử lại sau.",
                        "",
                        "ĐỒNG Ý",
                        false,
                        () => {

                        }
                    );
                }
            }).done();

        function timeout(ms, promise) {
            return new Promise((resolve, reject) => {
                const timeoutId = setTimeout(() => {
                    reject(new Error("Request timeout"))
                }, ms);
                promise.then(
                    (res) => {
                        clearTimeout(timeoutId);
                        resolve(res);
                    },
                    (error) => {
                        console.log("data_response error " + error);
                        clearTimeout(timeoutId);
                        callback(null);
                    }
                );
            })
        }
    },


    fetchPost(params, callback, paramOption = {}) {
        let { navigation, showLoading, transaction, gotoTrans, isCheckLogin, timeoutInit, nonCheckNet, handleReloadCamera, URL } = paramOption;
        if (showLoading) {
            PopupManager.showLoading();
        }
        if (transaction) {
            PopupManager.showLoading("Đang thực hiện giao dịch", "Thông tin thanh toán đã được mã hoá an toàn trước khi thực hiện giao dịch", require('../images/icon/ic_shield_transaction.png'));
        }
        let token = cache.JWT_TOKEN;
        let URL_Request = configs.URL_ROOT;
        if (URL) {
            URL_Request = URL_Request + URL;
        }
        let headers = {
            'Content-Type': 'application/json'
        };
        if (token && token != "" && !isCheckLogin) {
            headers.Authorization = 'Bearer ' + token;
        }
        let timeoutPost = 2 * 30 * 1000;
        if (timeoutInit) {
            timeoutPost = timeoutInit;
        }
        console.log("data_request URL_Request = " + URL_Request);
        console.log("data_request token = " + token);
        console.log("data_request data = " + JSON.stringify(params));
        let routerName = params.routerName;
        timeout(timeoutPost, fetch(URL_Request, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(params)
        })).then((response) => response.json())
            .then((result) => {
                console.log("data_response " + JSON.stringify(result));
                PopupManager.hideLoading();
                if (navigation && result.resultCode == 403) {
                    if (gotoTrans) {
                        callback(result);
                    } else {
                        this.showAlert("", "Quý khách đã hết thời gian hay không quyền truy cập. Xin vui lòng đăng nhập lại.", "ĐỒNG Ý", () => {
                            let parameters = {};
                            parameters.sessionExp = true;
                            parameters.routerName = routerName;
                            navigation.navigate("login", parameters);
                        });
                    }
                } else {
                    callback(result);
                }
            })
            .catch((error) => {
                console.log("data_response error " + error);
                if (showLoading || transaction) {
                    PopupManager.hideLoading();
                }
                if (error && error == "Error: Connection error") {
                    if (!nonCheckNet)
                        this.showAlert("Lỗi kết nối", "Không thể kết nối đến máy chủ. Vui lòng thử lại sau.", "ĐỒNG Ý", () => {
                            if (handleReloadCamera) {
                                handleReloadCamera();
                            }
                        });
                } else {
                    callback(false);
                }
            }).done();

        function timeout(ms, promise) {
            return new Promise((resolve, reject) => {
                const timeoutId = setTimeout(() => {
                    reject(new Error("Request timeout"))
                }, ms);
                promise.then(
                    (res) => {
                        clearTimeout(timeoutId);
                        resolve(res);
                    },
                    (err) => {
                        clearTimeout(timeoutId);
                        reject(new Error("Connection error"));
                    }
                );
            })
        }
    }
}