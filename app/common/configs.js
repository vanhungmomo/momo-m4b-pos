// let URL_MAIN = "http://10.10.18.103:9000/";
// let URL_MAIN = "http://10.10.18.103:7777/";
// let URL_MAIN = "https://testing.momo.vn:7777/";
let URL_MAIN = "http://10.20.12.14:7777/";
// let URL_MAIN = "https://business-api.momo.vn/";
export default {
    backgroundColor: '#b0006d',
    statusBarColor: '#b0006d',
    textColor: "#4d4d4d",
    URL_ROOT: URL_MAIN + "api/m4b-pos/",
    URL_ROOT_IMG: URL_MAIN + "extAssests/",
    URL_LOGIN: "bus-login",
    URL_CONFIRM_TRANS: "posTransaction",
    URL_CHECK_TRANS: "checkTrans",
    URL_TRAN_HIST: "transactionHistory",
    URL_PRINT_BILL: "printBill",
    URL_LOGOUT: "logout",
    URL_PRINT_BILL_IMAGE: "bill/",
    URL_VERIFIED: "verified",
    URL_REFUND_TRANS: "refundTransaction",
    URL_ACTIVE: "active",
    URL_STATIC_QR: "static-qr/generate",
    URL_CHECK_BY_BILL: "checkByBillID"
};