import React from 'react';
import { View, StatusBar, Image, FlatList, AppState, Alert } from 'react-native';
import configs from '../../common/configs';
import ItemHome from './ItemHome';
import DataApp from '../../data/DataApp';
import cache from '../../common/cache';
import MoMoHelper from '../../common/MoMoHelper';
import * as ckey from '../../common/StorageKey';
import LoginApi from '../login/LoginApi';
export default class Home extends React.Component {
    static navigationOptions = {
        header: null
    }

    componentDidMount() {
        this._isMounted = false;
        AppState.addEventListener('change', this.handleAppStateChange);
    }

    handleAppStateChange = async (state) => {
        if (state === 'active') {
            let profile = await MoMoHelper._getCached(ckey.USER_PROFILE);
            if (profile) {
                LoginApi.handleWakeUp({}, (result) => {
                    if (result) {
                        if (result.resultCode == 403 && !this.isShowPopup) {
                            this.isShowPopup = true;
                            Alert.alert(
                                "",
                                "Quý khách đã hết thời gian hay không quyền truy cập. Xin vui lòng đăng nhập lại.",
                                [
                                    {
                                        text: "ĐỒNG Ý", onPress: () => {
                                            this.isShowPopup = false;
                                            MoMoHelper._clearCache();
                                            MoMoHelper._setCached(ckey.USER_PROFILE, "");
                                            MoMoHelper.resetAndGotoPage(this.props.navigation, "login", null);
                                        }
                                    },
                                ],
                                { cancelable: false });
                        }
                    }
                });
            }
        }
    }

    onPress = (item) => {
        let parameters = {};
        if (item.redirectTo == "login") {
            parameters.isCheckLogin = true;
            parameters.gotoTrans = true;
        }
        this.props.navigation.navigate(item.redirectTo, parameters);
    }

    keyExtractor = (item, index) => item.id;

    _renderItem = ({ item, index }) => {
        return (<ItemHome
            item={item}
            key={index}
            index={index}
            onPress={this.onPress}
        />);
    }

    render() {
        let dataHome = DataApp.dataHome
        // let profile = cache.USER_PROFILE;
        // if (profile && profile.extraData) {
        //     let extraData = JSON.parse(profile.extraData);
        //     if (extraData && extraData.m4b_parking_ticket) {
        //         let item = { ...DataApp.dataHome[0] };
        //         item.name = "THANH TOÁN";
        //         item.redirectTo = "generateQRCode";
        //         dataHome[0] = item;
        //     }
        // }
        return (
            <View style={{ backgroundColor: configs.backgroundColor, flex: 1 }}>
                <StatusBar backgroundColor={configs.backgroundColor} barStyle={'light-content'} animated={true} />
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../images/logo/momo_logo.png')} style={{ width: 70, height: 70, marginTop: 50, alignItems: 'center' }} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                    <FlatList
                        ref={"flHome"}
                        data={dataHome}
                        scrollEnabled={true}
                        numColumns={2}
                        keyExtractor={this.keyExtractor}
                        renderItem={this._renderItem} />
                </View>
            </View>
        );
    }
}