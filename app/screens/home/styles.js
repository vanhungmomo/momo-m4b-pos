import React, { Component } from 'react';
import { Dimensions } from 'react-native';
let { width, height } = Dimensions.get('window');
export default {
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    grid: {
        backgroundColor: '#8d015a',
        marginLeft: 20,
        width: width / 2 - 35,
        height: width / 2 - 35,
        // width: height / 3,
        // height: height / 3,
        borderRadius: 10,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
    }
};