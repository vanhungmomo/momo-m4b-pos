import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import styles from './styles';
export default class ItemHome extends Component {

    onPress = () => {
        if (this.props.onPress) {
            this.props.onPress(this.props.item);
        }
    }

    render() {
        let { index, item } = this.props;
        let { name, icon, iconWidth, iconHeight } = item;
        let marginRight = index % 2 ? { marginRight: 20 } : { marginRight: 0 };
        return (
            <TouchableOpacity onPress={this.onPress} style={[styles.grid, marginRight]}>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
                    <Image source={icon} style={{ width: iconWidth, height: iconHeight }} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{
                        fontSize: 18,
                        color: "#FFF",
                        textAlign: 'center'
                    }}>{name}</Text>
                </View>
            </TouchableOpacity>);
    }
}