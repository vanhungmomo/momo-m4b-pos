import HttpUtils from '../../common/HttpUtils';
import configs from '../../common/configs';
export default {

    getTransHist(params, callback, navigation) {
        let paramOption = {};
        paramOption.URL = configs.URL_TRAN_HIST;
        paramOption.navigation = navigation;
        HttpUtils.fetchPost(params, callback, paramOption);
    }
}