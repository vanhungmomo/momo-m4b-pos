import React from 'react';
import { View, SectionList, Text, Image } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import MoMoHelper from '../../common/MoMoHelper';
import HistoryApi from './HistoryApi';
import moment from 'moment';
import HistoryHeader from './HistoryHeader';
import HistoryItem from './HistoryItem';
import styles from '../transaction/styles';
const MAX_LENGTH = 10;
export default class History extends React.Component {
    static navigationOptions = {
        header: null
    }

    state = {
        dataHis: [],
        arrTrans: [],
        isLoading: true,
        loadingMore: false
    }

    isSearch = false;
    isLastItem = false;
    _isMounted = false;

    componentDidMount() {
        this._isMounted = true;
        this.getDataHistory();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getDataHistory = () => {
        let { loadingMore, arrTrans } = this.state;
        let offset = loadingMore && arrTrans ? arrTrans.length : 0;
        let limit = MAX_LENGTH;//loadingMore && arrTrans ? (arrTrans.length + MAX_LENGTH) : MAX_LENGTH;
        let params = {};
        params.fromDate = "06-06-2018 00:00:00";
        params.offset = offset;
        params.limit = limit;
        params.toDate = moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
        HistoryApi.getTransHist(params, (result) => {
            let newData = [...arrTrans, ...result];
            this.reloadData(newData);
        }, this.props.navigation);
    }

    _handleRefresh = () => {
        if (!this._isMounted) {
            return;
        }
        this.setState({ dataHis: [], arrTrans: [], isLoading: true, loadingMore: false }, () => {
            this.getDataHistory();
        });
    }

    _handleLoadMore = () => {
        if (!this._isMounted) {
            return;
        }
        if (!this.isLastItem) {
            this.setState({ loadingMore: true }, () => {
                this.getDataHistory();
            });
        }
    }

    reloadData(arrDataInit, filter = "") {
        let newData = [];
        let objHeader = {};
        let currMonth = 0;
        let currYear = 0;

        if (arrDataInit && arrDataInit.length > 0) {
            if (arrDataInit.length < MAX_LENGTH) {
                this.isLastItem = true;
            } else {
                this.isLastItem = false;
            }
            let objItem = {};
            let objHeader = {};
            let dataContent = [];
            let totalAmounnt = 0;
            let arrData = [...arrDataInit];
            if (filter != "") {
                arrData = arrData.filter(item => MoMoHelper.removeAlias(item.phone).toUpperCase().includes(MoMoHelper.removeAlias(filter).toUpperCase()) || (item.tid + "").includes(filter));
            }
            for (let index = 0; index < arrData.length; index++) {
                let newItem = arrData[index];
                console.log("data_reload " + newItem.phone);
                console.log("data_reload 1 " + filter);
                let date = moment(newItem.date, "DD-MM-YYYY");
                let month = date.month() + 1;
                let year = date.year();
                newItem.dateFormat = (date.date() < 10 ? "0" : "") + date.date() + "/" + (month < 10 ? "0" : "") + month
                if (currMonth != month) {
                    currMonth = month;
                    currYear = year;
                    console.log("arr_data_test 2 " + JSON.stringify(newItem));
                    if (dataContent.length > 0) {
                        objHeader.totalAmounnt = "+" + MoMoHelper.formatNumberToMoney(totalAmounnt, "", "đ");
                        objItem.header = objHeader;
                        objItem.data = dataContent;
                        newData.push(objItem);
                        objHeader = {};
                        dataContent = [];
                        objItem = {};
                        totalAmounnt = 0;
                    }
                    objHeader.titleHeader = "THÁNG " + month + " - " + year;
                }
                dataContent.push(newItem);
                if (newItem.status == 0) {
                    if (newItem.tranType == "refund") {
                        totalAmounnt -= newItem.amount;
                    } else {
                        totalAmounnt += newItem.amount;
                    }
                }
                if (index == arrData.length - 1) {
                    objHeader.totalAmounnt = "+" + MoMoHelper.formatNumberToMoney(totalAmounnt, "", "đ");
                    objItem.header = objHeader;
                    objItem.data = dataContent;
                    newData.push(objItem);
                }
            }
        } else {
            this.isLastItem = true;
        }
        if (!this._isMounted) {
            return;
        }
        this.setState({ dataHis: newData, arrTrans: arrDataInit, isLoading: false, loadingMore: false });
    }

    onTextChange = (keyword) => {
        this.isSearch = false;
        if (keyword != "") {
            this.isSearch = true;
        }
        let { arrTrans } = this.state;
        this.reloadData(arrTrans, keyword);
    }

    onPressItem = (item) => {
        let isShowRefund = false;
        console.log("data_onPressItem " + JSON.stringify(item));
        if (item.tranType == "refund") {
            if (item.status != 0) {
                isShowRefund = true;
            }
        } else {
            if (item.status == 0) {
                if ((item.refundFormData && item.refundFormData.length > 0) || item.total_amount == 0) {
                    isShowRefund = false;
                } else {
                    isShowRefund = true;
                }
            }
        }
        let parameters = {};
        parameters.arrData = item.formData;
        parameters.arrRefundData = item.refundFormData;
        parameters.data = item;
        parameters.isShowRefund = isShowRefund;
        parameters.getDataHistory = this._handleRefresh;
        this.props.navigation.navigate("transactionDetail", parameters);
    }

    renderSectionHeader = ({ section }) => {
        console.log("data_header_test " + JSON.stringify(section));
        return (<HistoryHeader item={section.header} />)
    }

    renderItem = ({ item, index, section }) => {
        return (<HistoryItem item={item} key={index} onPressItem={this.onPressItem} />)
    }

    emptyComponent = () => {
        let { isLoading } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <View style={{ alignItems: 'center', paddingTop: 20, flex: 1 }}>
                    <Image source={require('../../images/icon/ic_empty.png')} resizeMode="contain"
                        style={{ width: 60, height: 60 }} />
                    <Text style={{ marginTop: 15, color: "#4D4D4D" }}>
                        {
                            this.isSearch
                                ?
                                "Không tìm thấy kết quả phù hợp."
                                :
                                isLoading
                                    ?
                                    "Dữ liệu đang được cập nhật..."
                                    :
                                    "Bạn chưa có lịch sử giao dịch."
                        }
                    </Text>
                </View>
            </View>
        )

    }

    render() {
        let { dataHis } = this.state;
        return (
            <View style={styles.container} >
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    isShowSearch={true}
                    popToTop={true}
                    onTextChange={this.onTextChange}
                    titleSearch={"Tìm giao dịch"}
                />
                <SectionList
                    renderItem={this.renderItem}
                    renderSectionHeader={this.renderSectionHeader}
                    sections={dataHis}
                    keyExtractor={(item, index) => item + index}
                    ListEmptyComponent={this.emptyComponent}
                    initialNumToRender={5}
                    initialNumToRender={10}
                    onEndReachedThreshold={1}
                    onEndReached={this._handleLoadMore}
                    refreshing={false}
                    onRefresh={this._handleRefresh}
                    removeClippedSubviews={true}
                />
            </View>
        );
    }
}

//id-index
//id2-index2