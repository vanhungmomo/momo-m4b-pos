import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import styles from './styles';
import MoMoHelper from '../../common/MoMoHelper';
export default class HistoryItem extends Component {
    constructor(props) {
        super(props);
    }

    onPress = () => {
        this.props.onPressItem(this.props.item);
    }

    render() {
        let { phone, dateFormat, status, amount, tid, tranType } = this.props.item;
        let amountFormat = MoMoHelper.formatNumberToMoney(amount, "", "đ");
        let phoneNumber = MoMoHelper.formatPhone(phone);
        let colorStatus = MoMoHelper.getColorStatus(status);
        let nameStatus = MoMoHelper.getNameStatus(status);
        let colorAmount = "#4D4D4D";
        if (status == 0) {
            if (tranType == "refund") {
                amountFormat = "-" + amountFormat;
            } else {
                amountFormat = "+" + amountFormat;
            }
        }
        if (tranType == "refund") {
            colorAmount = "#ff0000";
        }
        return (
            <TouchableOpacity onPress={this.onPress} style={{ backgroundColor: '#FFF', borderBottomColor: '#dadada', borderBottomWidth: 1, flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 8, alignItems: 'center' }}>
                <View style={styles.icon}>
                    <Image source={tranType == "refund" ? require('../../images/icon/ic_hoan_tien.png') : require('../../images/icon/ic_export.png')} resizeMode="contain"
                        style={{ width: 30, height: 30 }} />
                </View>
                <View style={{ borderWidth: 0, flex: 1, justifyContent: "space-between" }}>
                    <Text style={{ fontSize: 14, color: "#4D4D4D" }}>{phoneNumber}</Text>
                    <Text style={{ fontSize: 12, color: "#8F8E94", marginVertical: 3 }}>{dateFormat}</Text>
                    <Text style={{ fontSize: 12, color: colorStatus }}>{nameStatus}</Text>
                </View>
                <View style={{ justifyContent: "space-between", alignItems: 'flex-end' }}>
                    <Text style={{ fontSize: 14 }}>{" "}</Text>
                    <Text style={{ fontSize: 14, color: colorAmount, marginVertical: 3 }}>{amountFormat}</Text>
                    <Text style={{ fontSize: 12, color: "#8F8E94" }}>{tid}</Text>
                </View>
            </TouchableOpacity>);
    }
}