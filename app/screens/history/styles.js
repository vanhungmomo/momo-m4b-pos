import React, { Component } from 'react';
import { Dimensions } from 'react-native';
let { width, height } = Dimensions.get('window');
export default {
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    formInput: {
        backgroundColor: '#f2f2f2',
        borderBottomColor: '#dadada',
        borderTopColor: '#dadada',
        borderTopWidth: 0,
        borderBottomWidth: 1,
    },
    icon: {
        marginRight: 10, height: 40, width: 40,
        borderWidth: 1, borderColor: '#D5D4D8',
        borderRadius: 40 / 2, justifyContent: 'center', alignItems: 'center'
    }
};