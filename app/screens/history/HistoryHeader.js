import React, { Component } from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
export default class HistoryHeader extends Component {

    render() {
        let { titleHeader, totalAmounnt } = this.props.item;
        return (
            <View style={[styles.formInput, { borderBottomColor: '#dadada', borderBottomWidth: 1, flexDirection: 'row' }]}>
                <Text style={{ fontSize: 14, color: '#4d4d4d', marginHorizontal: 15, marginTop: 10, marginBottom: 3, flex: 1 }}>{titleHeader}</Text>
                <Text style={{ fontSize: 14, color: '#4d4d4d', marginHorizontal: 15, marginTop: 10, marginBottom: 3 }}>{totalAmounnt}</Text>
            </View>);
    }
}