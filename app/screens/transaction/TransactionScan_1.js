import React from 'react';
import { View, Text, Image, AppState, TouchableOpacity, TextInput } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import { Permissions } from '../../modules';
import SunmiInnerScanner, { SunmiScannerView } from '../../modules/scanner/SunmiInnerScanner';
import styles from './styles';
import MoMoModal from '../../components/modal';
import BarCodeFinder from '../../modules/scanqrcode/BarCodeFinder';
import MoMoReactToNative from '../../modules/MoMoReactToNative';
import HandleTransaction from './HandleTransaction';
import MoMoHelper from '../../common/MoMoHelper';
import Button from '../../components/button/Button';
import InputText from '../../components/input/InputText';
export default class TransactionScan extends React.Component {

    static navigationOptions = {
        header: null
    }

    milliseconds = 0;
    isFlash = 0;
    statusConfirm = -1;
    _isMounted = false;
    state = {
        hasCamera: false,
        isStatusCamera: 1,
        textFlash: "Bật flash",
        textCode: ""
    }
    componentDidMount() {
        this._isMounted = true;
        // AppState.addEventListener('change', this._handleAppStateChange);
        // this._hideTimeout = setTimeout(async () => {
        //     const { status } = await Permissions.getAsync(Permissions.CAMERA);
        //     if (status === 'granted') {
        //         this.grantedPermission();
        //     } else {
        //         this.showAlertGrantPermission();
        //     }
        // }, 500);
        this.openScannerWithOptions();
    }

    async openScannerWithOptions() {
        let options = {
            showSetting: true,
            showAlbum: true,
            paySound: true,
            payVibrate: true,// V1 not support
        }
        let result = await SunmiInnerScanner.openScannerWithOptions(options);
        if (result && result.value) {
            this.onCodeScan(result.value);
        } else {
            this.refs.MoMoModal.showModal("", "Mã không đúng hoặc đã hết hạn vui lòng thực hiện lại", "THỬ LẠI", "", this.onPressCancel, this.onPressConfirm);
        }
        // this.setState({ result: JSON.stringify(result) }
        //     , () => {
        //         console.log(this.state.result);
        //     });

    }

    // componentWillReceiveProps(nextProps) {
    //     console.log("componentWillReceiveProps ok1");
    //     const { focus } = nextProps;
    //     focus && this.focus();
    // }

    focus = () => {
        console.log("componentWillReceiveProps ok");
        this.TextInputRef.focus();
    }

    componentWillUnmount() {
        this._isMounted = false;
        // clearTimeout(this._hideTimeout);
        // AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleReloadCamera = () => {
        this.startCamera();
    }

    startCamera() {
        this.isFlash = 0;
        if (!this._isMounted) {
            return;
        }
        this.openScannerWithOptions();
        // this.setState({ isStatusCamera: 1, textFlash: "Bật flash" });
    }

    stopCamera() {
        if (!this._isMounted) {
            return;
        }
        this.setState({ isStatusCamera: 0 });
    }

    _handleAppStateChange = (state) => {
        if (state === 'active') {
            this.getCameraPermission();
        }
    }

    async askCameraPermission() {
        const { status, expires } = await Permissions.askAsync(Permissions.CAMERA);
        if (status === 'granted') {
            this.grantedPermission();
        } else {
            if (expires === 'never') {
                MoMoReactToNative.openMoMoDetailsSettings();
            }
        }
    }

    showAlertGrantPermission() {
        let Me = this;
        let title = "Cho phép truy cập Máy ảnh";
        let message = "Quý khách vui lòng cho phép MoMo truy cập Máy ảnh để tiếp tục sử dụng tính năng này.";
        MoMoReactToNative.showAlertNative(
            title,
            message,
            "",
            "ĐỒNG Ý",
            false,
            () => {
                Me.askCameraPermission();
            }
        );
        // if (this.refs.MoMoModal) {
        //     this.refs.MoMoModal.showModal(title, message, "ĐỒNG Ý", "ĐÓNG", this.onPressCancel, this.onPressConfirmAllowCamera);
        // }
    }

    onPressConfirmAllowCamera = () => {
        this.askCameraPermission();
    }

    async getCameraPermission() {
        const { status } = await Permissions.getAsync(Permissions.CAMERA);
        if (status === 'granted') {
            this.grantedPermission();
        }
    }

    grantedPermission() {
        if (!this._isMounted) {
            return;
        }
        this.setState({
            hasCamera: true
        });
    }

    onPressCancel = () => {
        this.props.navigation.goBack();
    }

    onPressConfirm = () => {
        if (this.statusConfirm != 151 && this.statusConfirm != 161) {
            this.startCamera();
            // this.setState({ textCode: "" }, () => {
            //     this.focus();
            // })
        } else {
            this.props.navigation.goBack();
        }
    }

    onCodeScan = (data) => {
        let diff = new Date().getTime() - this.milliseconds;
        if (diff < 800) {
            return;
        } else {
            this.milliseconds = new Date().getTime();
        }
        this.initTransaction(data);
        // this.stopCamera();
        // if (data && data.length > 0 && data[0].result) {
        //     this.initTransaction(data[0].result);
        // } else {
        //     this.refs.MoMoModal.showModal("", "Mã không đúng hoặc đã hết hạn vui lòng thực hiện lại", "THỬ LẠI", "", this.onPressCancel, this.onPressConfirm);
        // }
        return;
    }

    onPressAction = () => {
        let { textCode } = this.state;
        if (textCode != "") {
            this.initTransaction(textCode);
        } else {
            this.refs.MoMoModal.showModal("", "Mã không đúng hoặc đã hết hạn vui lòng thực hiện lại", "THỬ LẠI", "", this.onPressCancel, this.onPressConfirm);
        }

    }

    // onReadQRCode(result) {
    //     let diff = new Date().getTime() - this.milliseconds;
    //     if (diff < 800) {
    //         return;
    //     } else {
    //         this.milliseconds = new Date().getTime();
    //     }
    //     this.stopCamera();

    //     if (!result) {
    //         // this.showAlertScanError();
    //         return;
    //     }
    //     this.initTransaction(result);
    //     return;
    // }

    initTransaction(paymentCode) {
        let { amount } = this.props.navigation.state.params;
        if (amount != 0 && paymentCode != "") {
            let requestId = MoMoHelper.requestId();
            console.log("initTransaction " + requestId)
            HandleTransaction.initTransaction(this.props.navigation, amount, paymentCode, this._handleReloadCamera, (result, statusConfirm) => {
                this.statusConfirm = statusConfirm;
                // this.setState({ textCode: "" })
                if (result) {
                    if (this.refs.MoMoModal) {
                        this.refs.MoMoModal.showModal("", result, "THỬ LẠI", "", this.onPressCancel, this.onPressConfirm);
                    }
                }
            }, requestId, this.focus);
        }
    }

    onPressFlash = () => {
        if (this.isFlash == 1) {
            this.isFlash = 0;
        } else {
            this.isFlash = 1;
        }
        if (!this._isMounted) {
            return;
        }
        this.setState({ textFlash: this.isFlash == 1 ? "Tắt flash" : "Bật flash" });
    }

    onChangeText = (value) => {
        this.setState({ textCode: value });
    }

    onSubmitEditing = (event) => {
        let textCode = event.nativeEvent.text;
        if (textCode != "") {
            this.initTransaction(textCode.trim());
        } else {
            this.refs.MoMoModal.showModal("", "Mã không đúng hoặc đã hết hạn vui lòng thực hiện lại", "THỬ LẠI", "", this.onPressCancel, this.onPressConfirm);
        }
    }


    onPressClear = () => {
        this.setState({ textCode: "" })
    }

    onUnFocus = (type) => {

    }

    onFocus = (type) => {

    }

    render() {
        return (<View style={styles.container}>
            <MoMoHeader
                navigation={this.props.navigation}
                hadBack={true}
                title={"Quét QR Code"}
            />

            <View style={{ flex: 1, paddingBottom: 10 }}>
                <View style={{ flex: 1, backgroundColor: 'black' }} />
                {/*
                    this.state.hasCamera ?
                        <View style={{ flex: 1 }}>
                            <SunmiScannerView
                                style={styles.scanner}
                                startStopCamera={this.state.isStatusCamera}
                                isFlash={this.isFlash}
                                mute={0}
                                scanInterval={500}
                                onCodeScan={this.onCodeScan}>
                                <BarCodeFinder />
                            </SunmiScannerView>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', paddingHorizontal: 48, paddingVertical: 24 }}>
                                    <TouchableOpacity onPress={this.onPressFlash} style={styles.topButtonContainer}>
                                        <Image source={require('../../images/icon/ic_flash_new.png')} style={styles.topButtonImage} />
                                        <Text style={styles.topButtonText}>{this.state.textFlash}</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </View>
                        : <View style={{ flex: 1, backgroundColor: 'black' }} />
                */}

                {/*<View style={[styles.formInput, { flexDirection: 'row', alignItems: 'center' }]}>
                    <TextInput
                        ref={input => { this.TextInputRef = input }}
                        autoFocus={true}
                        placeholderTextColor={"#929292"}
                        placeholder={"Nhập số mã"}
                        value={this.state.textCode}
                        style={[styles.textInput, { flex: 1, paddingRight: 30 }]}
                        onChangeText={(value) => this.onChangeText(value)}
                        onSubmitEditing={this.onSubmitEditing}
                        underlineColorAndroid="transparent"
                    />
                    {this.state.textCode != "" ?
                        <TouchableOpacity onPress={this.onPressClear} style={[{ width: 14, height: 24, position: 'absolute', paddingRight: 30, right: 0, justifyContent: 'center' }]} >
                            <Image source={require('../../images/icon/ic_clear.png')} style={{ width: 15, height: 15 }} resizeMode="contain" />
                        </TouchableOpacity>
                        : null}

        </View>*/}
            </View>
            {/* <Button title={"Tiếp tục"} style={{ marginHorizontal: 20, marginVertical: 10 }} onPressAction={this.onPressAction} /> */}
            <MoMoModal ref="MoMoModal" disableCancel={true} />
        </View>)
    }
}