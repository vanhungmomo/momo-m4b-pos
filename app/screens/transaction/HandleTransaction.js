import TransactionApi from './TransactionApi';
import MoMoHelper from '../../common/MoMoHelper';
import DataApp from '../../data/DataApp';
export default {

    initTransaction(navigation, amount, paymentCode, handleReloadCamera, callback, _requestId) {
        let requestId = _requestId;
        if (typeof requestId === 'object') {
            requestId = new Date().getTime();
        }
        let params = {};
        params.paymentCode = paymentCode;
        params.requestId = requestId;
        params.amount = parseInt(amount);
        params.type = 0;//init
        console.log("data_result_test 1 " + JSON.stringify(params));
        TransactionApi.confirmTransaction(params, (result) => {
            let messageError = "Mã không đúng hoặc đã hết hạn vui lòng thực hiện lại";
            let statusConfirm = -1;
            console.log("data_result_test " + JSON.stringify(result));
            if (result) {
                let message = result.message;
                statusConfirm = result.status;
                if (result.status == 0) {
                    messageError = "";
                } else {
                    if (message) {
                        if (typeof message === "string") {
                            messageError = message;
                        } else if (typeof message === "object" && message.description) {
                            messageError = message.description;
                        }
                    }
                }
                if (messageError != "") {
                    callback(messageError, statusConfirm);
                } else {
                    let newArrData = [...DataApp.dataInitTrans];
                    newArrData = newArrData.map((item, index) => {
                        let newItem = { ...item };
                        if (newItem.value == "%phoneNumber%") {
                            if (message.customer_number) {
                                newItem.value = message.customer_number;
                            } else {
                                newItem.value = "";
                            }
                        }
                        if (newItem.value == "%amount%") {
                            if (message.amount) {
                                newItem.value = MoMoHelper.formatNumberToMoney(message.amount, null, "đ");
                            } else {
                                newItem.value = "0đ";
                            }
                        }
                        if (newItem.value == "%voucher%") {
                            if (message.customer_voucher_amount) {
                                newItem.value = MoMoHelper.formatNumberToMoney(message.customer_voucher_amount, null, "đ");
                            } else {
                                newItem.value = "0đ";
                            }
                        }
                        if (newItem.value == "%totalAmount%") {
                            if (message.customer_amount) {
                                newItem.value = MoMoHelper.formatNumberToMoney(message.customer_amount, null, "đ");
                            } else {
                                newItem.value = "0đ";
                            }
                        }
                        return newItem;
                    });
                    let parameters = {};
                    parameters.arrData = newArrData;
                    parameters.paymentCode = paymentCode
                    parameters.amount = parseInt(message.amount);
                    parameters.handleReloadCamera = handleReloadCamera;
                    navigation.navigate("transactionConfirm", parameters);
                    callback(null, statusConfirm);
                }
            } else {
                callback(messageError, statusConfirm);
            }
        }, navigation);
    }

}