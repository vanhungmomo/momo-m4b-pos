import React from 'react';
import { View, Text, Image, BackHandler } from 'react-native';
import styles from './styles';
import Button from '../../components/button/Button';
import TransactionApi from './TransactionApi';
import MoMoHelper from '../../common/MoMoHelper';
import MoMoModal from '../../components/modal';
import MoMoReactToNative from '../../modules/MoMoReactToNative';
import cache from '../../common/cache';
import configs from '../../common/configs';
export default class TransactionResult extends React.Component {
    static navigationOptions = {
        header: null
    }

    componentDidMount() {
        let Me = this;
        let { data } = this.props.navigation.state.params;
        let message = data.message;
        let token = cache.JWT_TOKEN;
        if (token && message.transid) {
            MoMoReactToNative.printData(configs.URL_ROOT + configs.URL_PRINT_BILL_IMAGE + message.transid, token);
        }
        Me.backButtonListener = BackHandler.addEventListener('hardwareBackPress', () => {
            Me.props.navigation.popToTop();
            return true;
        });
    }

    onPressAction = () => {
        this.props.navigation.popToTop();
    }

    onPressActionDetail = (data) => {
        let message = data.message;
        let params = {};
        params.tid = message.transid;
        TransactionApi.checkTrans(params, (result) => {
            let errorMessage = "Đã có lỗi trong quá trình xử lý";
            if (result) {
                if (result.resultCode == 0) {
                    let parameters = {};
                    parameters.arrData = result.formData;
                    this.props.navigation.navigate("transactionDetail", parameters);
                    errorMessage = "";
                } else {
                    if (result.resultMessage) {
                        errorMessage = result.resultMessage;
                    }
                }
            }
            if (this.refs.MoMoModal && errorMessage != "") {
                this.refs.MoMoModal.showModal("", messageError, "", "ĐÓNG", this.onPressCancel, this.onPressConfirm);
            }
        }, this.props.navigation);
    }


    onPressCancel = () => {

    }

    onPressConfirm = () => {

    }


    renderStatus() {
        let { data } = this.props.navigation.state.params;
        let statusTrans = "Giao dịch thành công";
        let imgStatus = require("../../images/icon/ic_success.png");
        if (data.status != 0) {
            statusTrans = "Giao dịch không thành công";
            imgStatus = require("../../images/icon/ic_fail.png");
        }
        return (<View style={styles.topContainner}>
            <Image source={imgStatus}
                style={{ width: 60, height: 60 }} />
            <Text style={{
                marginTop: 16,
                fontSize: 16,
                color: '#FFFFFF',
                fontWeight: 'bold',
                textAlign: 'center'
            }}>
                {statusTrans}
            </Text>
        </View>);
    }

    buildFormData() {
        let { arrData, data } = this.props.navigation.state.params;
        return MoMoHelper.buildFormData(arrData, () => {
            this.onPressActionDetail(data);
        });
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderStatus()}
                <View style={{ flex: 1, paddingBottom: 10 }}>
                    <View style={styles.formInput}>
                        {this.buildFormData()}
                    </View>
                </View>
                <Button title={"Màn hình chính"} style={{ marginHorizontal: 20, marginVertical: 10 }} onPressAction={this.onPressAction} />
                <MoMoModal ref="MoMoModal" disableCancel={true} />
            </View>
        );
    }
}