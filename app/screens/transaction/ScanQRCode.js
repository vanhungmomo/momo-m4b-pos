import React from 'react';
import { View, AppState } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import { BarCodeScanner, Permissions } from '../../modules';
import MoMoModal from '../../components/modal';
import MoMoReactToNative from '../../modules/MoMoReactToNative';
import HandleTransaction from './HandleTransaction';
import MoMoHelper from '../../common/MoMoHelper';
export default class ScanQRCode extends React.Component {
    static navigationOptions = {
        header: null
    }
    milliseconds = 0;
    _isMounted = false;

    state = {
        hasCamera: false
    }

    componentDidMount() {
        this._isMounted = true;
        AppState.addEventListener('change', this._handleAppStateChange);
        this._hideTimeout = setTimeout(async () => {
            const { status } = await Permissions.getAsync(Permissions.CAMERA);
            if (status === 'granted') {
                this.grantedPermission();
            } else {
                this.showAlertGrantPermission();
            }
        }, 500);
    }

    componentWillUnmount() {
        this._isMounted = false;
        clearTimeout(this._hideTimeout);
        AppState.removeEventListener('change', this._handleAppStateChange);
    }


    _handleAppStateChange = (state) => {
        if (state === 'active') {
            this.getCameraPermission();
        }
    }

    startCamera() {
        if (this.refs.barCodeScanner) {
            this.refs.barCodeScanner.startCamera();
        }
    }

    _handleReloadCamera = () => {
        this.startCamera();
    }


    async askCameraPermission() {
        console.log("on_confirm_test OK");
        // const { status } = await Permissions.getAsync(Permissions.CAMERA);
        const { status, expires } = await Permissions.askAsync(Permissions.CAMERA);
        console.log("on_confirm_test " + status);
        if (status === 'granted') {
            this.grantedPermission();
        } else {
            if (expires === 'never') {
                MoMoReactToNative.openMoMoDetailsSettings();
            }
        }
    }

    showAlertGrantPermission() {
        debugger;
        let title = "Cho phép truy cập Máy ảnh";
        let message = "Quý khách vui lòng cho phép MoMo truy cập Máy ảnh để tiếp tục sử dụng tính năng này.";
        if (this.refs.MoMoModal) {
            this.refs.MoMoModal.showModal(title, message, "TIẾP TỤC", "ĐÓNG", this.onPressCancel, this.onPressConfirmAllowCamera);
        }
    }

    onPressConfirmAllowCamera = () => {
        this.askCameraPermission();

    }

    async getCameraPermission() {
        const { status } = await Permissions.getAsync(Permissions.CAMERA);
        if (status === 'granted') {
            this.grantedPermission();
        }
    }

    grantedPermission() {
        if (!this._isMounted) {
            return;
        }
        this.setState({
            hasCamera: true
        });
    }

    stopCamera() {
        if (this.refs.barCodeScanner) {
            // off flash
            if (this.refs.textFlash) {
                this.isFlash = false;
                // this.refs.textFlash.setText(strings.textFlashOn);
                this.refs.barCodeScanner.setNativeProps({
                    torchMode: 'off'
                });
            }
            this.refs.barCodeScanner.stopCamera();
        }
    }

    onPressCancel = () => {
        this.props.navigation.goBack();
    }

    onPressConfirm = () => {
        this.startCamera();
    }

    onReadQRCode(result) {
        let diff = new Date().getTime() - this.milliseconds;
        if (diff < 800) {
            return;
        } else {
            this.milliseconds = new Date().getTime();
        }
        this.stopCamera();

        if (!result) {
            return;
        }
        this.initTransaction(result);
        return;
    }

    initTransaction(paymentCode) {
        let { amount } = this.props.navigation.state.params;
        if (amount != 0 && paymentCode != "") {
            let requestId = MoMoHelper.requestId();
            HandleTransaction.initTransaction(this.props.navigation, amount, paymentCode, this._handleReloadCamera, (result, statusConfirm) => {
                this.statusConfirm = statusConfirm;
                if (result) {
                    if (this.refs.MoMoModal) {
                        this.refs.MoMoModal.showModal("", result, "THỬ LẠI", "ĐÓNG", this.onPressCancel, this.onPressConfirm);
                    }
                }
            }, requestId);
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    title={"Quét QR Code"}
                />
                {
                    this.state.hasCamera ?
                        <BarCodeScanner
                            ref='barCodeScanner'
                            onBarCodeRead={(data) => this.onReadQRCode(data.data)}
                            style={{ flex: 1 }}
                            type='back'
                            torchMode={this.isFlash ? 'on' : 'off'}
                        />
                        :
                        <View style={{ flex: 1, backgroundColor: 'black' }} />
                }
                <MoMoModal ref="MoMoModal" disableCancel={true} />
            </View>
        );
    }
}