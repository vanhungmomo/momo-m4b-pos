import React from 'react';
import { View, TextInput, Text, ScrollView } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import styles from './styles';
import Button from '../../components/button/Button';
import MoMoHelper from '../../common/MoMoHelper';
import TransactionApi from './TransactionApi';
import MoMoModal from '../../components/modal';
import DataApp from '../../data/DataApp';
import Global from '../../data/Global';

export default class TransactionRefund extends React.Component<props> {
    static navigationOptions = {
        header: null
    }

    _isMounted = false;

    constructor(props) {
        super(props);
        let { data } = props.navigation.state.params;
        let amount = MoMoHelper.formatNumberToMoney(data.total_amount, null, "");
        this.state = {
            amount: data.total_amount,
            textAmount: amount,
            errorMessage: "",
            description: ""
        }
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onPressAction = () => {
        let { description, amount } = this.state;
        let { data } = this.props.navigation.state.params;
        let phoneNumber = data.phone;
        let errorMessage = "Chưa nhập số tiền";
        if (parseInt(amount) > parseInt(data.total_amount)) {
            errorMessage = "Số tiền hoàn lại không hợp lệ";
            if (!this._isMounted) {
                return;
            }
            this.setState({ errorMessage: errorMessage });
            return;
        } else if (amount != 0 && amount >= Global.minAmount) {
            errorMessage = "";
            let params = {};
            let requestId = MoMoHelper.requestId();
            if (typeof requestId === 'object') {
                requestId = new Date().getTime();
            }
            params.requestId = requestId;
            params.momoTransId = data.tid + "";
            params.amount = parseInt(amount);
            params.description = description;
            TransactionApi.refundTransaction(params, (result) => {
                console.log("transaction_refund " + JSON.stringify(result));
                let messageError = "Đã có lỗi trong quá trình xử lý";
                if (result.resultCode == 0) {
                    messageError = "";
                    let dataRefund = JSON.parse(result.data);
                    let amountRefund = dataRefund.amount;
                    if (this.refs.MoMoModal) {
                        this.refs.MoMoModal.showModal("Hoàn tiền thành công", "Quý khách vừa hoàn số tiền " + MoMoHelper.formatNumberToMoney(amountRefund, null, "đ") + " vào ví MoMo " + MoMoHelper.formatPhone(phoneNumber) + ".", "", "ĐÓNG", () => {
                            if (this.props.navigation.state.params.getDataHistory) {
                                this.props.navigation.state.params.getDataHistory();
                                this.props.navigation.pop(2)
                            }
                        }, () => {
                            if (this.props.navigation.state.params.getDataHistory) {
                                this.props.navigation.state.params.getDataHistory();
                                this.props.navigation.pop(2)
                            }
                        });
                    }
                } else {
                    if (result.resultMessage) {
                        messageError = result.resultMessage;
                        if (this.refs.MoMoModal) {
                            this.refs.MoMoModal.showModal("Hoàn tiền thất bại", messageError, "", "ĐÓNG", this.onPressCancel, this.onPressConfirm);
                        }
                    }
                }
            });
        } else if (amount < Global.minAmount && amount > 0) {
            errorMessage = "Số tiền hoàn lại tối thiểu là " + MoMoHelper.formatNumberToMoney(Global.minAmount, null, "đ");;
        }
        if (!this._isMounted) {
            return;
        }
        this.setState({ errorMessage: errorMessage });
    }

    onPressCancel = () => {

    }

    onPressConfirm = () => {

    }

    onChangeText = (type, value) => {
        if (type == "amount") {
            let newValue = value.replace(/\./g, "").trim();
            let amount = MoMoHelper.formatNumberToMoney(newValue, null, "");
            if (!this._isMounted) {
                return;
            }
            this.setState({ amount: newValue, textAmount: amount, errorMessage: "" });
        } else {
            if (!this._isMounted) {
                return;
            }
            this.setState({ description: value });
        }
    }

    renderRefundPolicy() {
        return DataApp.refundPolicy.map((item, index) => {
            return (<Text style={{ fontSize: 16, color: "#4d4d4d", marginTop: 5 }} key={"item_" + index}>{item}</Text>);
        });
    }

    render() {
        let { description, textAmount, errorMessage } = this.state;
        return (
            <View style={styles.container}>
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    title={"Hoàn tiền"}
                />
                <ScrollView style={{ flex: 1, paddingBottom: 10 }} keyboardShouldPersistTaps="always">
                    <View style={styles.formInput}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TextInput
                                maxLength={10}
                                placeholderTextColor={"#929292"}
                                placeholder={"Nhập số tiền"}
                                keyboardType={"numeric"}
                                value={textAmount == 0 ? "" : textAmount}
                                style={[styles.textInput, { flex: 1, paddingRight: 30 }]}
                                onChangeText={(value) => this.onChangeText("amount", value)}
                                underlineColorAndroid="transparent"
                            />
                            <Text style={{ position: 'absolute', right: 20, fontSize: 30 }}>{"đ"}</Text>
                        </View>
                        {
                            errorMessage != "" ?
                                <Text style={{ fontSize: 16, color: '#ce3a3e', paddingHorizontal: 15, marginTop: 5 }}>{errorMessage}</Text>
                                : null
                        }
                        <TextInput
                            placeholderTextColor={"#929292"}
                            placeholder={"Ghi chú (Không bắt buộc)"}
                            value={description}
                            style={[styles.textInput, { fontSize: 16 }]}
                            onChangeText={(value) => this.onChangeText("description", value)}
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <View style={{ paddingVertical: 10, paddingHorizontal: 15 }}>
                        <Text style={{ fontSize: 18, color: "#4d4d4d", fontWeight: 'bold', marginBottom: 5 }}>{"Quy định hoàn tiền"}</Text>
                        {this.renderRefundPolicy()}
                    </View>
                </ScrollView>
                <Button title={"Tiếp tục"} style={{ marginHorizontal: 20, marginVertical: 10 }} onPressAction={this.onPressAction} />
                <MoMoModal ref="MoMoModal" disableCancel={true} />
            </View>
        );
    }
}