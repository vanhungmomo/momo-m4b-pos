import React from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import styles from './styles';
import Button from '../../components/button/Button';
import MoMoModal from '../../components/modal';
import TransactionApi from './TransactionApi';
import cache from '../../common/cache';
let { width, height } = Dimensions.get('window');
import MoMoReactToNative from '../../modules/MoMoReactToNative';
import configs from '../../common/configs';
export default class GenerateQRCode extends React.Component {
    static navigationOptions = {
        header: null
    }

    state = {
        dynamicQRUrl: "",
        billId: "",
        loading: true
    }

    componentDidMount() {
        this.generateQRCode();
    }

    generateQRCode = () => {
        let profile = cache.USER_PROFILE;
        if (profile && profile.extraData) {
            let extraData = JSON.parse(profile.extraData);
            if (extraData && extraData.m4b_parking_ticket) {
                let params = {};
                params.amount = Number(extraData.m4b_parking_ticket.defaultAmount);
                params.version = 1;
                TransactionApi.generateQRCode(params, (result) => {
                    if (result && result.status == 0) {
                        MoMoReactToNative.generateCode(result.dynamicQRUrl, (base64QRCode, base64BarCode) => {
                            this.setState({
                                dynamicQRUrl: 'data:image/png;base64,' + base64QRCode,
                                billId: result.billId,
                                loading: false
                            });
                        }
                        );
                    } else {
                        this.setState({
                            loading: false
                        });
                    }
                }, this.props.navigation);
            }
        }
    }

    onPressAction = () => {
        let { billId } = this.state;
        let params = {};
        params.billID = billId;
        params.version = 1;
        TransactionApi.checkByBill(params, (result) => {
            console.log("TransactionApi.checkByBill == " + JSON.stringify(result));
            if (result) {
                if (result.status == 0) {
                    this.generateQRCode();
                    let token = cache.JWT_TOKEN;
                    MoMoReactToNative.printData(configs.URL_ROOT + configs.URL_PRINT_BILL_IMAGE + result.transId, token);
                } else {
                    this.refs.MoMoModal.showModal("Thông báo", "Hóa đơn chưa được thanh toán", "Đóng", "", this.onPressCancel, this.onPressConfirm);
                }
            }
        });
    }

    onPressConfirm = () => {
        // this.generateQRCode();
    }

    render() {
        let { dynamicQRUrl, loading } = this.state;
        return (
            <View style={styles.container}>
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    title={"Thanh toán"}
                />
                <View style={{ flex: 1, paddingBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                    {
                        dynamicQRUrl && !loading ?
                            <Image accessibilityLabel='QrCode' source={{ uri: dynamicQRUrl }}
                                style={{ width: width - 100, height: width - 100 }} resizeMode={'contain'} />
                            : null
                    }
                    {
                        dynamicQRUrl && !loading ?
                            <Text style={{ fontSize: 16, color: '#4d4d4d', paddingHorizontal: 15, marginTop: 50, textAlign: 'center' }}>{"Sau khi thanh toán thành công bạn vui lòng nhấn In Vé để xuất vé xe."}</Text>
                            :
                            !loading ?
                                <Text style={{ fontSize: 16, color: '#4d4d4d', paddingHorizontal: 15, marginTop: 50, textAlign: 'center' }}>{"Chưa lấy được thông tin vui lòng thử lại."}</Text>
                                : null
                    }

                </View>
                <Button title={dynamicQRUrl ? "In Vé" : "Thử lại"} style={{ marginHorizontal: 20, marginVertical: 10 }} onPressAction={this.onPressAction} />
                <MoMoModal ref="MoMoModal" disableCancel={true} />
            </View>
        );
    }
}