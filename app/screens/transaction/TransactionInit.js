import React from 'react';
import { View, TextInput, Text, Keyboard } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import styles from './styles';
import Button from '../../components/button/Button';
import MoMoHelper from '../../common/MoMoHelper';
import MoMoModal from '../../components/modal';
import Global from '../../data/Global';
export default class TransactionInit extends React.Component {
    static navigationOptions = {
        header: null
    }
    _isMounted = false;

    state = {
        amount: 0,
        textAmount: "",
        errorMessage: ""

    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onPressAction = () => {
        let { amount } = this.state;
        let errorMessage = "Chưa nhập số tiền";
        if (amount != 0 && amount >= Global.minAmount) {
            errorMessage = "";
            if (amount > Global.maxAmount) {
                errorMessage = "Số tiền thanh toán tối đa là " + MoMoHelper.formatNumberToMoney(Global.maxAmount, null, "đ");
            } else {
                Keyboard.dismiss();
                let parameters = {};
                parameters.amount = parseInt(amount);
                this.props.navigation.navigate("transactionScan", parameters);//sunmi
                // this.props.navigation.navigate("scanQRCode", parameters);
            }
        } else if (amount < Global.minAmount && amount > 0) {
            errorMessage = "Số tiền thanh toán tối thiểu là " + MoMoHelper.formatNumberToMoney(Global.minAmount, null, "đ");
        }
        if (!this._isMounted) {
            return;
        }
        this.setState({ errorMessage: errorMessage });
    }

    onChangeText = (type, value) => {
        if (type == "amount") {
            let newValue = value.replace(/\./g, "").trim();
            let amount = MoMoHelper.formatNumberToMoney(newValue, null, "");
            if (!this._isMounted) {
                return;
            }
            this.setState({ amount: newValue, textAmount: amount, errorMessage: "" });
        }
    }

    render() {
        let { textAmount, errorMessage } = this.state;
        return (
            <View style={styles.container}>
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    title={"Thanh toán"}
                />
                <View style={{ flex: 1, paddingBottom: 10 }}>
                    <View style={styles.formInput}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TextInput
                                autoFocus={true}
                                maxLength={10}
                                placeholderTextColor={"#929292"}
                                placeholder={"Nhập số tiền"}
                                keyboardType={"numeric"}
                                value={textAmount == 0 ? "" : textAmount}
                                style={[styles.textInput, { flex: 1, paddingRight: 30 }]}
                                onChangeText={(value) => this.onChangeText("amount", value)}
                                underlineColorAndroid="transparent"
                            />
                            <Text style={{ position: 'absolute', right: 20, fontSize: 30 }}>{"đ"}</Text>
                        </View>
                        {
                            errorMessage != "" ?
                                <Text style={{ fontSize: 16, color: '#ce3a3e', paddingHorizontal: 15, marginTop: 5 }}>{errorMessage}</Text>
                                : null
                        }
                    </View>
                </View>
                <Button title={"Tiếp tục"} style={{ marginHorizontal: 20, marginVertical: 10 }} onPressAction={this.onPressAction} />
                <MoMoModal ref="MoMoModal" disableCancel={true} />
            </View>
        );
    }
}