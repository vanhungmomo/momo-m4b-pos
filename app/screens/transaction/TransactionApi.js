import HttpUtils from '../../common/HttpUtils';
import configs from '../../common/configs';
export default {

    confirmTransaction(params, callback, navigation) {
        let paramOption = {};
        paramOption.URL = configs.URL_CONFIRM_TRANS;
        paramOption.transaction = true;
        paramOption.navigation = navigation;
        HttpUtils.fetchPost(params, callback, paramOption);
    },

    checkTrans(params, callback, navigation) {
        let paramOption = {};
        paramOption.URL = configs.URL_CHECK_TRANS;
        paramOption.showLoading = true;
        paramOption.navigation = navigation;
        HttpUtils.fetchPost(params, callback, paramOption);
    },

    // printBill(params, callback) {
    //     HttpUtils.fetchPost(params, callback, null, configs.URL_PRINT_BILL);
    // },

    printBillFromImage(params, callback) {
        let paramOption = {};
        paramOption.URL = configs.URL_PRINT_BILL_IMAGE;
        HttpUtils.fetchGet(params, callback, paramOption);
    },

    refundTransaction(params, callback, navigation) {
        let paramOption = {};
        paramOption.URL = configs.URL_REFUND_TRANS;
        paramOption.transaction = true;
        paramOption.navigation = navigation;
        HttpUtils.fetchPost(params, callback, paramOption);
    },

    generateQRCode(params, callback, navigation) {
        let paramOption = {};
        paramOption.URL = configs.URL_STATIC_QR;
        paramOption.showLoading = true;
        paramOption.navigation = navigation;
        HttpUtils.fetchPost(params, callback, paramOption);
    },

    checkByBill(params, callback, navigation) {
        let paramOption = {};
        paramOption.URL = configs.URL_CHECK_BY_BILL;
        paramOption.showLoading = true;
        paramOption.navigation = navigation;
        HttpUtils.fetchPost(params, callback, paramOption);
    }
}