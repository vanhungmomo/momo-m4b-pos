import React, { Component } from 'react';
import { Dimensions } from 'react-native';
let { width, height } = Dimensions.get('window');
export default {
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    textInput: {
        fontSize: 30,
        marginHorizontal: 15,
        marginTop: 5,
        height: 50,
        paddingBottom: 2,
        borderBottomColor: '#dadada',
        borderBottomWidth: 1,
    },
    formInput: {
        backgroundColor: '#FFF',
        borderBottomColor: '#dadada',
        borderTopColor: '#dadada',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        paddingBottom: 10,
    },
    topContainner: {
        backgroundColor: '#b0006d',
        alignItems: 'center',
        paddingVertical: 20,
    },
    scanner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    topButtonImage: {
        width: 15,
        height: 15,
        justifyContent: 'center',
        resizeMode: 'contain'
    },
    topButtonText: {
        fontSize: 12,
        color: 'white',
        marginLeft: 5,
        backgroundColor: 'transparent'
    },
    topButtonContainer: {
        height: 36,
        width: 100,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 18
    }
};