import React from 'react';
import { View, Text, Image, AppState, TouchableOpacity, TextInput } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import { Permissions } from '../../modules';
import { SunmiScannerView } from '../../modules/scanner/SunmiInnerScanner';
import styles from './styles';
import MoMoModal from '../../components/modal';
import BarCodeFinder from '../../modules/scanqrcode/BarCodeFinder';
import MoMoReactToNative from '../../modules/MoMoReactToNative';
import HandleTransaction from './HandleTransaction';
import MoMoHelper from '../../common/MoMoHelper';
export default class TransactionScan extends React.Component {

    static navigationOptions = {
        header: null
    }

    milliseconds = 0;
    isFlash = 0;
    statusConfirm = -1;
    _isMounted = false;
    state = {
        hasCamera: false,
        isStatusCamera: 1,
        textFlash: "Bật flash"
    }
    componentDidMount() {
        this._isMounted = true;
        AppState.addEventListener('change', this._handleAppStateChange);
        this._hideTimeout = setTimeout(async () => {
            const { status } = await Permissions.getAsync(Permissions.CAMERA);
            if (status === 'granted') {
                this.grantedPermission();
            } else {
                this.showAlertGrantPermission();
            }
        }, 500);
    }

    componentWillUnmount() {
        this._isMounted = false;
        clearTimeout(this._hideTimeout);
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleReloadCamera = () => {
        this.startCamera();
    }

    startCamera() {
        this.isFlash = 0;
        if (!this._isMounted) {
            return;
        }
        this.setState({ isStatusCamera: 1, textFlash: "Bật flash" });
    }

    stopCamera() {
        if (!this._isMounted) {
            return;
        }
        this.setState({ isStatusCamera: 0 });
    }

    _handleAppStateChange = (state) => {
        if (state === 'active') {
            this.getCameraPermission();
        }
    }

    async askCameraPermission() {
        const { status, expires } = await Permissions.askAsync(Permissions.CAMERA);
        if (status === 'granted') {
            this.grantedPermission();
        } else {
            if (expires === 'never') {
                MoMoReactToNative.openMoMoDetailsSettings();
            }
        }
    }

    showAlertGrantPermission() {
        let Me = this;
        let title = "Cho phép truy cập Máy ảnh";
        let message = "Quý khách vui lòng cho phép MoMo truy cập Máy ảnh để tiếp tục sử dụng tính năng này.";
        MoMoReactToNative.showAlertNative(
            title,
            message,
            "",
            "ĐỒNG Ý",
            false,
            () => {
                Me.askCameraPermission();
            }
        );
    }

    async getCameraPermission() {
        const { status } = await Permissions.getAsync(Permissions.CAMERA);
        if (status === 'granted') {
            this.grantedPermission();
        }
    }

    grantedPermission() {
        if (!this._isMounted) {
            return;
        }
        this.setState({
            hasCamera: true
        });
    }

    onPressCancel = () => {
        this.props.navigation.goBack();
    }

    onPressConfirm = () => {
        if (this.statusConfirm != 151 && this.statusConfirm != 161) {
            this.startCamera();
        } else {
            this.props.navigation.goBack();
        }
    }

    onCodeScan = (data) => {
        let diff = new Date().getTime() - this.milliseconds;
        if (diff < 800) {
            return;
        } else {
            this.milliseconds = new Date().getTime();
        }
        this.stopCamera();
        if (data && data.length > 0 && data[0].result) {
            this.initTransaction(data[0].result);
        } else {
            this.refs.MoMoModal.showModal("", "Mã không đúng hoặc đã hết hạn vui lòng thực hiện lại", "THỬ LẠI", "", this.onPressCancel, this.onPressConfirm);
        }
        return;
    }

    initTransaction(paymentCode) {
        let { amount } = this.props.navigation.state.params;
        if (amount != 0 && paymentCode != "") {
            let requestId = MoMoHelper.requestId();
            console.log("initTransaction " + requestId)
            HandleTransaction.initTransaction(this.props.navigation, amount, paymentCode, this._handleReloadCamera, (result, statusConfirm) => {
                this.statusConfirm = statusConfirm;
                if (result) {
                    if (this.refs.MoMoModal) {
                        this.refs.MoMoModal.showModal("", result, "THỬ LẠI", "", this.onPressCancel, this.onPressConfirm);
                    }
                }
            }, requestId);
        }
    }

    onPressFlash = () => {
        if (this.isFlash == 1) {
            this.isFlash = 0;
        } else {
            this.isFlash = 1;
        }
        if (!this._isMounted) {
            return;
        }
        this.setState({ textFlash: this.isFlash == 1 ? "Tắt flash" : "Bật flash" });
    }

    render() {
        return (<View style={styles.container}>
            <MoMoHeader
                navigation={this.props.navigation}
                hadBack={true}
                title={"Quét QR Code"}
            />
            <View style={{ flex: 1 }}>
                {
                    this.state.hasCamera ?
                        <View style={{ flex: 1 }}>
                            <SunmiScannerView
                                style={styles.scanner}
                                startStopCamera={this.state.isStatusCamera}
                                isFlash={this.isFlash}
                                mute={0}
                                scanInterval={500}
                                onCodeScan={this.onCodeScan}>
                                <BarCodeFinder />
                            </SunmiScannerView>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', paddingHorizontal: 48, paddingVertical: 24 }}>
                                    <TouchableOpacity onPress={this.onPressFlash} style={styles.topButtonContainer}>
                                        <Image source={require('../../images/icon/ic_flash_new.png')} style={styles.topButtonImage} />
                                        <Text style={styles.topButtonText}>{this.state.textFlash}</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </View>
                        : <View style={{ flex: 1, backgroundColor: 'black' }} />
                }
            </View>
            <MoMoModal ref="MoMoModal" disableCancel={true} />
        </View>)
    }
}