import React from 'react';
import { View } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import styles from './styles';
import MoMoModal from '../../components/modal';
import HandleTransaction from './HandleTransaction';
import ScannerSensor from '../../modules/scansensor/ScannerSensor';
import MoMoHelper from '../../common/MoMoHelper';
export default class TransactionScan extends React.Component {

    static navigationOptions = {
        header: null
    }


    milliseconds = 0;
    isFlash = 0;
    statusConfirm = -1;
    _isMounted = false;
    state = {
        isResume: false
    }
    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    _handleReloadCamera = () => {
        this.startCamera();
    }

    startCamera() {
        this.isFlash = 0;
        if (!this._isMounted) {
            return;
        }
        this.setState({ isResume: true });
    }

    onPressCancel = () => {
        this.props.navigation.goBack();
    }

    onPressConfirm = () => {
        if (this.statusConfirm != 151 && this.statusConfirm != 161) {
            this.startCamera();
        } else {
            this.props.navigation.goBack();
        }
    }

    onClickWith = (event) => {
        let data = event.nativeEvent;
        let diff = new Date().getTime() - this.milliseconds;
        if (diff < 800) {
            return;
        } else {
            this.milliseconds = new Date().getTime();
        }
        if (!data.data) {
            return;
        }
        this.initTransaction(data.data);
        return;
    }

    initTransaction(paymentCode) {
        console.log("data_click_result " + paymentCode);
        let { amount } = this.props.navigation.state.params;
        if (amount != 0 && paymentCode != "") {
            let requestId = MoMoHelper.requestId();
            HandleTransaction.initTransaction(this.props.navigation, amount, paymentCode, this._handleReloadCamera, (result, statusConfirm) => {
                this.statusConfirm = statusConfirm;
                if (result) {
                    if (this.refs.MoMoModal) {
                        this.refs.MoMoModal.showModal("", result, "XÁC NHẬN", "ĐÓNG", this.onPressCancel, this.onPressConfirm);
                    }
                }
            }, requestId);
        }
    }

    onPressFlash = () => {
        if (this.isFlash == 1) {
            this.isFlash = 0;
        } else {
            this.isFlash = 1;
        }
        if (!this._isMounted) {
            return;
        }
        this.setState({ textFlash: this.isFlash == 1 ? "Tắt flash" : "Bật flash" });
    }

    render() {
        let { isResume } = this.state;
        return <View style={styles.container}>
            <MoMoHeader
                navigation={this.props.navigation}
                hadBack={true}
                title={"Quét QR Code"}
            />
            <ScannerSensor onClickWith={this.onClickWith} isResume={isResume} />
            <MoMoModal ref="MoMoModal" disableCancel={true} />
        </View>
    }
}