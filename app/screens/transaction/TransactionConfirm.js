import React from 'react';
import { View, Text, Image } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import styles from './styles';
import Button from '../../components/button/Button';
import MoMoHelper from '../../common/MoMoHelper';
import TransactionApi from './TransactionApi';
import MoMoModal from '../../components/modal';
import DataApp from '../../data/DataApp';
export default class TransactionConfirm extends React.Component {
    static navigationOptions = {
        header: null
    }
    
    onPressAction = () => {
        let { paymentCode, amount } = this.props.navigation.state.params;
        let params = {};
        let requestId = MoMoHelper.requestId();
        if (typeof requestId === 'object') {
            requestId = new Date().getTime();
        }
        console.log("request_id_test 2 " + requestId);
        params.paymentCode = paymentCode;
        params.requestId = requestId;
        params.amount = parseInt(amount);
        params.type = 1;//confirm
        TransactionApi.confirmTransaction(params, (result) => {
            console.log("result_data_test " + JSON.stringify(result));
            let showError = true;
            let messageError = "Mã không đúng hoặc đã hết hạn vui lòng thực hiện lại";
            let parameters = {};
            if (result) {
                let data = result.message;
                if (result.status == 0) {
                    showError = false;
                    let newArrData = [...DataApp.dataResultTrans];
                    newArrData = newArrData.map((item, index) => {
                        let newItem = { ...item };
                        if (newItem.value == "%amount%") {
                            if (data.amount) {
                                newItem.value = MoMoHelper.formatNumberToMoney(data.amount, null, "đ");
                            } else {
                                newItem.value = "";
                            }
                        }
                        if (newItem.value == "%phoneNumber%") {
                            if (data.phoneNumber) {
                                newItem.value = data.phoneNumber;
                            } else {
                                newItem.value = "";
                            }
                        }
                        if (newItem.value == "%transId%") {
                            if (data.transid) {
                                newItem.value = data.transid;
                            } else {
                                newItem.value = "";
                            }
                        }
                        return newItem;
                    });
                    parameters.arrData = newArrData;
                    parameters.data = result;
                } else {
                    if (data) {
                        if (typeof data === "string") {
                            messageError = data;
                        } else if (typeof data === "object" && data.description) {
                            messageError = data.description;
                        }
                    }
                }
            }
            if (showError) {
                if (this.refs.MoMoModal) {
                    this.refs.MoMoModal.showModal("", messageError, "XÁC NHẬN", "ĐÓNG", this.onPressCancel, this.onPressConfirm);
                }
            } else {
                this.props.navigation.navigate("transactionResult", parameters);
            }
        });
    }

    buildFormData() {
        let { arrData } = this.props.navigation.state.params;
        return MoMoHelper.buildFormData(arrData);
    }

    renderFooter() {
        return (<View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15, paddingHorizontal: 15 }}>
            <Image source={require('../../images/icon/ic_ssl.png')} style={{ height: 40, width: 105, marginRight: 15 }} />
            <Text style={{ fontSize: 14, color: '#929292', flex: 1, textAlign: 'left', lineHeight: 20 }}>{"Bảo mật chuẩn SSL/TLS, mọi thông tin giao dịch đều được mã hoá an toàn."}</Text>
        </View>)
    }

    onPressCancel = () => {

    }

    onPressConfirm = () => {
        let { handleReloadCamera } = this.props.navigation.state.params;
        if (handleReloadCamera) {
            this.props.navigation.state.params.handleReloadCamera();
            this.props.navigation.goBack();
        }
    }

    render() {
        let { handleReloadCamera } = this.props.navigation.state.params;
        return (
            <View style={styles.container}>
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    handleReloadCamera={handleReloadCamera}
                    onHandleBack={this.onHandleBack}
                    title={"Thanh toán an toàn"}
                />
                <View style={{ flex: 1, paddingBottom: 10 }}>
                    <View style={styles.formInput}>
                        <View style={{ backgroundColor: '#f2f2f2', paddingHorizontal: 15, paddingTop: 10, paddingBottom: 3, borderBottomColor: '#dadada', borderBottomWidth: 1 }}>
                            <Text style={{ fontSize: 16, color: '#4d4d4d' }}>{"CHI TIẾT GIAO DỊCH"}</Text>
                        </View>
                        {this.buildFormData()}
                    </View>
                    {this.renderFooter()}
                </View>
                <Button title={"Xác nhận"} style={{ marginHorizontal: 20, marginVertical: 10 }} onPressAction={this.onPressAction} />
                <MoMoModal ref="MoMoModal" disableCancel={true} />
            </View>
        );
    }
}