import React from 'react';
import { View, TextInput, Text } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import styles from './styles';
import Button from '../../components/button/Button';
import TransactionApi from './TransactionApi';
import MoMoModal from '../../components/modal';
export default class TransactionCheck extends React.Component {
    static navigationOptions = {
        header: null
    }

    _isMounted = false;

    state = {
        tidTrans: "",
        resultMessage: ""
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onPressAction = () => {
        let { tidTrans } = this.state;
        if (tidTrans == "") {
            if (!this._isMounted) {
                return;
            }
            this.setState({ resultMessage: "Chưa nhập mã giao dịch" });
            return;
        }
        let params = {};
        params.tid = tidTrans;
        TransactionApi.checkTrans(params, (result) => {
            let errorMessage = "Đã có lỗi trong quá trình xử lý";
            if (result) {
                if (result.resultCode == 0) {
                    let parameters = {};
                    parameters.arrData = result.formData;
                    this.props.navigation.navigate("transactionDetail", parameters);
                    errorMessage = "";
                } else {
                    if (result.resultMessage) {
                        errorMessage = result.resultMessage;
                    }
                }
            }
            if (!this._isMounted) {
                return;
            }
            this.setState({ resultMessage: errorMessage });
        }, this.props.navigation);
    }

    onChangeText = (value) => {
        let newValue = value.replace(/\./g, "").trim();
        if (!this._isMounted) {
            return;
        }
        this.setState({ tidTrans: newValue, resultMessage: "" });
    }

    render() {
        let { tidTrans, resultMessage } = this.state;
        return (
            <View style={styles.container}>
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    title={"Kiểm tra giao dịch"}
                />
                <View style={{ flex: 1, paddingBottom: 10 }}>
                    <View style={styles.formInput}>
                        <TextInput
                            autoFocus={true}
                            placeholderTextColor={"#929292"}
                            placeholder={"Nhập mã giao dịch"}
                            keyboardType={"numeric"}
                            value={tidTrans}
                            style={[styles.textInput, { fontSize: 18 }]}
                            onChangeText={this.onChangeText}
                            underlineColorAndroid="transparent"
                        />
                        {
                            resultMessage != "" ?
                                <Text style={{ fontSize: 16, color: '#ce3a3e', paddingHorizontal: 15, marginTop: 5 }}>{resultMessage}</Text>
                                : null
                        }
                    </View>
                </View>
                <Button title={"Tiếp tục"} style={{ marginHorizontal: 20, marginVertical: 10 }} onPressAction={this.onPressAction} />
                <MoMoModal ref="MoMoModal" disableCancel={true} />
            </View>
        );
    }
}