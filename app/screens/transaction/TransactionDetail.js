import React from 'react';
import { View, Text } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import styles from './styles';
import Button from '../../components/button/Button';
import MoMoHelper from '../../common/MoMoHelper';
export default class TransactionDetail extends React.Component {
    static navigationOptions = {
        header: null
    }

    onPressAction = () => {
        let { data, getDataHistory } = this.props.navigation.state.params;
        let parameters = {};
        parameters.data = data;
        parameters.getDataHistory = getDataHistory;
        this.props.navigation.navigate("transactionRefund", parameters);
    }

    buildFormData() {
        let { arrData } = this.props.navigation.state.params;
        return MoMoHelper.buildFormData(arrData);
    }

    buildFormDataRefund(arrRefundData) {
        return MoMoHelper.buildFormData(arrRefundData);
    }

    render() {
        let { isShowRefund, arrRefundData } = this.props.navigation.state.params;
        return (
            <View style={styles.container}>
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    title={"Thông tin giao dịch"}
                />
                <View style={{ flex: 1, paddingBottom: 10 }}>
                    <View style={{ backgroundColor: '#f2f2f2', paddingHorizontal: 15, paddingTop: 10, paddingBottom: 3 }}>
                        <Text style={{ fontSize: 16, color: '#4d4d4d' }}>{"THÔNG TIN THANH TOÁN"}</Text>
                    </View>
                    <View style={styles.formInput}>
                        {this.buildFormData()}
                    </View>

                    {
                        arrRefundData && arrRefundData.length > 0 ?
                            <View>
                                <View style={{ backgroundColor: '#f2f2f2', paddingHorizontal: 15, paddingTop: 10, paddingBottom: 3 }}>
                                    <Text style={{ fontSize: 16, color: '#4d4d4d' }}>{"THÔNG TIN HOÀN TIỀN"}</Text>
                                </View>
                                <View style={styles.formInput}>
                                    {this.buildFormDataRefund(arrRefundData)}
                                </View>
                            </View>
                            : null
                    }
                </View>
                {
                    isShowRefund ?
                        <Button title={"Hoàn tiền"} style={{ marginHorizontal: 20, marginVertical: 10 }} onPressAction={this.onPressAction} />
                        : null
                }
            </View>
        );
    }
}