import React from 'react';
import { View, Text, Image } from 'react-native';
import MoMoHeader from '../../components/header/MoMoHeader';
import styles from './styles';
import MoMoHelper from '../../common/MoMoHelper';
import MoMoModal from '../../components/modal';
import cache from '../../common/cache';
import HttpUtils from '../../common/HttpUtils';
import configs from '../../common/configs';
import DataApp from '../../data/DataApp';
export default class TransactionCheck extends React.Component {
    static navigationOptions = {
        header: null
    }
    state = {
        tidTrans: "",
        resultMessage: ""

    }

    _isMounted = false;

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onPressAction = () => {
        let { tidTrans } = this.state;
        if (tidTrans == "") {
            if (!this._isMounted) {
                return;
            }
            this.setState({ resultMessage: "Chưa nhập mã giao dịch" });
            return;
        }
        let params = {};
        params.tid = tidTrans;
        TransactionApi.checkTrans(params, (result) => {
            let errorMessage = "Đã có lỗi trong quá trình xử lý";
            if (result) {
                if (result.resultCode == 0) {
                    let parameters = {};
                    parameters.arrData = result.formData;
                    this.props.navigation.navigate("transactionDetail", parameters);
                    errorMessage = "";
                } else {
                    if (result.resultMessage) {
                        errorMessage = result.resultMessage;
                    }
                }
            }
            if (!this._isMounted) {
                return;
            }
            this.setState({ resultMessage: errorMessage });
        });
    }

    onChangeText = (value) => {
        if (!this._isMounted) {
            return;
        }
        this.setState({ tidTrans: value, resultMessage: "" });
    }

    buildFormData(profile) {
        let newArrData = [...DataApp.dataSetting];
        newArrData = newArrData.map((item, index) => {
            let newItem = { ...item };
            if (newItem.value == "%storeId%") {
                if (profile.storeId) {
                    newItem.value = profile.storeId;
                } else {
                    newItem.value = "";
                }
            }
            if (newItem.value == "%storeName%") {
                if (profile.storeName) {
                    newItem.value = profile.storeName;
                } else {
                    newItem.value = "";
                }
            }
            if (newItem.value == "%address%") {
                if (profile.storeAddress) {
                    newItem.value = profile.storeAddress;
                } else {
                    newItem.value = "";
                }
            }
            return newItem;
        });
        return MoMoHelper.buildFormData(newArrData);
    }

    onPressConfirm = () => {
        let paramOption = {};
        paramOption.URL = configs.URL_LOGOUT;
        paramOption.showLoading = true;
        HttpUtils.fetchPost({}, () => {
            MoMoHelper._clearCache();
            MoMoHelper.resetAndGotoPage(this.props.navigation, "login", null);
        }, paramOption);
    }

    onPressLogout = () => {
        if (this.refs.MoMoModal) {
            this.refs.MoMoModal.showModal("Đăng xuất", "Bạn muốn đăng xuất tài khoản này?", "ĐỒNG Ý", "KHÔNG", this.onPressCancel, this.onPressConfirm);
        }
    }

    render() {
        let profile = cache.USER_PROFILE;
        return (
            <View style={styles.container}>
                <MoMoHeader
                    navigation={this.props.navigation}
                    hadBack={true}
                    isLogout={true}
                    title={"Thông tin"}
                    onPressLogout={this.onPressLogout}
                />
                {
                    profile ?
                        <View style={{ flex: 1, paddingBottom: 10 }}>
                            <View style={[styles.formInput, { flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15, paddingTop: 5 }]}>
                                <View style={styles.icon}>
                                    <Image source={{ uri: configs.URL_ROOT_IMG + profile.avatarSignature }} resizeMode="contain"
                                        style={{ width: 35, height: 35 }} />
                                </View>
                                <Text style={{ fontSize: 16, color: '#4d4d4d', flex: 1 }}>{profile.merchantName}</Text>
                            </View>
                            <View style={[styles.formInput, { marginTop: 10, paddingBottom: 10 }]}>
                                {this.buildFormData(profile)}
                            </View>
                        </View>
                        : null
                }
                <MoMoModal ref="MoMoModal" disableCancel={true} />
            </View>
        );
    }
}