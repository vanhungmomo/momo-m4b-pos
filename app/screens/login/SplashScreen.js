import React, { Component } from 'react';
import { View, StatusBar, Image } from 'react-native';
import configs from '../../common/configs';
import MoMoHelper from '../../common/MoMoHelper';
import cache from '../../common/cache';
import * as ckey from '../../common/StorageKey';
export default class SplashScreen extends Component {

    static navigationOptions = {
        header: null
    }

    async componentDidMount() {
        // MoMoHelper._setCached(ckey.ACTIVE_SIGNATURE, "1dfawcfasa");
        // let activeSignature = await MoMoHelper._getCached(ckey.ACTIVE_SIGNATURE);
        // if (activeSignature && activeSignature != "") {
        //     cache.ACTIVE_SIGNATURE = activeSignature;
        //     let profile = await MoMoHelper._getCached(ckey.USER_PROFILE);
        //     let userName = await MoMoHelper._getCached(ckey.USER_NAME);
        //     let routerName = "login";
        //     if (profile && profile != "") {
        //         cache.JWT_TOKEN = profile.accessToken;
        //         cache.USER_PROFILE = profile;
        //         cache.USER_NAME = userName;
        //         routerName = "home";
        //     }
        //     MoMoHelper.resetAndGotoPage(this.props.navigation, routerName, null);
        // } else {
        //     MoMoHelper.resetAndGotoPage(this.props.navigation, "activeCode", null);
        // }

        let profile = await MoMoHelper._getCached(ckey.USER_PROFILE);
        let userName = await MoMoHelper._getCached(ckey.USER_NAME);
        let routerName = "login";
        if (profile && profile != "") {
            cache.JWT_TOKEN = profile.accessToken;
            cache.USER_PROFILE = profile;
            cache.USER_NAME = userName;
            routerName = "home";
        }
        MoMoHelper.resetAndGotoPage(this.props.navigation, routerName, null);
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: configs.backgroundColor, justifyContent: 'center', alignItems: 'center' }}>
                <StatusBar backgroundColor={configs.backgroundColor} barStyle={'light-content'} animated={true} />
                <Image source={require('../../images/logo/momo_logo.png')} style={{ width: 80, height: 80, marginBottom: 100 }} />
            </View>
        );
    }
}