import React, { Component } from 'react';
import { Dimensions } from 'react-native';
let { width, height } = Dimensions.get('window');
export default {
    container: {
        flex: 1,
        backgroundColor: 'red'
    },
    header: {
        alignItems: 'center',
        marginTop: 60
    },
    icon: {
        width: 60,
        height: 60
    },
    body: {
        marginTop: 10,
        width: width
    },
};