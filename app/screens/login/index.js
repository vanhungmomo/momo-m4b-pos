import React from 'react';
import { View, StatusBar, Dimensions, Image, KeyboardAvoidingView, ScrollView, Text } from 'react-native';
let { width, height } = Dimensions.get('window');
import configs from '../../common/configs';
import InputText from '../../components/input/InputText';
import Button from '../../components/button/Button';
import styles from './styles';
import MoMoHelper from '../../common/MoMoHelper';
import * as ckey from '../../common/StorageKey';
import cache from '../../common/cache';
import LoginApi from './LoginApi';
import MoMoReactToNative from '../../modules/MoMoReactToNative';
export default class Login extends React.Component {
    static navigationOptions = {
        header: null
    }

    _isMounted = false;

    state = {
        userName: "",
        pass: "",
        isShowClear: false,
        resultMessage: ""
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onPressAction = async () => {
        let { userName, pass } = this.state;
        let checkLogin = false;
        let sessionExp = false;
        let isLoggedIn = false;
        let gotoTrans = false;
        if (this.props && this.props.navigation && this.props.navigation.state && this.props.navigation.state.params) {
            checkLogin = this.props.navigation.state.params.isCheckLogin;
            sessionExp = this.props.navigation.state.params.sessionExp;
            gotoTrans = this.props.navigation.state.params.gotoTrans;
            if (sessionExp || checkLogin) {
                isLoggedIn = true;
            }
        }
        let userNameLogin = userName;
        if (isLoggedIn) {
            let userNameCache = await MoMoHelper._getCached(ckey.USER_NAME);
            if (userNameCache && userNameCache != "") {
                userNameLogin = userNameCache;
            }
        }

        if (userNameLogin.trim() == "" && !isLoggedIn) {
            if (!this._isMounted) {
                return;
            }
            this.setState({ resultMessage: "Tên đăng nhập không được để trống" });
            return;
        }
        if (pass.trim() == "") {
            if (!this._isMounted) {
                return;
            }
            this.setState({ resultMessage: "Mật khẩu không được để trống" });
            return;
        }
        let params = {};
        params.username = userNameLogin;
        params.password = pass;
        if (!gotoTrans) {
            let snKey = await MoMoReactToNative.getSN();
            let activeSignature = await MoMoHelper._getCached(ckey.ACTIVE_SIGNATURE);
            params.activeSignature = activeSignature;
            params.serialNumber = snKey;
        }
        if (gotoTrans) {
            LoginApi.handleVeriredLogin(params, async (result) => {
                if (result) {
                    console.log("handleVeriredLogin 1 " + JSON.stringify(result));
                    if (result.resultCode == 403) {
                        let paramsNew = {};
                        paramsNew.username = userNameLogin;
                        paramsNew.password = pass;
                        let snKeyNew = await MoMoReactToNative.getSN();
                        let activeSignatureNew = await MoMoHelper._getCached(ckey.ACTIVE_SIGNATURE);
                        paramsNew.activeSignature = activeSignatureNew;
                        paramsNew.serialNumber = snKeyNew;
                        this.handleLogin(paramsNew, false, true, userNameLogin);
                    } else if (result.resultCode == 0) {
                        this.props.navigation.navigate("history", {});
                    } else {
                        if (result.resultMessage) {
                            if (!this._isMounted) {
                                return;
                            }
                            this.setState({ resultMessage: result.resultMessage });
                        } else {
                            if (!this._isMounted) {
                                return;
                            }
                            this.setState({ resultMessage: "Đã có lỗi trong quá trình xử lý" });
                        }
                    }
                }
            }, false);
        } else {
            this.handleLogin(params, sessionExp, checkLogin, userNameLogin);
        }
    }

    handleLogin(params, sessionExp, checkLogin, userNameLogin) {
        LoginApi.handleLogin(params, (result) => {
            console.log("login " + JSON.stringify(result));
            if (result) {
                if (result.resultCode == 0) {
                    if (result.accessToken) {
                        cache.JWT_TOKEN = result.accessToken;
                        cache.USER_PROFILE = result;
                        cache.USER_NAME = userNameLogin;
                        MoMoHelper._setCached(ckey.JWT_TOKEN, result.accessToken);
                        MoMoHelper._setCached(ckey.USER_PROFILE, result);
                        MoMoHelper._setCached(ckey.USER_NAME, userNameLogin);
                    }
                    if (sessionExp) {
                        this.props.navigation.goBack();
                    } else {
                        if (!this._isMounted) {
                            return;
                        }
                        this.setState({ resultMessage: "" }, () => {
                            if (checkLogin) {
                                this.props.navigation.navigate("history", {});
                            } else {
                                MoMoHelper.resetAndGotoPage(this.props.navigation, "home", null);
                            }
                        });
                    }
                } else {
                    if (result.resultMessage) {
                        if (result.resultCode == 15) {
                            MoMoReactToNative.showAlertNative(
                                "Lỗi đăng nhập",
                                result.resultMessage,
                                "",
                                "ĐỒNG Ý",
                                false,
                                () => {
                                    MoMoHelper._clearCache();
                                    MoMoHelper._setCached(ckey.ACTIVE_SIGNATURE, "");
                                    MoMoHelper.resetAndGotoPage(this.props.navigation, "activeCode", null);
                                }
                            );
                        } else {
                            if (!this._isMounted) {
                                return;
                            }
                            this.setState({ resultMessage: result.resultMessage });
                        }
                    } else {
                        if (!this._isMounted) {
                            return;
                        }
                        this.setState({ resultMessage: "Đã có lỗi trong quá trình xử lý" });
                    }
                }
            } else {
                if (!this._isMounted) {
                    return;
                }
                this.setState({ resultMessage: "Đã có lỗi trong quá trình xử lý" });
            }
        }, checkLogin);
    }

    onPressActionHome = () => {
        this.props.navigation.goBack();
    }

    onPressClear = (typeInput) => {
        if (typeInput == "userName") {
            if (!this._isMounted) {
                return;
            }
            this.setState({ userName: "", isShowClear: false, resultMessage: "" });
        } else {
            if (!this._isMounted) {
                return;
            }
            this.setState({ pass: "", isShowClear: false, resultMessage: "" });
        }
    }

    onChangeText(type, value) {
        if (type == "userName") {
            if (!this._isMounted) {
                return;
            }
            this.setState({ userName: value, isShowClear: true, resultMessage: "" });
        } else {
            if (!this._isMounted) {
                return;
            }
            this.setState({ pass: value, isShowClear: true, resultMessage: "" });
        }
    }

    onUnFocus = (type) => {

    }

    onFocus = (type) => {

    }

    render() {
        let sessionExp = false;
        let isLoggedIn = false;
        let userName = "";
        let isLogin = false;
        if (this.props && this.props.navigation && this.props.navigation.state && this.props.navigation.state.params) {
            userName = cache.USER_NAME;
            sessionExp = this.props.navigation.state.params.sessionExp;
            isLogin = this.props.navigation.state.params.isCheckLogin;
            if (sessionExp || isLogin) {
                isLoggedIn = true;
            }
        }
        let { resultMessage } = this.state;
        return (
            <KeyboardAvoidingView style={styles.container}>
                <StatusBar backgroundColor={configs.backgroundColor} barStyle={'light-content'} animated={true} />
                <View style={{ flex: 1, backgroundColor: configs.backgroundColor, justifyContent: 'center', alignItems: 'center' }}>
                    <ScrollView style={{ height: height }} keyboardShouldPersistTaps="always">
                        {
                            !isLoggedIn ?
                                <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                                    <Image source={require('../../images/logo/momo_logo.png')} style={{ width: 80, height: 80, marginTop: 40, alignItems: 'center' }} />
                                </View>
                                : null
                        }
                        <View style={styles.body}>
                            {
                                !isLoggedIn ?
                                    <InputText
                                        autoFocus={true}
                                        textAlign={'center'}
                                        placeholderTextColor={"#929292"}
                                        backgroundInputColor={"#FFF"}
                                        color={"rgba(0,0,0,0.8)"}
                                        keyboardType={"default"}
                                        autoCapitalize={"none"}
                                        value={this.state.userName}
                                        typeInput={"userName"}
                                        onChangeText={(value) => this.onChangeText("userName", value)}
                                        sourceLeft={require('../../images/icon/ic_clear_none.png')}
                                        sourceRight={require('../../images/icon/ic_clear.png')}
                                        styleIconLeft={{ width: 15, height: 15 }}
                                        styleIconRight={{ width: 15, height: 15 }}
                                        placeholder={"Tên đăng nhập"}
                                        borderRadius={20}
                                        onUnFocus={this.onUnFocus}
                                        onFocus={this.onFocus}
                                        onPressClear={this.onPressClear} />
                                    :
                                    <View style={{ marginTop: 40, marginBottom: 10 }}>
                                        <Text style={{ fontSize: 18, color: "#FFF", textAlign: 'center', fontWeight: 'bold' }}>{"Tài khoản"}</Text>
                                        <Text style={{ fontSize: 18, color: "#FFF", textAlign: 'center' }}>{userName}</Text>
                                    </View>
                            }
                            <InputText
                                textAlign={'center'}
                                autoFocus={isLoggedIn ? true : false}
                                placeholderTextColor={"#929292"}
                                secureTextEntry={true}
                                keyboardType={"default"}
                                autoCapitalize={"none"}
                                value={this.state.pass}
                                typeInput={"pass"}
                                onChangeText={(value) => this.onChangeText("pass", value)}
                                placeholder={"Mật khẩu"}
                                backgroundInputColor={"#FFF"}
                                color={"rgba(0,0,0,0.8)"}
                                sourceLeft={require('../../images/icon/ic_clear_none.png')}
                                sourceRight={require('../../images/icon/ic_clear.png')}
                                styleIconLeft={{ width: 15, height: 15 }}
                                styleIconRight={{ width: 15, height: 15 }}
                                borderRadius={20}
                                onUnFocus={this.onUnFocus}
                                onFocus={this.onFocus}
                                onPressClear={this.onPressClear} />
                            {
                                resultMessage != "" ?
                                    <Text style={{ fontSize: 16, color: "#fff469", textAlign: 'center', marginVertical: 5 }}>{resultMessage}</Text>
                                    : null
                            }
                            <Button title={isLoggedIn ? "ĐĂNG NHẬP" : "XÁC NHẬN"} style={{ marginTop: 20, borderRadius: 20 }} onPressAction={this.onPressAction} style={{ backgroundColor: "#8D015A", borderRadius: 20 }} />
                            {
                                isLogin ?
                                    <Button title={"Màn hình chính".toUpperCase()} style={{ marginHorizontal: 20, backgroundColor: 'transparent' }} onPressAction={this.onPressActionHome} />
                                    : null
                            }
                        </View>
                    </ScrollView>
                </View>
            </KeyboardAvoidingView>
        );
    }
}