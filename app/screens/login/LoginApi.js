import HttpUtils from '../../common/HttpUtils';
import configs from '../../common/configs';
export default {
    handleLogin(params, callback, isCheckLogin = false) {
        let paramOption = {};
        paramOption.URL = configs.URL_LOGIN;
        paramOption.isCheckLogin = isCheckLogin;
        paramOption.showLoading = true;
        HttpUtils.fetchPost(params, callback, paramOption);
    },

    handleVeriredLogin(params, callback, isCheckLogin = false, gotoTrans = false) {
        let paramOption = {};
        paramOption.URL = configs.URL_VERIFIED;
        paramOption.isCheckLogin = isCheckLogin;
        paramOption.gotoTrans = gotoTrans;
        paramOption.showLoading = true;
        HttpUtils.fetchPost(params, callback, paramOption);
    },

    handleActiveCode(params, callback, isCheckLogin = false) {
        let paramOption = {};
        paramOption.URL = configs.URL_ACTIVE;
        paramOption.isCheckLogin = isCheckLogin;
        paramOption.showLoading = true;
        HttpUtils.fetchPost(params, callback, paramOption);
    },

    handleWakeUp(params, callback) {
        let paramOption = {};
        paramOption.URL = configs.URL_WAKE_UP;
        HttpUtils.fetchPost(params, callback, paramOption);
    }
}