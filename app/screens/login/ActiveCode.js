import React from 'react';
import { View, StatusBar, Dimensions, Image, KeyboardAvoidingView, ScrollView, Text } from 'react-native';
let { width, height } = Dimensions.get('window');
import configs from '../../common/configs';
import InputText from '../../components/input/InputText';
import Button from '../../components/button/Button';
import styles from './styles';
import cache from '../../common/cache';
import MoMoHelper from '../../common/MoMoHelper';
import * as ckey from '../../common/StorageKey';
import MoMoReactToNative from '../../modules/MoMoReactToNative';
import LoginApi from './LoginApi';
export default class ActiveCode extends React.Component {
    static navigationOptions = {
        header: null
    }
    _isMounted = false;

    state = {
        codeInput: "",
        isShowClear: false,
        resultMessage: ""
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onPressAction = async () => {
        let { codeInput } = this.state;
        let snKey = await MoMoReactToNative.getSN();
        console.log("data_sn_number " + snKey);
        if (codeInput.trim() == "") {
            if (!this._isMounted) {
                return;
            }
            this.setState({ resultMessage: "Mã xác thực không được để trống" });
            return;
        }
        if (snKey && codeInput != "") {
            let params = {};
            params.activeCode = codeInput;
            params.serialNumber = snKey;
            LoginApi.handleActiveCode(params, (result) => {
                if (result) {
                    if (result.resultCode == 0) {
                        cache.ACTIVE_SIGNATURE = result.activeSignature;
                        MoMoHelper._setCached(ckey.ACTIVE_SIGNATURE, result.activeSignature);
                        MoMoHelper.resetAndGotoPage(this.props.navigation, "login", null);
                    } else {
                        if (!this._isMounted) {
                            return;
                        }
                        this.setState({ resultMessage: result.resultMessage });
                    }
                }
            });
        }
    }


    onPressClear = (typeInput) => {
        if (!this._isMounted) {
            return;
        }
        this.setState({ codeInput: "", isShowClear: false, resultMessage: "" });
    }

    onChangeText(type, value) {
        if (!this._isMounted) {
            return;
        }
        this.setState({ codeInput: value, isShowClear: true, resultMessage: "" });
    }

    onUnFocus = (type) => {

    }

    onFocus = (type) => {

    }

    render() {
        let { resultMessage } = this.state;
        return (
            <KeyboardAvoidingView style={styles.container}>
                <StatusBar backgroundColor={configs.backgroundColor} barStyle={'light-content'} animated={true} />
                <View style={{ flex: 1, backgroundColor: configs.backgroundColor, justifyContent: 'center', alignItems: 'center' }}>
                    <ScrollView style={{ height: height }} keyboardShouldPersistTaps="always">
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                            <Image source={require('../../images/logo/momo_logo.png')} style={{ width: 80, height: 80, marginTop: 40, alignItems: 'center' }} />
                        </View>
                        {
                            resultMessage != "" ?
                                <Text style={{ fontSize: 16, color: "#fff469", textAlign: 'center', marginVertical: 5 }}>{resultMessage}</Text>
                                : null
                        }
                        <View style={styles.body}>
                            <InputText
                                autoFocus={true}
                                textAlign={'center'}
                                placeholderTextColor={"#929292"}
                                backgroundInputColor={"#FFF"}
                                color={"rgba(0,0,0,0.8)"}
                                autoCapitalize={"none"}
                                value={this.state.codeInput}
                                keyboardType={"numeric"}
                                typeInput={"userName"}
                                onChangeText={(value) => this.onChangeText("codeInput", value)}
                                sourceRight={require('../../images/icon/ic_clear.png')}
                                styleIconRight={{ width: 15, height: 15 }}
                                placeholder={"Nhập mã xác thực"}
                                borderRadius={20}
                                onUnFocus={this.onUnFocus}
                                onFocus={this.onFocus}
                                onPressClear={this.onPressClear} />
                            <Button title={"XÁC THỰC"} style={{ marginTop: 20, borderRadius: 20 }} onPressAction={this.onPressAction} style={{ backgroundColor: "#8D015A", borderRadius: 20 }} />
                        </View>
                    </ScrollView>
                </View>
            </KeyboardAvoidingView>
        );
    }
}