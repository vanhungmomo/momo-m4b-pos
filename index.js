import { AppRegistry, YellowBox } from 'react-native';
import setup from './app/setup';
if (!__DEV__) {
    console.log = () => { };
}
// YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'])
AppRegistry.registerComponent('momopos', () => setup);