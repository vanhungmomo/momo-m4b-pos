package com.momopos;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import com.facebook.react.ReactActivity;
import com.momopos.modules.Exponent;
import com.momopos.utils.AidlUtil;
import woyou.aidlservice.jiuiv5.IWoyouService;

public class MainActivity extends ReactActivity {

    private static IWoyouService woyouService;
    private ServiceConnection connService = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        pref.edit().putString("debug_http_host","10.20.28.111:8081").commit();

        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        bindService(intent, connService, Context.BIND_AUTO_CREATE);

    }

    public static IWoyouService getWoyouService() {
        return woyouService;
    }

    @Override
    protected String getMainComponentName() {
        return "momopos";
    }

    @Override
    protected void onResume() {
        super.onResume();
        Exponent.initialize(this, getApplication());
        Exponent.getInstance().setCurrentActivity(this);
        AidlUtil.getInstance().initPrinter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(connService);
    }
}
