package com.momopos.modules.barcodescanner;


import android.support.annotation.Nullable;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BarCodeScannerViewManager extends ViewGroupManager<BarCodeScannerView> {
    private static final String REACT_CLASS = "ExponentBarCodeScanner";

    public static final int COMMAND_START_CAMERA = 1;
    public static final int COMMAND_STOP_CAMERA = 0;
    public static final int COMMAND_READ_IMAGE_QR_CODE = 2;

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public BarCodeScannerView createViewInstance(ThemedReactContext themedReactContext) {
        return new BarCodeScannerView(themedReactContext);
    }

    @Override
    @Nullable
    public Map getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder builder = MapBuilder.builder();
        for (BarCodeScannerView.Events event : BarCodeScannerView.Events.values()) {
            builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
        }
        return builder.build();
    }

    @ReactProp(name = "type")
    public void setType(BarCodeScannerView view, int type) {
        view.setCameraType(type);
    }

    @ReactProp(name = "torchMode")
    public void setTorchMode(BarCodeScannerView view, int torchMode) {
        view.setTorchMode(torchMode);
    }

    @ReactProp(name = "barCodeTypes")
    public void setBarCodeTypes(BarCodeScannerView view, ReadableArray barCodeTypes) {
        if (barCodeTypes == null) {
            return;
        }
        List<String> result = new ArrayList<String>(barCodeTypes.size());
        for (int i = 0; i < barCodeTypes.size(); i++) {
            result.add(barCodeTypes.getString(i));
        }
        view.setBarCodeTypes(result);
    }

    @javax.annotation.Nullable
    @Override
    public Map<String, Integer> getCommandsMap() {
        return MapBuilder.of(
                "startCamera", COMMAND_START_CAMERA,
                "stopCamera", COMMAND_STOP_CAMERA,
                "readImageQRCode", COMMAND_READ_IMAGE_QR_CODE);
    }

    @Override
    public void receiveCommand(BarCodeScannerView view, int commandId, @javax.annotation.Nullable ReadableArray args) {
        super.receiveCommand(view, commandId, args);
        switch (commandId) {
            case COMMAND_START_CAMERA:
                view.startCamera();
                break;
            case COMMAND_STOP_CAMERA:
                view.stopCamera();
                break;
            case COMMAND_READ_IMAGE_QR_CODE:
                if (args != null && args.size() > 0) {
                    view.readImageQRCode(args.getString(0));
                }
                break;
        }
    }
}
