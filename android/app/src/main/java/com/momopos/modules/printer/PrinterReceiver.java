package com.momopos.modules.printer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.facebook.react.modules.core.DeviceEventManagerModule;
public class PrinterReceiver extends BroadcastReceiver {
    public PrinterReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent data) {
        try{
            String action = data.getAction();
            String type = "PrinterStatus";
            SunmiInnerPrinterModule.reactApplicationContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit(type, action);
        }catch (Exception e){

        }
    }
}