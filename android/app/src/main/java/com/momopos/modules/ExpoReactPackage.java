package com.momopos.modules;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.momopos.modules.barcodescanner.BarCodeScannerModule;
import com.momopos.modules.barcodescanner.BarCodeScannerViewManager;
import com.momopos.modules.permissions.PermissionsModule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vanhungpc on 6/4/18.
 */

public class ExpoReactPackage implements ReactPackage {

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        List<NativeModule> nativeModules = new ArrayList<>();
        nativeModules.add(new BarCodeScannerModule(reactContext));
        nativeModules.add(new PermissionsModule(reactContext));
        return nativeModules;
    }


    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        List<ViewManager> viewManagers = new ArrayList<>(Arrays.<ViewManager>asList(
                new BarCodeScannerViewManager()
        ));

        return viewManagers;
    }
}
