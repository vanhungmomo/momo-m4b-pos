package com.momopos.modules.barcodescanner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.view.TextureView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.google.zxing.*;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.qrcode.QRCodeMultiReader;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;

class BarCodeScannerViewFinder extends TextureView implements TextureView.SurfaceTextureListener, Camera.PreviewCallback {
    private  boolean mIsLoaded = false;
    private int mCameraType;
    private SurfaceTexture mSurfaceTexture;
    private boolean mIsStarting;
    private boolean mIsStopping;
    private boolean mIsChanging;
    private BarCodeScannerView mBarCodeScannerView;
    private Camera mCamera;

    // Concurrency lock for barcode scanner to avoid flooding the runtime
    public static volatile boolean barCodeScannerTaskLock = false;

    // Reader instance for the barcode scanner
    private final MultiFormatReader mMultiFormatReader = new MultiFormatReader();

    public BarCodeScannerViewFinder(Context context, int type, BarCodeScannerView barCodeScannerView) {
        super(context);
        this.setSurfaceTextureListener(this);
        mCameraType = type;
        mBarCodeScannerView = barCodeScannerView;
        this.initBarcodeReader(BarCodeScanner.getInstance().getBarCodeTypes());
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mSurfaceTexture = surface;
        startCamera();
        /*if (!mIsLoaded) {
            Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                public void run() {
                    startCamera();
                    mIsLoaded = true;
                }
            };
            handler.postDelayed(runnable, 300);
        } else {
            startCamera();
        }*/
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        mSurfaceTexture = null;
        stopCamera();
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        mSurfaceTexture = surface;
    }

    public double getRatio() {
        int width = BarCodeScanner.getInstance().getPreviewWidth(this.mCameraType);
        int height = BarCodeScanner.getInstance().getPreviewHeight(this.mCameraType);
        return ((float) width) / ((float) height);
    }

    public void setCameraType(final int type) {
        if (this.mCameraType == type) {
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                mIsChanging = true;
                stopPreview();
                mCameraType = type;
                startPreview();
                mIsChanging = false;
            }
        }).start();
    }

    public void setTorchMode(int torchMode) {
        BarCodeScanner.getInstance().setTorchMode(torchMode);
    }

    public void start() {
        startPreview();
    }
    public void stop() {
        stopPreview();
    }

    private void startPreview() {
        if (mSurfaceTexture != null) {
            startCamera();
        }
    }

    private void stopPreview() {
        if (mCamera != null) {
            stopCamera();
        }
    }

    synchronized private void startCamera() {
        if (!mIsStarting) {
            mIsStarting = true;
            try {
                mCamera = BarCodeScanner.getInstance().acquireCameraInstance(mCameraType);
                Camera.Parameters parameters = mCamera.getParameters();
                // set autofocus
                List<String> focusModes = parameters.getSupportedFocusModes();
                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                }
                // set picture size
                // defaults to max available size
                Camera.Size optimalPictureSize = BarCodeScanner.getInstance().getBestSize(
                        parameters.getSupportedPictureSizes(),
                        Integer.MAX_VALUE,
                        Integer.MAX_VALUE
                );
                parameters.setPictureSize(optimalPictureSize.width, optimalPictureSize.height);

                mCamera.setParameters(parameters);
                mCamera.setPreviewTexture(mSurfaceTexture);
                mCamera.startPreview();
                // send previews to `onPreviewFrame`
                mCamera.setPreviewCallback(BarCodeScannerViewFinder.this);
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                stopCamera();
            } finally {
                mIsStarting = false;
            }
        }
    }

    synchronized private void stopCamera() {
        if (!mIsStopping) {
            mIsStopping = true;
            try {
                if (mCamera != null) {
                    mCamera.stopPreview();
                    // stop sending previews to `onPreviewFrame`
                    mCamera.setPreviewCallback(null);
                    BarCodeScanner.getInstance().releaseCameraInstance();
                    mCamera = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mIsStopping = false;
            }
        }
    }

    /**
     * Initialize the barcode decoder.
     * Supports all iOS codes except [code138, code39mod43, itf14]
     * Additionally supports [codabar, code128, maxicode, rss14, rssexpanded, upc_a, upc_ean]
     */
    private void initBarcodeReader(List<String> barCodeTypes) {
        EnumMap<DecodeHintType, Object> hints = new EnumMap<>(DecodeHintType.class);
        EnumSet<BarcodeFormat> decodeFormats = EnumSet.noneOf(BarcodeFormat.class);

        if (barCodeTypes != null) {
            for (String code : barCodeTypes) {
                String formatString = (String) BarCodeScannerModule.VALID_BARCODE_TYPES.get(code);
                if (formatString != null) {
                    decodeFormats.add(BarcodeFormat.valueOf(code));
                }
            }
        }

        hints.put(DecodeHintType.POSSIBLE_FORMATS, decodeFormats);
        mMultiFormatReader.setHints(hints);
    }

    public void onPreviewFrame(byte[] data, Camera camera) {
        if (!BarCodeScannerViewFinder.barCodeScannerTaskLock) {
            BarCodeScannerViewFinder.barCodeScannerTaskLock = true;
            new ReaderAsyncTask(camera, data, mBarCodeScannerView).execute();
        }
    }

    public void onReadImage(String base64) {
        byte[] bytes = Base64.decode(base64, 0);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int[] pixels = new int[width * height];
            bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

            try {
                RGBLuminanceSource source = new RGBLuminanceSource(width, height, pixels);
                BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(source));
                QRCodeMultiReader reader = new QRCodeMultiReader();
                Result result = reader.decodeMultiple(binaryBitmap)[0];
                sendResult(result.getBarcodeFormat().toString(), result.getText());
            } catch (Exception e) {
                // Unhandled error, unsure what would cause this
                sendResult("QR_CODE", null);
            }

            mMultiFormatReader.reset();
        }
    }

    private void sendResult(final String type, final String data) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (BarCodeScanner.getInstance().getBarCodeTypes().contains(type)) {
                    //stopCamera();
                    WritableMap event = Arguments.createMap();
                    event.putString("type", type);
                    event.putString("data", data);
                    mBarCodeScannerView.onBarCodeRead(event);
                }
            }
        });
    }

    private class ReaderAsyncTask extends AsyncTask<Void, Void, Void> {
        private byte[] mImageData;
        private final Camera mCamera;

        ReaderAsyncTask(Camera camera, byte[] imageData, BarCodeScannerView barCodeScannerView) {
            mCamera = camera;
            mImageData = imageData;
            mBarCodeScannerView = barCodeScannerView;
        }

        @Override
        protected Void doInBackground(Void... ignored) {
            if (isCancelled()) {
                return null;
            }
            try {
                // setting PreviewCallback does not really have an effect - this method is called anyway so we
                // need to check if camera changing is in progress or not
                if (!mIsChanging) {
                    Camera.Size size = mCamera.getParameters().getPreviewSize();

                    int width = size.width;
                    int height = size.height;

                    // rotate for zxing if orientation is portrait
                    if (BarCodeScanner.getInstance().getActualDeviceOrientation() == 0) {
                        byte[] rotated = new byte[mImageData.length];
                        for (int y = 0; y < height; y++) {
                            for (int x = 0; x < width; x++) {
                                rotated[x * height + height - y - 1] = mImageData[x + y * width];
                            }
                        }
                        width = size.height;
                        height = size.width;
                        mImageData = rotated;
                    }

                    PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(mImageData, width, height, 0, 0, width, height, false);
                    BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
                    Result result = mMultiFormatReader.decodeWithState(bitmap);
                    sendResult(result.getBarcodeFormat().toString(), result.getText());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            mMultiFormatReader.reset();
            BarCodeScannerViewFinder.barCodeScannerTaskLock = false;
            return null;
        }
    }
}
