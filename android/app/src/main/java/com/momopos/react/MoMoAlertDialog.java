package com.momopos.react;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.momopos.R;

/**
 * Created by vanhungpc on 6/14/18.
 */

public class MoMoAlertDialog {
    private boolean mCancel;
    private Context mContext;
    private AlertDialog mAlertDialog;
    private Builder mBuilder;
    private boolean mHasHeader = false;
    private int mTitleResId;
    private CharSequence mTitle;
    private int mMessageResId;
    private CharSequence mMessage;
    private boolean mHasShow = false;
    private DialogInterface.OnDismissListener mOnDismissListener;
    private int pId = -1, nId = -1;
    private String pText, nText;
    View.OnClickListener pListener, nListener;
    private boolean isButtonVertical = false;

    private boolean isShowImageHeader = false;

    public MoMoAlertDialog(Context context) {
        this.mContext = context;
    }

    public void show() {
        if (!mHasShow) {
            mBuilder = new Builder();
        } else {
            mAlertDialog.show();
        }
        mHasShow = true;
    }

    public void dismiss() {
        mAlertDialog.dismiss();
    }

    public void setEnableHeader(boolean value) {
        mHasHeader = value;
        if (mBuilder != null) {
            mBuilder.setEnableHeader(value);
        }

    }

    public void setIsButtonVertical(boolean _isButtonVertical) {
        isButtonVertical = _isButtonVertical;
    }

    public void setIsShowImageHeader(boolean _imgHeader){ isShowImageHeader = _imgHeader;}

    public MoMoAlertDialog setTitle(int resId) {
        mTitleResId = resId;
        if (mBuilder != null) {
            mBuilder.setTitle(resId);
        }
        return this;
    }

    public MoMoAlertDialog setTitle(CharSequence title) {
        mTitle = title;
        if (mBuilder != null) {
            mBuilder.setTitle(title);
        }
        return this;
    }

    public MoMoAlertDialog setMessage(int resId) {
        mMessageResId = resId;
        if (mBuilder != null) {
            mBuilder.setMessage(resId);
        }
        return this;
    }

    public MoMoAlertDialog setMessage(CharSequence message) {
        mMessage = message;
        if (mBuilder != null) {
            mBuilder.setMessage(message);
        }
        return this;
    }

    public MoMoAlertDialog setPositiveButton(int resId, final View.OnClickListener listener) {
        this.pId = resId;
        this.pListener = listener;
        return this;
    }

    public MoMoAlertDialog setPositiveButton(String text, final View.OnClickListener listener) {
        this.pText = text;
        this.pListener = listener;
        return this;
    }

    public MoMoAlertDialog setNegativeButton(int resId, final View.OnClickListener listener) {
        this.nId = resId;
        this.nListener = listener;
        return this;
    }

    public MoMoAlertDialog setNegativeButton(String text, final View.OnClickListener listener) {
        this.nText = text;
        this.nListener = listener;
        return this;
    }

    public MoMoAlertDialog setCanceledOnTouchOutside(boolean cancel) {
        this.mCancel = cancel;
        if (mBuilder != null) {
            mBuilder.setCanceledOnTouchOutside(mCancel);
        }
        return this;
    }

    public MoMoAlertDialog setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.mOnDismissListener = onDismissListener;
        return this;
    }

    private class Builder {

        private Window mAlertDialogWindow;
        private ImageView mHeaderView;
        private TextView mTitleView;
        private TextView mMessageView;
        private TextView mNegativeView, mNegativeViewV;
        private TextView mPositiveView, mPositiveViewV;
        private LinearLayout lnButtonHorizontal, lnButtonVertical;


        private Builder() {

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_momo_alert_dialog, null);

            builder.setView(contentView);

            lnButtonHorizontal = (LinearLayout)contentView.findViewById(R.id.lnButtonHorizontal);
            lnButtonVertical = (LinearLayout)contentView.findViewById(R.id.lnButtonVertical);
            mNegativeViewV = (TextView) contentView.findViewById(R.id.btnNegativeV);
            mPositiveViewV = (TextView) contentView.findViewById(R.id.btnPositiveV);

            mHeaderView = (ImageView) contentView.findViewById(R.id.imgHeader);
            mTitleView = (TextView) contentView.findViewById(R.id.txtTitle);
            mMessageView = (TextView) contentView.findViewById(R.id.txtMessage);
            mNegativeView = (TextView) contentView.findViewById(R.id.btnNegative);
            mPositiveView = (TextView) contentView.findViewById(R.id.btnPositive);

            mAlertDialog = builder.create();
            mAlertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            /*mAlertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            if (Build.VERSION.SDK_INT > 19) {//Android 6
                mAlertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_PANEL);
            } else {
                mAlertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            }*/

            if(isButtonVertical){
                lnButtonHorizontal.setVisibility(View.GONE);
                lnButtonVertical.setVisibility(View.VISIBLE);
            }else{
                lnButtonHorizontal.setVisibility(View.VISIBLE);
                lnButtonVertical.setVisibility(View.GONE);
            }

            mAlertDialog.getWindow()
                    .clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                            WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            mAlertDialog.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MASK_STATE);
            //mAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            mAlertDialog.show();

            setEnableHeader(mHasHeader);

            if (mTitleResId != 0) {
                setTitle(mTitleResId);
            }
            if (mTitle != null) {
                setTitle(mTitle);
            }
            if ((mTitle == null && mTitleResId == 0) || mTitleView.equals("")) {
                mTitleView.setVisibility(View.GONE);
                ((LinearLayout)mTitleView.getParent()).setVisibility(View.GONE);
            }
            if (mMessageResId != 0) {
                setMessage(mMessageResId);
            }
            if (mMessage != null) {
                setMessage(mMessage);
            }
            if (pId != -1) {
                mPositiveView.setVisibility(View.VISIBLE);
                mPositiveView.setText(pId);
                mPositiveView.setOnClickListener(pListener);

                mPositiveViewV.setVisibility(View.VISIBLE);
                mPositiveViewV.setText(pId);
                mPositiveViewV.setOnClickListener(pListener);
            }
            if (nId != -1) {
                mNegativeView.setVisibility(View.VISIBLE);
                mNegativeView.setText(nId);
                mNegativeView.setOnClickListener(nListener);

                mNegativeViewV.setVisibility(View.VISIBLE);
                mNegativeViewV.setText(nId);
                mNegativeViewV.setOnClickListener(nListener);
            }
            if (!isNullOrEmpty(pText)) {
                mPositiveView.setVisibility(View.VISIBLE);
                mPositiveView.setText(pText);
                mPositiveView.setOnClickListener(pListener);

                mPositiveViewV.setVisibility(View.VISIBLE);
                mPositiveViewV.setText(pText);
                mPositiveViewV.setOnClickListener(pListener);
            }
            if (!isNullOrEmpty(nText)) {
                mNegativeView.setVisibility(View.VISIBLE);
                mNegativeView.setText(nText);
                mNegativeView.setOnClickListener(nListener);

                mNegativeViewV.setVisibility(View.VISIBLE);
                mNegativeViewV.setText(nText);
                mNegativeViewV.setOnClickListener(nListener);
            }
            if (isNullOrEmpty(pText) && pId == -1) {
                mPositiveView.setVisibility(View.GONE);
                mPositiveViewV.setVisibility(View.GONE);
            }
            if (isNullOrEmpty(nText) && nId == -1) {
                mNegativeView.setVisibility(View.GONE);

                mNegativeViewV.setVisibility(View.GONE);
            }

            mAlertDialog.setCanceledOnTouchOutside(mCancel);
            mAlertDialog.setCancelable(mCancel);
            if (mOnDismissListener != null) {
                mAlertDialog.setOnDismissListener(mOnDismissListener);
            }
        }

        public void setEnableHeader(boolean value) {
            if (value) {
                mHeaderView.setVisibility(View.VISIBLE);
            } else {
                mHeaderView.setVisibility(View.GONE);
            }
        }

        public void setTitle(int resId) {
            mTitleView.setText(resId);
        }

        public void setTitle(CharSequence title) {
            mTitleView.setText(title);
        }

        public void setMessage(int resId) {
            if (mMessageView != null) {
                mMessageView.setText(resId);
            }
        }

        public void setMessage(CharSequence message) {
            if (mMessageView != null) {
                mMessageView.setText(message);
            }
        }

        public void setCanceledOnTouchOutside(boolean canceledOnTouchOutside) {
            mAlertDialog.setCanceledOnTouchOutside(canceledOnTouchOutside);
            mAlertDialog.setCancelable(canceledOnTouchOutside);
        }
    }

    private boolean isNullOrEmpty(String text) {
        return text == null || text.isEmpty();
    }
}
