package com.momopos.react.helper;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;


/**
 * Day la lop lay thong tin ve thiet bi dang chay ung dung.
 * Cac thong tin can lay nhu System Os, Device Name, Device Version, App version.
 *
 * @author Luat Le
 * @Created by 20-11-2013
 */
public class DevicesUtils {

    /**
     * Lấy chiều rộng của thiết bị
     * @param ctx
     * @return
     */
    public static int getDeviceWidth(Context ctx) {
        WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int mScreenWidth;
        if (Build.VERSION.SDK_INT >= 13) {
            Point size = new Point();
            display.getSize(size);
            mScreenWidth = size.x;
        } else {
            mScreenWidth = display.getWidth();
        }
        return mScreenWidth;
    }

    /**
     * Lấy chiều cao của thiết bị
     * @param ctx
     * @return
     */
    public static int getDeviceHeight(Context ctx) {
        WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int mScreenHeight;
        if (Build.VERSION.SDK_INT >= 13) {
            Point size = new Point();
            display.getSize(size);
            mScreenHeight = size.y;
        } else {
            mScreenHeight = display.getWidth();
        }
        return mScreenHeight;
    }

    /**
     * Get IP address from first non-localhost interface
     * @param useIPv4  true=return ipv4, false=return ipv6
     * @return  address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    /**
     * Returns MAC address of the given interface name.
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return  mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac==null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx=0; idx<mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
                return buf.toString();
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }



    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        try {
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                return true;
            }
            return false;
        }
        catch (Exception e){
            return false;
        }

    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static String getNumberSecretFromFile(Context context){
        if(context != null) {
            File folder = createFolder(context);
            if (folder != null) {
                File file_1 = createFile(folder);
                int length = (int) file_1.length();
                byte[] bytes = new byte[length];

                FileInputStream in = null;
                try {
                    in = new FileInputStream(file_1);
                    in.read(bytes);
                    in.close();
                    String temp = new String(bytes);
                    return temp;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return null;
    }

    public static boolean setNumberSecretToFile(Context context, String imeiString) {
        if(context != null) {
            File folder = createFolder(context);

            if (folder != null) {
                File file_1 = createFile(folder);
                FileOutputStream stream = null;
                try {
                    stream = new FileOutputStream(file_1);
                    stream.write(imeiString.getBytes());
                    stream.close();
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            } else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public static File createFolder(Context context){
        File folder = null;
        if( isExternalStorageWritable() && checkPermission(context)){
            folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES), "system/data/etc/com/m/o/m/o");
        }else{
            if(context == null){
                return null;
            }else{
                folder = new File(context.getFilesDir(), "system/data/etc/com/m/o/m/o");
            }
        }
        if(folder.exists()){
            return folder;
        }else{
            if (folder.mkdirs()) {
                return folder;
            }else{
                //ko tạo được thư mục
                return null;
            }
        }
    }

    private static File createFile(File folder){
        File file_1 = new File(folder,"hangouts_incoming_message.txt");
        if(file_1.exists()){
            return file_1;
        }else{
            try{
                file_1.createNewFile();
                return file_1;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private static boolean checkPermission(Context context) {
        if(context != null) {
            int result = ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        }
        else {
            return false;
        }
    }


}