package com.momopos.react.helper.qr;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by buian on 5/2/18.
 */

public class ImagePickerHelper {
    public static final String DEFAULT_FORMAT_COMPRESS = "jpg";
    public static final int DEFAULT_SIZE_COMPRESS = 80;

    public static Bitmap getScreenShot(View view) {
        View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public static boolean saveImageBase64ToLibrary(Context context, String base64Image) {
        try {
            if (context == null || TextUtils.isEmpty(base64Image))
                return false;
            byte[] decodeString = Base64.decode(base64Image, Base64.DEFAULT);
            Bitmap icon = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
            String name = null; //auto gen name by milliseconds
            String desc = "desc";
            boolean success = saveBitmapToLibrary(context, icon, name, desc);
            if (success)
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean saveBitmapToLibrary(Context context, Bitmap bitmap, String imgName, String imgDesc) {
        String result = "";
        if (imgName == null)
            imgName = SystemClock.uptimeMillis() + "";
        if (imgDesc == null)
            imgDesc = "";
        try {
            if (bitmap != null && context != null) {
                result = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, imgName, imgDesc);
            }
        } catch (Exception ex) {

        } finally {
            return result != "";
        }
    }

    public static String getImageBase64FromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            URLConnection connection = url.openConnection();
            InputStream is = connection.getInputStream();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int read = 0;
            while ((read = is.read(buffer, 0, buffer.length)) != -1) {
                os.write(buffer, 0, read);
            }
            os.flush();
            return Base64.encodeToString(os.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            return "";
        }
    }

    public static String getImageBase64FromBitmap(Bitmap bitmap, int imageSize) {
        return ImagePickerHelper.getCompressImageBase64FromBitmap(bitmap, imageSize, DEFAULT_SIZE_COMPRESS, DEFAULT_FORMAT_COMPRESS);
    }

    private static Bitmap.CompressFormat getFormat(String format){
        switch (format){
            case "png":
                return Bitmap.CompressFormat.PNG;
            case "webp":
                return Bitmap.CompressFormat.WEBP;
            case DEFAULT_FORMAT_COMPRESS:
            default:
                return Bitmap.CompressFormat.JPEG;
        }
    }

    public static String getCompressImageBase64FromBitmap(Bitmap bitmap, int imageSize, int sizeCompress, String format){
        if (bitmap == null)
            return "";
        if(sizeCompress <= 0) return "";

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (imageSize <= 0) {
            imageSize = 512;
        }
        float scaleWidth = imageSize;
        float scaleHeight = scaleWidth / width * height;
        String img_str = "";
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap resizedbitmap = Bitmap.createScaledBitmap(bitmap, Math.round(scaleWidth), Math.round(scaleHeight), true);
            resizedbitmap.compress(getFormat(format), sizeCompress, stream);

            byte[] image = stream.toByteArray();
            img_str = Base64.encodeToString(image, 0);

            if (stream != null) {
                stream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return img_str;
    }

    public static File storeBitMap(Bitmap bm, String fileName) {
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dirPath, fileName);
        if (file.exists()){
//            Log.d("MomoreactHelper", "storeBitMap: file is exists");
            file.delete();
        }

        file = new File(dirPath, fileName);
        if (file.exists()){
//            Log.d("MomoreactHelper", "storeBitMap: file is exists 2");
            file.delete();
        }

        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
