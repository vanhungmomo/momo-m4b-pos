package com.momopos.react;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.RemoteException;
import android.support.v4.print.PrintHelper;
import android.view.View;
import android.widget.Toast;

import com.facebook.react.bridge.Callback;
import com.momopos.MainActivity;
import com.momopos.MainApplication;
import com.momopos.utils.AidlUtil;
import com.momopos.utils.BluetoothUtil;
import com.momopos.utils.ESCUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * Created by vanhungpc on 6/8/18.
 */

public class MoMoReactHelper {
    public static MoMoReactHelper instance;
    public static MoMoReactHelper getInstance(){
        if(instance == null)
            instance = new MoMoReactHelper();
        return instance;
    }

    public static Bitmap getBitmapFromURL(final String src, final String token) {
        Bitmap myBitmap = null;
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty ("Authorization", "Bearer " +token);
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            myBitmap = BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myBitmap;
    }

    public void actionPrint(final Activity mActivity, final String content, final String token){
        final MainApplication baseApp = (MainApplication) mActivity.getApplication();
        Bitmap bitmap = getBitmapFromURL(content, token);
//        PrintHelper photoPrinter = new PrintHelper(mActivity);
//        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
//        photoPrinter.printBitmap("droids.jpg - test print", bitmap);

//        String dataToPrint="$big$This is a printer test$intro$posprinterdriver.com$intro$$intro$$cut$$intro$";
//
//        Intent intentPrint = new Intent();

//        intentPrint.setAction(Intent.ACTION_SEND);
//        intentPrint.putExtra(Intent.EXTRA_TEXT, dataToPrint);
//        intentPrint.setType("text/plain");
//
//        mActivity.startActivity(intentPrint);

        if(bitmap != null){
            if (baseApp.isAidl()) {
                AidlUtil.getInstance().printBitmap(bitmap);
            } else {
                BluetoothUtil.sendData(ESCUtil.nextLine(3));
            }
//            BluetoothUtil.sendData(ESCUtil.nextLine(3));
        }
    }

    public static void showAlert(final Activity context, String title, String msg, String btnTitleCancel, String btnTitleConfirm, boolean isTouchOutSide, final Callback callback, boolean... isImgShow){
        try {
            if (title.length() == 0) {
                title = null;
            }
            final MoMoAlertDialog dialog = new MoMoAlertDialog(context);
            dialog.setTitle(title)
                    .setMessage(msg)
                    .setCanceledOnTouchOutside(isTouchOutSide)
                    .setNegativeButton(btnTitleCancel, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (callback != null) {
                                callback.invoke(1);
                            }
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton(btnTitleConfirm, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (callback != null) {
                                callback.invoke(0);
                            }
                            dialog.dismiss();
                        }
                    });
            boolean imgErrShow = isImgShow.length > 0 && isImgShow[0];
            dialog.setEnableHeader(imgErrShow);

            boolean isButtonVertical = false;
            if(btnTitleConfirm != null && btnTitleConfirm.length() >= 15)
                isButtonVertical = true;
            dialog.setIsButtonVertical(isButtonVertical);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showLCD(Activity mActivity, String price){
        IWoyouService woyouService = MainActivity.getWoyouService();
        if(woyouService == null){
            Toast.makeText(mActivity, "Service not ready", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            //woyouService.sendLCDCommand(2);
            woyouService.sendLCDDoubleString("Tổng tiền",price ,null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}
