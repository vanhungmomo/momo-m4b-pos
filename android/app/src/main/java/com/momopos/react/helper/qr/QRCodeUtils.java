package com.momopos.react.helper.qr;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.momopos.R;
import com.momopos.react.helper.DevicesUtils;
import java.io.*;

/**
 * Created by luat on 7/14/14.
 */
public class QRCodeUtils {

    // folder derectory QR code
    public static final String FOLDER_SDCARD_EXTRA = "/momo/momo_qrcode/";

    public static int QR_CODE_TYPE = -1;

    /**
     * Tính toán kích thước QRCode hiển thị
     * @param ctx Ngữ cảnh gọi hàm
     * @return kích thước hình ảnh
     */
    public static int qrSize(Context ctx){
        //Find screen size
        int width = DevicesUtils.getDeviceWidth(ctx);
        int height = DevicesUtils.getDeviceHeight(ctx);
        int smallerDimension = width < height ? width : height;
//        smallerDimension = smallerDimension * 3/4;
        return smallerDimension;
    }

    /**
     * Gen QRCode
     * @param ctx Ngữ cảnh gọi hàm
     * @param qrInputText Chuỗi dữ liệu cần gen QR Code
     * @return  hình đã gen được
     */
    public static Bitmap generate(Context ctx, String qrInputText){

        Bitmap bitmap = null;

        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                qrSize(ctx));

        //Generate QR code
        try {
            bitmap = qrCodeEncoder.encodeAsBitmap();
        } catch (WriterException e) {
            e.printStackTrace();
            bitmap = null;
        }

        return bitmap;
    }

    /**
     * Tạo thư mục chứa QRCode images
     *
     * @param ctx Ngữ cảnh gọi hàm
     * @return Đường dẫn thư mục đã tạo
     */
    public static String createQRCodeFolder(Context ctx) {
        String ExternalStorageDirectoryPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String targetPath = ExternalStorageDirectoryPath + FOLDER_SDCARD_EXTRA;
        File targetDirector = new File(targetPath);
        if(!targetDirector.exists()){
            targetDirector.mkdirs();
        }
//        File folder = new File(MoMoAvatar.getRootPath(ctx) + File.separator + MoMoAvatar.FOLDER_NAME_MOMO + File.separator + "momo_qrcode");
//        if (!folder.exists()) {
//            folder.mkdirs();
//        }
        return targetDirector.getPath();
    }

    /**
     * Lưu lại QR Code đã tạo
     * @param ctx Ngữ cảnh gọi hàm
     * @param fileName  Tên file lưu lại
     * @param bmp Hình cần lưu lại
     */
    public static File saveQRCode(Context ctx, String fileName, Bitmap bmp){
        String pathFolder = createQRCodeFolder(ctx);

        OutputStream fOut = null;
        File file = new File(pathFolder, fileName);

        try {
            if (file.exists()){
                file.createNewFile();
            }

            fOut = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            file = null;
        } catch (IOException e) {
            e.printStackTrace();
            file = null;
        }
        return file;
    }

    /**
     * Lấy file QR Code đã tạo
     * @param ctx Ngữ cảnh gọi hàm
     * @param fileName  Tên file lưu lại
     * @param bmp Hình cần lưu lại
     */
    public static File getQRCode(Context ctx, String fileName, Bitmap bmp){

        OutputStream fOut = null;
        File file = new File(fileName);
        if (file.exists())
            return file;

        try {
            fOut = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

        } catch (Exception e) {
            e.printStackTrace();
            file = null;
        }
        return file;
    }

    public static String getBase64BarCode(String Code, Activity _context){
        try{
            Bitmap bitmapBarCode = QRCodeEncoder.encodeAsBitmapBarCode(Code, BarcodeFormat.CODE_128, DevicesUtils.getDeviceWidth(_context), DevicesUtils.getDeviceWidth(_context) / 3);
            String base64BarCode = ImagePickerHelper.getImageBase64FromBitmap(bitmapBarCode,512);
            return base64BarCode;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public static String getBase64QRCode(String Code, Activity _context){
        try{
            //QR Code
            int qrSize = QRCodeUtils.qrSize(_context);
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(Code, null, Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(), qrSize);
            Bitmap bitmap = BitmapFactory.decodeResource(_context.getResources(), R.mipmap.ic_momo_qr_code);
            bitmap = Bitmap.createScaledBitmap(bitmap, qrSize / 5, qrSize / 5, true);
            bitmap = qrCodeEncoder.encodeAsBitmap_Logo(bitmap);
            //Bitmap bitmapQRCode = qrCodeEncoder.encodeAsBitmap();
            String base64QRCode = ImagePickerHelper.getImageBase64FromBitmap(bitmap,512);
            return base64QRCode;
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

//    public static String formatQRCode(int type, String phoneNumber, String amount, String content){
//        String template = "%s" + MomoMessage.BELL + "%s" + MomoMessage.BELL + "%s" + MomoMessage.BELL + "%s";
//        return String.format(template, type, phoneNumber, amount, content);
//    }

}
