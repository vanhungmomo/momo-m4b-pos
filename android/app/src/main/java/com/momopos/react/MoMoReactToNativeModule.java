package com.momopos.react;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Base64;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.momopos.MainActivity;
import com.momopos.react.helper.qr.QRCodeUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by vanhungpc on 6/8/18.
 */

public class MoMoReactToNativeModule extends ReactContextBaseJavaModule {

    public MoMoReactToNativeModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "MoMoReactToNative";
    }

    @ReactMethod
    public void printData(String data, String token) {
        MainActivity mActivity = (MainActivity)getCurrentActivity();
       MoMoReactHelper.getInstance().actionPrint(mActivity, data, token);
    }


    @ReactMethod
    public void openMoMoDetailsSettings() {
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",mActivity.getPackageName(), null);
        intent.setData(uri);
        mActivity.startActivity(intent);
    }

    @ReactMethod
    public void showLoadingTransactionWithMessage(String text, String textDesc){
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        MoMoLoading.showLoadingTransactionWithMessage(mActivity, text, textDesc, false);
    }

    @ReactMethod
    public void showLoading() {
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        MoMoLoading.showLoading(mActivity);
    }

    @ReactMethod
    public void showLoadingWithMessage(String text) {
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        MoMoLoading.showLoadingWithMessage(mActivity,text,false);
    }
    @ReactMethod
    public void hideLoading() {
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        MoMoLoading.hideLoading(mActivity,true);
    }

    @ReactMethod
    public void hideLoadingTransaction() {
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        MoMoLoading.hideLoading(mActivity,false);
    }

    @ReactMethod
    public void showAlertNative(String title, String msg, String btnTitleCancel, String btnTitleConfirm, boolean isTouchOutSide, final Callback callback){
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        MoMoReactHelper.showAlert(mActivity,title,msg,btnTitleCancel,btnTitleConfirm,isTouchOutSide,callback);
    }


    @ReactMethod
    public void getBase64String(String uri, Promise promise) {
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        Bitmap myBitmap = null;
        try {
            URL url = new URL(uri);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            myBitmap = BitmapFactory.decodeStream(input);
            if (myBitmap == null) {
                promise.reject("Error", "Failed to decode Bitmap, uri: " + uri);
            } else {
                promise.resolve(bitmapToBase64(myBitmap));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @ReactMethod
    public void getSN(Promise promise){
//        promise.resolve("SONASSSSSS");
        promise.resolve(SystemProperties.get("ro.serialno"));
    }

    @ReactMethod
    public void showLCD(String price){
        MainActivity mActivity = (MainActivity)getCurrentActivity();
        MoMoReactHelper.showLCD(mActivity, price);
    }

    @ReactMethod
    public void generateCode(final String payment_code, final Callback cb) {
        final MainActivity mActivity = (MainActivity)getCurrentActivity();
        if(mActivity != null) {
            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    String Code = payment_code;
                    String base64BarCode = QRCodeUtils.getBase64BarCode(Code, mActivity);
                    String base64QRCode = QRCodeUtils.getBase64QRCode(Code, mActivity);
                    cb.invoke(base64QRCode, base64BarCode);
                    return null;
                }
            };
            task.execute();
        }
    }
}