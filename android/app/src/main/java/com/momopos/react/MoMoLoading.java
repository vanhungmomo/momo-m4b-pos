package com.momopos.react;

import android.app.Activity;
import android.app.Dialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.momopos.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by vanhungpc on 6/10/18.
 */

public class MoMoLoading {
    public static Dialog dialogProcessBar;
    private static ProgressBar mprogressBar;
    private static ImageView iv_success_process;
    private static TextView tv_process;



    public static void showLoading(final Activity activity) {
        showLoadingWithMessage(activity,null,false);
    }


    public static void showLoadingWithMessage(final Activity activity, final String text,final boolean isCancel) {
        if (activity!=null && !activity.isFinishing()){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //synchronized (dialogProcessBar){
                        if (dialogProcessBar != null) {
                            dialogProcessBar.dismiss();
                            dialogProcessBar = null;
                        }

                        dialogProcessBar = new Dialog(activity, R.style.MoMoDialog);
                        dialogProcessBar.requestWindowFeature(Window.FEATURE_NO_TITLE);


                        dialogProcessBar.setCancelable(isCancel);


                        dialogProcessBar.setContentView(R.layout.popup_processing_bar);
                        iv_success_process = (ImageView) dialogProcessBar.findViewById(R.id.iv_success_process);
                        tv_process = (TextView) dialogProcessBar.findViewById(R.id.popup_processing_bar_tv);
                        if (TextUtils.isEmpty(text)){
                            tv_process.setVisibility(View.INVISIBLE);
                        }else {
                            tv_process.setVisibility(View.VISIBLE);
                            tv_process.setText(text);
                        }
                        mprogressBar = (ProgressBar) dialogProcessBar.findViewById(R.id.circular_progress_bar);
                        final Animation myRotation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.rotator);
                        mprogressBar.startAnimation(myRotation);

                        if (dialogProcessBar != null && !dialogProcessBar.isShowing())
                            dialogProcessBar.show();
                        //}


                    } catch (Exception e) {
                        //android.util.Log.e("showLoading", e.toString());
                    }
                }
            });
        }
    }

    public static void showLoadingTransactionWithMessage(final Activity activity, final String text, final String desc, final boolean isCancel) {
        if (activity!=null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //synchronized (dialogProcessBar){
                        if (dialogProcessBar != null && dialogProcessBar.isShowing()) {
                            dialogProcessBar.dismiss();
                            dialogProcessBar = null;
                        }

                        dialogProcessBar = new Dialog(activity, R.style.MoMoDialog);
                        dialogProcessBar.requestWindowFeature(Window.FEATURE_NO_TITLE);


                        dialogProcessBar.setCancelable(isCancel);


                        dialogProcessBar.setContentView(R.layout.popup_processing_transaction_bar);
                        iv_success_process = (ImageView) dialogProcessBar.findViewById(R.id.iv_success_process);
                        //iv_success_process.setImageResource(resId);


                        tv_process = (TextView) dialogProcessBar.findViewById(R.id.popup_processing_bar_tv);
                        if (TextUtils.isEmpty(text)){
                            tv_process.setVisibility(View.INVISIBLE);
                        }else {
                            tv_process.setVisibility(View.VISIBLE);
                            tv_process.setText(text);
                        }

                        TextView tv_desc = (TextView) dialogProcessBar.findViewById(R.id.desc_processing_bar_tv);
                        if(TextUtils.isEmpty(desc)){
                            tv_desc.setVisibility(View.INVISIBLE);
                        }
                        else{
                            tv_desc.setVisibility(View.VISIBLE);
                            tv_desc.setText(desc);
                        }

                        mprogressBar = (ProgressBar) dialogProcessBar.findViewById(R.id.circular_progress_bar);
                        final Animation myRotation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.rotator);
                        mprogressBar.startAnimation(myRotation);

                        if (dialogProcessBar != null && !dialogProcessBar.isShowing())
                            dialogProcessBar.show();
                        //}


                    } catch (Exception e) {
                        //android.util.Log.e("showLoading", e.toString());
                    }
                }
            });
        }
    }


    public static void hideLoading(final Activity activity,final boolean isShowDone) {
        try {
            if (activity !=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoadingDialog(isShowDone);
                    }
                });
            }else {
                hideLoadingDialog(isShowDone);
            }
        }catch (Exception e){
            //android.util.Log.e("hideLoading", e.toString());
        }
    }

    public static void showDoneLoading(final Activity activity) {
        try {
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mprogressBar.clearAnimation();
                        mprogressBar.setVisibility(View.INVISIBLE);
                        iv_success_process.setVisibility(View.VISIBLE);

                        dismissTimerTask(200L);

                        if (dialogProcessBar != null)
                            dialogProcessBar.show();
                    }
                });
            }else {
                if(dialogProcessBar != null && dialogProcessBar.isShowing()){
                    dialogProcessBar.dismiss();
                }
            }
        }catch (Exception e){
            //android.util.Log.e("showDoneLoading", e.toString());
        }
    }

    public static void dismissLoadingDialog(final Activity activity) {
        try {
            if (activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(dialogProcessBar != null && dialogProcessBar.isShowing()){
                            dialogProcessBar.dismiss();
                        }
                    }
                });
            }else {
                if(dialogProcessBar != null && dialogProcessBar.isShowing()){
                    dialogProcessBar.dismiss();
                }
            }
        }catch (Exception e){
            //android.util.Log.e("dismissLoadingDialog", e.toString());
        }
    }

    private static void hideLoadingDialog(final boolean isShowDone){
        if (dialogProcessBar != null) {
            dialogProcessBar.dismiss();
            mprogressBar.clearAnimation();
            mprogressBar.setVisibility(View.INVISIBLE);
            if (isShowDone){
                iv_success_process.setVisibility(View.VISIBLE);
            }else {
                iv_success_process.setVisibility(View.INVISIBLE);
            }
            dismissTimerTask(200L);
        }
    }

    private static void dismissTimerTask(long milisecond){
        try {
            TimerTask task = new TimerTask() {
                public void run() {
                    dismissLoadingDialog(null);
                }
            };
            Timer timer2 = new Timer("Timer");
            long delay = milisecond;
            timer2.schedule(task, delay);
        }catch (Exception e){
            //android.util.Log.e("dismissTimerTask", e.toString());
        }
    }
}
