package com.momopos;

import android.app.Application;
import com.facebook.react.ReactApplication;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.momopos.modules.printer.SunmiInnerPrinterPackage;
import com.momopos.modules.scanner.SunmiInnerScannerPackage;
import com.momopos.react.MoMoReactToNativePackage;
import com.momopos.utils.AidlUtil;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.momopos.modules.ExpoReactPackage;
import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private boolean isAidl;

  public boolean isAidl() {
    return isAidl;
  }

  public void setAidl(boolean aidl) {
    isAidl = aidl;
  }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
              new MainReactPackage(),
            new AsyncStoragePackage(),
              new ExpoReactPackage(),
              new MoMoReactToNativePackage(),
              new SunmiInnerScannerPackage(),
              new SunmiInnerPrinterPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, false);
    isAidl = true;
    AidlUtil.getInstance().connectPrinterService(this);
  }
}
